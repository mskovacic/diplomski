module.exports = function(grunt) {
  // Bower directory for JS libs.
  var bowerDir = 'client/bower_components/';

  // App source JS files.
  var scriptsDir = 'client/js/source/';
  var scriptsSafeDestDir = 'client/js/min-safe/';
  grunt.option('scriptsSafeDestDir', 'client/js/min-safe/');
  grunt.option('scriptsDestDir', 'client/js/');

  // CSS and fonts.
  var stylesDir = 'client/css/source/';
  grunt.option('stylesDestDir', 'client/css/');
  var fontsDestDir = 'client/fonts/';

  // Grunt configuration.
  grunt.initConfig({
    // Concat and minimize CSS files.
    cssmin: {
      options: {
        shorthandCompacting: true
      },
      style: {
        files: {
          '<%= grunt.option("stylesDestDir") %>style.min.css': [
            bowerDir + 'bootstrap/dist/css/bootstrap.min.css',
            bowerDir + 'metisMenu/dist/metisMenu.min.css',
            bowerDir + 'font-awesome-min/css/font-awesome.min.css',
            bowerDir + 'angular-loading-bar/build/loading-bar.min.css',
            bowerDir + 'datatables.net-bs/css/dataTables.bootstrap.min.css',
            bowerDir + 'angularjs-datepicker/dist/angular-datepicker.min.css',
            stylesDir + 'sb-admin-2.css',
            stylesDir + 'BootstrapXL.css',
            stylesDir + 'style.css'
          ]
        }
      }
    },
    // Copy fonts.
    copy: {
      fonts: {
        expand: true,
        flatten: true,
        src: bowerDir + 'font-awesome-bower/fonts/*',
        dest: fontsDestDir,
      },
    },
    // Set up dependency injections for minifying.
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      app: {
        files: {
          '<%= grunt.option("scriptsSafeDestDir") %>index.js': [scriptsDir + 'index.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>homeCtrl.js': [scriptsDir + 'controllers/homeCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>loginCtrl.js': [scriptsDir + 'controllers/loginCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>registerCtrl.js': [scriptsDir + 'controllers/registerCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>adminsCtrl.js': [scriptsDir + 'controllers/adminsCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>panelCtrl.js': [scriptsDir + 'controllers/panelCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>resetPwdCtrl.js': [scriptsDir + 'controllers/resetPwdCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>tokensCtrl.js': [scriptsDir + 'controllers/tokensCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>tokenListCtrl.js': [scriptsDir + 'controllers/tokenListCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>usersCtrl.js': [scriptsDir + 'controllers/usersCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>requestsCtrl.js': [scriptsDir + 'controllers/requestsCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>errorsCtrl.js': [scriptsDir + 'controllers/errorsCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>networksCtrl.js': [scriptsDir + 'controllers/networksCtrl.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>tokenDetailsDir.js': [scriptsDir + 'directives/tokenDetailsDir.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>requestsDir.js': [scriptsDir + 'directives/requestsDir.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>adminRolesDir.js': [scriptsDir + 'directives/adminRolesDir.js'],
          '<%= grunt.option("scriptsSafeDestDir") %>ngEnterDir.js': [scriptsDir + 'directives/ngEnterDir.js']
        }
      }
    },
    // JS minification.
    uglify: {
      options: {
        mangle: false // Don't mangle (controller names, etc.)
      },
      js: {
        options: {
          sourceMap: true, // Generate source maps to show line numbers in console (outputs and errors).
          sourceMapName: '<%= grunt.option("scriptsDestDir") %>app.js.map'
        },
        files: {
          '<%= grunt.option("scriptsDestDir") %>app.min.js': [
            bowerDir + 'jquery/dist/jquery.min.js',
            bowerDir + 'angular/angular.js',
            bowerDir + 'angular-ui-router/release/angular-ui-router.min.js',
            bowerDir + 'angular-animate/angular-animate.min.js',
            bowerDir + 'angular-loading-bar/build/loading-bar.min.js',
            bowerDir + 'bootstrap/dist/js/bootstrap.min.js',
            bowerDir + 'metisMenu/dist/metisMenu.min.js',
            bowerDir + 'datatables.net/js/jquery.dataTables.min.js',
            bowerDir + 'datatables.net-bs/js/dataTables.bootstrap.min.js',
            bowerDir + 'angularjs-datepicker/dist/angular-datepicker.min.js',
            scriptsDir + 'sb-admin-2.js',
            scriptsDir + 'config.js',
            scriptsDir + 'translations/translations.js',
            scriptsDir + 'translations/translations.en.js',
            scriptsDir + 'translations/translations.hr.js',
            scriptsDir + 'charts/loader.js',
            scriptsSafeDestDir + 'index.js',
            scriptsSafeDestDir + 'homeCtrl.js',
            scriptsSafeDestDir + 'loginCtrl.js',
            scriptsSafeDestDir + 'registerCtrl.js',
            scriptsSafeDestDir + 'adminsCtrl.js',
            scriptsSafeDestDir + 'panelCtrl.js',
            scriptsSafeDestDir + 'resetPwdCtrl.js',
            scriptsSafeDestDir + 'tokensCtrl.js',
            scriptsSafeDestDir + 'tokenListCtrl.js',
            scriptsSafeDestDir + 'usersCtrl.js',
            scriptsSafeDestDir + 'requestsCtrl.js',
            scriptsSafeDestDir + 'errorsCtrl.js',
            scriptsSafeDestDir + 'networksCtrl.js',
            scriptsSafeDestDir + 'tokenDetailsDir.js',
            scriptsSafeDestDir + 'requestsDir.js',
            scriptsSafeDestDir + 'adminRolesDir.js',
            scriptsSafeDestDir + 'ngEnterDir.js'
          ]
        }
      }
    },
    watch: {
      scripts: {
        files: [scriptsDir + '**/*.js', 'Gruntfile.js'],
        tasks: ['ngAnnotate', 'uglify'],
        options: {
          interrupt: true,
        }
      }
    }
  });

  // Load tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default grunt task concats and minifies JS and CSS.
  grunt.registerTask('default', ['cssmin', 'copy', 'ngAnnotate', 'uglify']);
};
