"use strict";

const cluster = require('cluster');
global.worker = cluster.worker;
if (cluster.isMaster) {
  var config = require('./config.js');
  var cpuCount = require('os').cpus().length;
  if (config.maxCPU > 0 && config.maxCPU < cpuCount) {
    cpuCount = config.maxCPU;
  }
  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }
} else {
  // Dependencies.
  const config = require('./config.js');
  const express = require('express');
  const router = express.Router();
  const app = express();
  const bodyParser = require('body-parser');
  const uaParser = require('ua-parser-js');
  const fs = require('fs');
  const winston = require('winston');
  require('winston-daily-rotate-file');

  // Logging.
  if (!fs.existsSync(config.logDir)) {
    fs.mkdirSync(config.logDir);
  }

  const tsFormat = () => (new Date()).toLocaleTimeString();
  const transport = new (winston.transports.DailyRotateFile)({
    filename: `${config.logDir}/log`,
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: process.env.ENV === 'development' ? 'debug' : 'info'
  });
  const logger = new (winston.Logger)({
    exitOnError: false,
    level: config.logLevel,
    transports: [
      transport,
      new winston.transports.Console({
        timestamp: tsFormat,
        colorize: true
      })
    ]
  }); 
  global.ispravimeLogger = logger;

  // Set compression.
  if (config.useCompression) {
    var compression = require('compression');
    app.use(compression());
  }

  // Check if application is run in testing mode.
  global.testing = false;
  if (process.argv.length == 3) {
    if (process.argv[2] == 'testing') {
      global.testing = true;
    }
  }
  if (global.testing && cluster.worker.id == 1) {
    global.ispravimeLogger.log('info', 'Running app in testing configuration');
  }

  // Set body parser.
  app.use(bodyParser.json({
    'limit': '50mb'
  }));
  app.use(bodyParser.urlencoded({
    'extended': false,
    'limit': '50mb'
  }));

  // Set routes.
  require('./routes/_routes.js')(router);
  app.use('/api', router);
  // No valid route found
  app.use('/api/*', function(req, res) {
    res.status(404);
    res.json({
      'success': false,
      'response': 'endpoint_not_exist'
    });
  });

  if (!config.onlyApi) {
    // Set frontend page.
    app.use(express.static('client'));
    // For refreshing.
    app.use(function(req, res) {
      // Use res.sendfile, as it streams instead of reading the file into memory.
      res.sendFile(__dirname + '/client/index.html');
    });
  }

  // Generic errors.
  app.use(function(err, req, res, next) {
    global.ispravimeLogger.log('error', err);
    res.status(500);
    res.json({
      'success': false,
      'response': 'error_generic',
      'details': err
    });
  });

  // Listen on HTTP and HTTPS
  var https = require('https');
  var http = require('http');
  var sslOptions = {
    key: config.sslKey,
    cert: config.sslCert,
    ca: config.sslCACert,
    requestCert: true,
    rejectUnauthorized: false
  };
  var httpListener = http.createServer(app).listen(config.httpPort, config.listenAddress, function() {
    global.ispravimeLogger.log('info', 'Worker ' + cluster.worker.id + ': HTTP listening on ' + httpListener.address().address + ':' + httpListener.address().port);
  });
  var httpsListener = https.createServer(sslOptions, app).listen(config.httpsPort, config.listenAddress, function() {
    global.ispravimeLogger.log('info', 'Worker ' + cluster.worker.id + ': HTTPS listening on ' + httpsListener.address().address + ':' + httpsListener.address().port);
  });

  // Listen on external interfaces
  if (config.listenOnExternalAddress) {
    var os = require('os');
    var ifaces = os.networkInterfaces();

    var eHttpListeners = {};
    var eHttpsListeners = {};

    Object.keys(ifaces).forEach(function(ifname) {
      var alias = 0;

      ifaces[ifname].forEach(function(iface) {
        if ('IPv4' !== iface.family || iface.internal !== false) {
          // Skip internal interfaces, we already listened on them.
          return;
        }
        var listenerName = ifname + alias;
        eHttpListeners[listenerName] = http.createServer(app).listen(config.httpPort, iface.address, function() {
          global.ispravimeLogger.log('info', 'Worker ' + cluster.worker.id + ': HTTP listening on ' + eHttpListeners[listenerName].address().address + ':' + eHttpListeners[listenerName].address().port);
        });
        eHttpsListeners[listenerName] = https.createServer(sslOptions, app).listen(config.httpsPort, iface.address, function() {
          global.ispravimeLogger.log('info', 'Worker ' + cluster.worker.id + ': HTTPS listening on ' + eHttpsListeners[listenerName].address().address + ':' + eHttpsListeners[listenerName].address().port);
        });
        ++alias;
      });
    });
  }
}
