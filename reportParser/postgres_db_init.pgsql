--CREATE DATABASE ispravi_me_statistika ENCODING UTF8;
CREATE DATABASE db3 WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    TABLESPACE = baza;
\c db3
CREATE TABLE _User (
    Id UUID PRIMARY KEY,
    Name TEXT,
    FirstAccess TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    LastAccess TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    LastIP INET NOT NULL,
    NumberOfRequests INTEGER NOT NULL DEFAULT 0,
    NumberOfMistakes INTEGER NOT NULL DEFAULT 0,
    Ignore BOOLEAN NOT NULL DEFAULT FALSE
);

--ALTER TABLE _user RENAME COLUMN NumberOfMistakesTotal TO NumberOfMistakes

CREATE TABLE UserIPHistory (
    Id SERIAL PRIMARY KEY,
    UserId UUID NOT NULL CONSTRAINT UserUserIPHistoryFK REFERENCES _User(Id) ON DELETE CASCADE,
    IP INET NOT NULL,
    Access TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX ON UserIPHistory(UserId);

CREATE TABLE Request (
    Id SERIAL PRIMARY KEY,
    RequestTime TIMESTAMP WITH TIME ZONE NOT NULL,
    ProcessingTime DOUBLE PRECISION NULL,
    IpAddress INET NOT NULL,
    WordCount INTEGER NOT NULL,
    MistakeCount INTEGER NOT NULL,
    UniqueMistakeCount INTEGER NOT NULL,
    UserId UUID NOT NULL CONSTRAINT UserRequestFK REFERENCES _User(Id) ON DELETE CASCADE,
    TextFile TEXT NOT NULL,
    Context VARCHAR(200) NOT NULL,
    Ignore BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE INDEX ON Request (UserId);
CREATE INDEX ON Request (RequestTime);

CREATE TABLE Mistake (
    Id SERIAL PRIMARY KEY,
    Suspicious TEXT NOT NULL,
    Suggestions TEXT NOT NULL,
    Severity VARCHAR(50) NOT NULL,
    RequestCount INTEGER NOT NULL,
    TotalOccurrences INTEGER NOT NULL,
    UNIQUE (Suspicious)
);

CREATE INDEX ON Mistake (Suspicious);

CREATE TABLE UniqueMistake (
    Id SERIAL PRIMARY KEY,
    Suspicious TEXT NOT NULL,
    Suggestions TEXT NOT NULL,
    Severity VARCHAR(50) NOT NULL,
    RequestCount INTEGER NOT NULL,
    TotalOccurrences INTEGER NOT NULL,
    UNIQUE (Suspicious)
);

CREATE INDEX ON UniqueMistake (Suspicious);

CREATE TABLE MistakeRequest (
    UniqueMistakeId INTEGER NOT NULL CONSTRAINT MistakeRequestUniqueMistakeIdFK REFERENCES UniqueMistake(Id) ON DELETE CASCADE,
    MistakeId INTEGER NOT NULL CONSTRAINT MistakeRequestMistakeIdFK REFERENCES Mistake(Id) ON DELETE CASCADE,
    RequestId INTEGER NOT NULL CONSTRAINT MistakeRequestIdFK REFERENCES Request(Id) ON DELETE CASCADE,
    Occurrences INTEGER NOT NULL,
    UNIQUE (UniqueMistakeId, MistakeId, RequestId)
);

CREATE INDEX ON MistakeRequest(UniqueMistakeId);
CREATE INDEX ON MistakeRequest(MistakeId);
CREATE INDEX ON MistakeRequest(RequestId);

CREATE TABLE _Network (
    Id SERIAL PRIMARY KEY,
    Name TEXT NOT NULL,
    Mask VARCHAR(200) NOT NULL,
    Allowed BOOLEAN DEFAULT TRUE
);

CREATE TABLE _Admin (
    Id SERIAL PRIMARY KEY,
    Email VARCHAR(200) NOT NULL,
    DateRegistered TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    Salt VARCHAR(200) NOT NULL,
    Password VARCHAR(200),
    UNIQUE (email)
);

CREATE TABLE AdminPasswordReset (
    Id SERIAL PRIMARY KEY,
    Token TEXT,
    IssuedAt TIMESTAMP WITH TIME ZONE,
    AdminId INTEGER NOT NULL CONSTRAINT RoleAdminIdFK REFERENCES _Admin(Id) ON DELETE CASCADE
);

CREATE INDEX ON AdminPasswordReset(AdminId);

CREATE TABLE _Role (
    Id SERIAL PRIMARY KEY,
    Name VARCHAR(200) NOT NULL,
    AdminId INTEGER NOT NULL CONSTRAINT RoleAdminIdFK REFERENCES _Admin(Id) ON DELETE CASCADE,
    UNIQUE (Name, AdminId)
);

CREATE INDEX ON _Role(AdminId);

CREATE TABLE Token (
    Id SERIAL PRIMARY KEY,
    Token TEXT NOT NULL,
    AdminId INTEGER NOT NULL CONSTRAINT TokenAdminIdFK REFERENCES _Admin(Id) ON DELETE CASCADE,
    IsValid BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE INDEX ON Token(AdminId);

CREATE TABLE ServiceAccess (
    Id SERIAL PRIMARY KEY,
    Uri TEXT NOT NULL,
    Method VARCHAR(20) NOT NULL,
    TokenId INTEGER NULL CONSTRAINT ServiceAccessTokenFK REFERENCES Token(Id) ON DELETE CASCADE,
    AdminId INTEGER NULL CONSTRAINT ServiceAccessAdminFK REFERENCES _Admin(Id) ON DELETE CASCADE
);

CREATE INDEX ON ServiceAccess(TokenId);
CREATE INDEX ON ServiceAccess(AdminId);

CREATE TABLE RevokedToken (
    Id SERIAL PRIMARY KEY,
    Token TEXT NOT NULL,
    TokenOwner INTEGER NOT NULL CONSTRAINT RevokedTokenAdminFK REFERENCES _Admin(Id) ON DELETE CASCADE,
    RevokedAt TIMESTAMP WITH TIME ZONE NOT NULL,
    RevokedUntil TIMESTAMP WITH TIME ZONE NULL,
    RevokedBy INTEGER NOT NULL,
    RevocationReason TEXT NULL
);

CREATE INDEX ON RevokedToken(Token);
CREATE INDEX ON RevokedToken(TokenOwner);

CREATE TABLE Service (
    Id SERIAL PRIMARY KEY,
    Name TEXT NOT NULL,
    Description TEXT NOT NULL,
    UNIQUE (Name)
);

CREATE TABLE ServiceEndpoint (
    Id SERIAL PRIMARY KEY,
    Method VARCHAR(20) NOT NULL,
    Description TEXT NOT NULL,
    NoAuthRequired BOOLEAN NOT NULL,
    AllowedByDefault BOOLEAN NOT NULL,
    ServiceId INTEGER NOT NULL CONSTRAINT ServiceEndpointServiceFK REFERENCES Service(Id) ON DELETE CASCADE,
    Uri TEXT NOT NULL,
    UNIQUE(Method, ServiceId, Uri)
);

CREATE INDEX ON ServiceEndpoint(ServiceId);

CREATE TABLE AllowedRoles (
    Id SERIAL PRIMARY KEY,
    Name VARCHAR(200) NOT NULL,
    ServiceEndpointId INTEGER NOT NULL CONSTRAINT ServiceEndpointRolesFK REFERENCES ServiceEndpoint(Id) ON DELETE CASCADE,
    UNIQUE (Name, ServiceEndpointId)
);

CREATE INDEX ON AllowedRoles(ServiceEndpointId);
