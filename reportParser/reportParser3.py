#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Skripta koja upisuje nove zahtjeve u bazu.
"""
import time
import json
import sys
import getopt
import psycopg2

if sys.version_info > (3, 0):
    # Python 3 code in this block
    pass
else:
    # Python 2 code in this block
    print("Please use python 3!")
    exit(1)

def popravi(redak):
    """zamijene se cudni znakovi hrvatskim slovima"""
    return(redak.replace("{", u"š")
        .replace("}", u"ć")
        .replace("~", u"č")
        .replace("|", u"đ")
        .replace("`", u"ž")
        .replace("[", u"Š")
        .replace("]", u"Ć")
        .replace("^", u"Č")
        .replace("\\", u"Đ")
        .replace("@", u"Ž")
        .replace("#", " "))

def parse_time(arg):
    """Pretvori string u vrijeme"""
    if arg == '0':
        return None
    #return time.strptime(arg, '%a %b %d %H:%M:%S %Z %Y')
    if ("CET" in arg):
        return time.strptime(arg, '%a %b %d %H:%M:%S CET %Y')
    else:
        return time.strptime(arg, '%a %b %d %H:%M:%S CEST %Y')

def parse_iso_time(arg):
    """Pretvori iso string u vrijeme"""
    try:
        return time.strptime(arg, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        print("Uneseni datum mora biti u ISO formatu!")
        sys.exit(1)

def main(argv):
    """Glavna metoda u skripti"""
    stat_filename = ''
    report_filename = ''
    begin_index = 1
    begin_time = None
    end_time = None
    dsn = "host=localhost dbname=db1 user=postgres password=postgres"
    try:
        opts, args = getopt.getopt(argv,"hs:r:u:i:b:e:d:",["sfile=","rfile=","importurl=","begin_index=","begin_time=","end_time="])
    except getopt.GetoptError:
        print('Usage: parseReport.py -s <stat_filename> -r <report_filename> -u <importurl> -i <beingindex> -b <begin_time> -e <end_time> -d <dsn>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: parseReport.py -s <stat_filename> -r <report_filename> -u <importurl> -i <beingindex> -b <begin_time> -e <end_time> -d <dsn>')
            print("-d, --dsn        Example: 'host=localhost dbname=db1 user=postgres password=postgres'")
            sys.exit()
        elif opt in ("-s", "--stat_filename"):
            stat_filename = arg
        elif opt in ("-r", "--report_filename"):
            report_filename = arg
        elif opt in ("-i", "--beingindex"):
            begin_index = int(arg)
        elif opt in ("-b", "--beingtime"):
            begin_time = parse_iso_time(arg)
        elif opt in ("-e", "--end_time"):
            end_time = parse_iso_time(arg)
        elif opt in ("-d", "--dsn"):
            dsn = arg
    print('Stat file is', stat_filename)
    print('Report file is', report_filename)
    print('Starting at header #' + str(begin_index))
    if begin_time != None:
        print('Skipping reports/stats that are older than' + str(begin_time))
    if end_time != None:
        print('Skipping reports/stats that are newer than' + str(end_time))

    stat_file = open(stat_filename, "r", encoding="iso-8859-15")
    stats = stat_file.read()
    stat_file.close()

    report_file = open(report_filename, "r", encoding="iso-8859-15")
    reports = report_file.read()
    report_file.close()

    stats = stats.split("\nFrom: ")
    reports = reports.split("\nFrom: ")
    print('Number of headers in stat file: ' + str(len(stats)))
    print('Number of headers in report file: ' + str(len(reports)))
    min_length = min(len(stats), len(reports))
    last_header_diff = 0

    for i in range(begin_index, min_length):
        print('\nProcessing request ' + str(i) + '/' + str(min_length - 1))
        lines = stats[i].split("\n")
        request_time = parse_time(lines[2])
        if begin_time != None and request_time < begin_time:
            print("- skipping (request time is earlier than begin time)")
            continue
        if end_time != None and request_time > end_time:
            print("- skipping (request time is after end time)")
            continue
        processing_time = None
        if "Time: " in lines[0]:
            processing_time = float(lines[0].split()[2])
        ip_address = lines[0].split()[0]
        user_id = lines[1].split("UserID: ")[1]
        errors_types = lines[6].split()
        errors_count = lines[7].split()

        # Find matching report.
        m = i - last_header_diff - 1
        if m < 0:
            m = 0
        found_matching_report = False
        reports_split = []
        while not found_matching_report and m < min_length - 1:
            m += 1
            reports_split = reports[m].split('\n')
            time2 = None
            if "Time: " in reports_split[0]:
                time2 = float(reports_split[0].split()[2])
            request_time2 = parse_time(reports_split[2])
            ip2 = reports_split[0].split()[0]
            user_id2 = reports_split[1].split("UserID: ")[1]
            if request_time == request_time2 and ip_address == ip2 and user_id == user_id2 and processing_time == time2:
                print("Request time %s %s\nip %s %s\nuserid %s %s\ntime %s %s" % (
                    time.strftime("%c", request_time),
                    time.strftime("%c", request_time2), 
                    ip_address, 
                    ip2, 
                    user_id, 
                    user_id2, 
                    processing_time, 
                    time2
                ))
                found_matching_report = True
                break

        if found_matching_report:
            last_header_diff = m - i
            print('- matched stat header ' + str(i) + ' with report header ' + str(m))
            izvjestaj = {}
            izvjestaj['errors'] = []
            # izvjestaj['numberOfErrors'] = int(errors_count[-2]) # statistics file sometimes has wrong value - count manually in report file
            izvjestaj['numberOfErrors'] = 0

            if len(reports_split) == 5:
                # no errors
                pass
            else:
                for j in range(4, len(reports_split) - 1):
                    s = reports_split[j].split('=>')
                    sl = s[0].split()
                    _type = ''
                    suspicious = ''
                    num_occur = 0
                    if len(sl) == 2:
                        '''
                        type = '-xx-'
                        suspicious = popravi(sl[0])
                        num_occur = sl[1]
                        print 'UPOZORENJE'
                        print reports_split[j]
                        print suspicious
                        '''
                        continue
                    elif len(sl) == 3:
                        _type = sl[0]
                        suspicious = popravi(sl[1])
                        num_occur = sl[2]
                    else:
                        continue

                    e = {}
                    e['suspicious'] = suspicious
                    e['severity'] = _type.replace('-', '')
                    e['occurrences'] = int(num_occur)
                    izvjestaj['numberOfErrors'] += e['occurrences']

                    suggestions = []
                    e['suggestions'] = []
                    if len(s) == 2:
                        sr = s[1].split('?')
                        for k in sr[:-1]:
                            suggestions.append(popravi(k).strip())
                        e['suggestions'] = json.dumps(suggestions)

                    izvjestaj['errors'].append(e)

            podaci_za_bazu = {}
            podaci_za_bazu['ip'] = ip_address
            podaci_za_bazu['userID'] = user_id
            podaci_za_bazu['requestTime'] = time.strftime("%Y-%m-%d %H:%M:%S+00", request_time)
            podaci_za_bazu['context'] = "on"
            podaci_za_bazu['wordCounter'] = int(errors_count[-1])
            podaci_za_bazu['textFile'] = '/'.join(lines[4].split()[3].split('/')[-2:])
            podaci_za_bazu['report'] = izvjestaj
            podaci_za_bazu['timeProcessed'] = processing_time
            podaci_za_bazu['mistakesTotal'] = sum(map(lambda x: x['occurrences'], podaci_za_bazu['report']['errors']))
            print('- importing')
            conn = psycopg2.connect(dsn)
            cur = conn.cursor()
            cur.execute("UPDATE _User SET LastAccess = %s, LastIP = %s, NumberOfRequests = NumberOfRequests + 1, NumberOfMistakes = NumberOfMistakes + %s WHERE Id = %s", (
                podaci_za_bazu['requestTime'],
                podaci_za_bazu['ip'],
                podaci_za_bazu['mistakesTotal'],
                podaci_za_bazu['userID']
            ))
            if cur.rowcount == 0:
                cur.execute("INSERT INTO _User(Id, FirstAccess, LastAccess, LastIP, NumberOfRequests, NumberOfMistakes) VALUES (%s, %s, %s, %s, 1, %s)", (
                    podaci_za_bazu['userID'],
                    podaci_za_bazu['requestTime'],
                    podaci_za_bazu['requestTime'],
                    podaci_za_bazu['ip'],
                    podaci_za_bazu['mistakesTotal']
                ))
            cur.execute("INSERT INTO UserIPHistory(UserId, IP, Access) VALUES (%s, %s, %s)", (
                podaci_za_bazu['userID'],
                podaci_za_bazu['ip'],
                podaci_za_bazu['requestTime']
            ))
            cur.execute("INSERT INTO Request(RequestTime, ProcessingTime, IpAddress, WordCount, MistakeCount, UniqueMistakeCount, UserId, TextFile, Context) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING Id", (
                podaci_za_bazu['requestTime'],
                podaci_za_bazu['timeProcessed'],
                podaci_za_bazu['ip'],
                podaci_za_bazu['wordCounter'],
                podaci_za_bazu['report']['numberOfErrors'],
                len(podaci_za_bazu['report']['errors']),
                podaci_za_bazu['userID'],
                podaci_za_bazu['textFile'],
                podaci_za_bazu['context']
            ))
            request_id = cur.fetchone()[0]
            for mistake in podaci_za_bazu['report']['errors']:
                cur.execute("UPDATE Mistake SET TotalOccurrences = TotalOccurrences + %s, RequestCount = RequestCount + 1 WHERE suspicious = %s RETURNING Id", (
                    mistake["occurrences"],
                    mistake['suspicious']
                ))
                row = cur.fetchone()
                mistake_id = 0
                if row is None:
                    cur.execute("INSERT INTO Mistake(Suspicious, Suggestions, Severity, TotalOccurrences, RequestCount) VALUES (%s, %s, %s, %s, 1) RETURNING Id", (
                        mistake['suspicious'],
                        mistake["suggestions"],
                        mistake['severity'],
                        mistake["occurrences"]
                    ))
                    mistake_id = cur.fetchone()[0]
                else:
                    mistake_id = row[0]
                cur.execute("UPDATE UniqueMistake SET TotalOccurrences = TotalOccurrences + %s, RequestCount = RequestCount + 1 WHERE suspicious = LOWER(%s) RETURNING Id", (
                    mistake["occurrences"],
                    mistake['suspicious']
                ))
                row = cur.fetchone()
                unique_mistake_id = 0
                if row is None:
                    cur.execute("INSERT INTO UniqueMistake(Suspicious, Suggestions, Severity, TotalOccurrences, RequestCount) VALUES (LOWER(%s), %s, %s, %s, 1) RETURNING Id", (
                        mistake['suspicious'],
                        mistake["suggestions"],
                        mistake['severity'],
                        mistake["occurrences"]
                    ))
                    unique_mistake_id = cur.fetchone()[0]
                else:
                    unique_mistake_id = row[0]
                cur.execute('SAVEPOINT sp1')
                try:
                    cur.execute("INSERT INTO MistakeRequest(UniqueMistakeId,MistakeId,RequestId, Occurrences) VALUES (%s, %s, %s, %s)", (
                        unique_mistake_id,
                        mistake_id,
                        request_id,
                        mistake["occurrences"]
                    ))
                except psycopg2.IntegrityError:
                    cur.execute('ROLLBACK TO SAVEPOINT sp1')
                    cur.execute("UPDATE MistakeRequest SET Occurrences = Occurrences + %s WHERE MistakeId = %s AND RequestId = %s AND UniqueMistakeId = %s", (
                        mistake["occurrences"],
                        mistake_id,
                        request_id,
                        unique_mistake_id
                    ))
                else:
                    cur.execute('RELEASE SAVEPOINT sp1')
            conn.commit()
            cur.close()
            conn.close()
            last_import_file = open('lastImport.txt', 'w')
            last_import_file.write('Last index: {0}\nLast imported matching headers:\n\tHeader from report file {1}: #{2}\n\tHeader from stat file {3}: #{0}\n\n\tHeader:\n\t\t{4}\n\t\t{5}\n\t\t{6}\n'
                .format(str(i), report_filename, str(m), stat_filename, lines[0], lines[1], lines[2]))  # python will convert \n to os.linesep
            last_import_file.close()
        else:
            print('- no matching header in statistics file')

if __name__ == "__main__":
    main(sys.argv[1:])
