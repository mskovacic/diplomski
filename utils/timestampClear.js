// Clears jQuery's timestamp in URL for correct caching.
// cache: true in jQuery's ajax function does not work properly with apicache,
// requests get cached but don't get added to the index so they can't be cleared.

var timestampClear = function(req, res, next) {
  req.originalUrl = req.originalUrl.split('&_=')[0];
  req.url = req.url.split('&_=')[0];
  delete req.query._;
  next();
};

module.exports = {
  'timestampClear': timestampClear
};
