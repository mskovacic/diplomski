'use strict';

const appRoot = require('app-root-path');
const servicesBL = require(appRoot + '/bl/services.js');
const adminBL = require(appRoot + '/bl/admin.js');

var getEndpointPermissions = function(admId, callback) {
  servicesBL.getServiceAccessByAdmin(admId, (err, success) => {
    if (err || success.length == 0) {
      return callback(false);
    }

    return callback(admin.serviceAccess);
  });
};

var addEndpointPermission = function(admId, serviceName, uri, method, callback) {
  servicesBL.getServiceEndpointsByService(serviceName, method, uri, (err, success) => {
    if (success.length == 0 || err) {
      return callback('noService');
    }

    adminBL.createServiceAccess(uri, method, null, admId, (err, success) => {
      if (err) {
        return callback('errUpdate');
      }

      return callback(success);
    });
  });
};

var removeEndpointPermission = function(admId, uri, method, callback) {
  servicesBL.deleteServiceAccess(admId, uri, method, (err, success) => {
    if (err || success.length == 0) {
      return callback('errUpdate');
    }

    callback();
  });
};

var logGrantDefaultEndpointsForService = function(callback) {
  return function(data) {
    callback(data);
  };
};

var grantDefaultEndpointsForService = function(serviceName, endpoints, callback) {
  adminBL.getAllAdmins((err, success) => {
    if (err) {
      callback('err_admins_find');
    }

    for (var u = 0; u < success.length; u++) {
      for (var e = 0; e < endpoints.length; e++) {
        if (endpoints[e].allowedByDefault === true) {
          addEndpointPermission(success[u].id, serviceName, endpoints[e].uri, endpoints[e].method, logGrantDefaultEndpointsForService(callback));
        }
      }
    }
  });
};

var removeInvalidServiceAccess = function(callback) {
  adminBL.getAllAdmins((err, success) => {
    if (err) {
      callback('err_admins_find');
    }

    for (var u = 0; u < success.length; u++) {
      for (var e = 0; e < success[e].serviceAccess.length; e++) {
        // if endpoint not exist
          //removeEndpointPermission();
      }
    }
  });
};

module.exports = {
  'getEndpointPermissions': getEndpointPermissions,
  'addEndpointPermission': addEndpointPermission,
  'removeEndpointPermission': removeEndpointPermission,
  'grantDefaultEndpointsForService': grantDefaultEndpointsForService
};
