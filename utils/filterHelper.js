'use strict';

var ip = require('ip');

function filterNumericColumn(column, columnName) {
    global.ispravimeLogger.debug("executing filterIntColumn");
    const filterObject = {};
    if (column.search.value) {
        filterObject[columnName] = {
            comparison: '=',
            value: column.search.value
        };
    }

    // Match range.
    const regexRange = /(.*)\s+-\s+(.*)/g;
    let rangeMatch;
    while ((rangeMatch = regexRange.exec(column.search.value)) !== null) {
        if (rangeMatch.length == 3) {
            filterObject[columnName] = {
            from: rangeMatch[1],
            until: rangeMatch[2]
            };
        }
    }

    // Match comparison operators.
    const regexCompare = /\s*(<=|>=|<|>|!|=)\s*(.*)/g;
    let comparisonMatch;
    const allowedOperators = ['<', '<=', '>', '>=', '=', '!='];
    while ((comparisonMatch = regexCompare.exec(column.search.value)) !== null) {
        if (comparisonMatch.length == 3) {
            let range = comparisonMatch[2];
            if (allowedOperators.indexOf(comparisonMatch[1]) > -1) {
                filterObject[columnName] = {
                    comparison: comparisonMatch[1],
                    value: range
                };
            }
        }
    }

    return filterObject;
}

function filterTextColumn(column, columnName) {
    global.ispravimeLogger.debug("executing filterTextColumn");
    const filterObject = {};

    if (column.search.value) {
        //filterObject.userID = column.search.value;
        filterObject[columnName] = column.search.value;
    }

    return filterObject;
}

function filterDateColumn(column, columnName) {
    global.ispravimeLogger.debug("executing filterDateCoulmn");
    const filterObject = {};
    if (column.search.value) {
        filterObject[columnName] = {
            comparison: '=',
            value: new Date(column.search.value).toISOString()
        };
    }

    // Match range.
    const regexRange = /(.*)\s+-\s+(.*)/g;
    let rangeMatch;
    while ((rangeMatch = regexRange.exec(column.search.value)) !== null) {
        if (rangeMatch.length == 3) {
            filterObject[columnName] = {
            from: new Date(rangeMatch[1]).toISOString(),
            until: new Date(rangeMatch[2]).toISOString()
            };
        }
    }

    // Match comparison operators.
    const regexCompare = /\s*([<>!][=]*)\s*(.*)/g;
    let comparisonMatch;
    const allowedOperators = ['<', '<=', '>', '>=', '=', '!='];
    while ((comparisonMatch = regexCompare.exec(column.search.value)) !== null) {
        if (comparisonMatch.length == 3) {
            let date = comparisonMatch[2];
            if (allowedOperators.indexOf(comparisonMatch[1]) > -1) {
            filterObject[columnName] = {
                comparison: comparisonMatch[1],
                value: new Date(date).toISOString()
            };
            }
        }
    }

    return filterObject;
}

function filterIP(column, ipMin, ipMax, columnName) {
    global.ispravimeLogger.debug(`filterIP ${JSON.stringify(column)} ${ipMin} ${ipMax} ${columnName}`);
    const filterObject = {};
    if (column.search.value) {
        filterObject[columnName] = column.search.value;
    }

    if (ipMin || ipMax) {
        var iPMin = ipMin;
        var iPMax = ipMax;
        if (!iPMin) iPMin = '0.0.0.0';
        if (!iPMax) iPMax = '255.255.255.255';
        try {
            iPMin = ip.toString(ip.toBuffer(iPMin));
        } 
        catch (err) { 
            iPMin = '0.0.0.0';
        }

        try {
            iPMax = ip.toString(ip.toBuffer(iPMax));
        } 
        catch (err) { 
            iPMax = '255.255.255.255'; 
        }

        filterObject[columnName] = {
            from: iPMin,
            until: iPMax
        };
    }

    return filterObject;
}

module.exports.usersFiltering = function(query) {
    global.ispravimeLogger.debug("executing usersFiltering");
    if (!query.columns) {
        return {};
    }

    const userObject = filterTextColumn(query.columns[0], 'name');
    const firstAccessObject = filterDateColumn(query.columns[1], 'firstAccess');
    const lastAccessObject = filterDateColumn(query.columns[2], 'lastAccess');
    const numberOfRequestsObject = filterNumericColumn(query.columns[3], 'numberOfRequests');
    const numberOfMistakesObject = filterNumericColumn(query.columns[4], 'numberOfMistakes');
    const IPObject = filterIP(query.columns[5], query.ipMin, query.ipMax, 'lastIP');
    IPObject.onlyLatestIP = query.onlyLatestIP === 'true';
    return Object.assign({}, userObject, firstAccessObject, lastAccessObject, numberOfRequestsObject, numberOfMistakesObject, IPObject);
}

module.exports.usersSorting = function(query) {
    global.ispravimeLogger.debug("executing usersSorting");
    const sortObject = {};
    var order;
    try {
        order = +query.order[0].column;
    }
    catch(e) {
        order = 0;
    }
  
    var orderDirection;
    try {
        orderDirection = query.order[0].dir;
        const allowedDirections = ['asc', 'desc'];
        if (allowedDirections.indexOf(orderDirection) > -1) {
            sortObject.direction = orderDirection;
        }
        else {
            sortObject.direction = 'asc';
        }
    }
    catch(e) {
        sortObject.direction = 'asc';
    }
  
    switch(order) {
        case 0:
            sortObject.columnName = 'name'
            break;
        case 1:
            sortObject.columnName = 'firstaccess';
            break;
        case 2:
            sortObject.columnName = 'lastAccess';
            break;
        case 3:
            sortObject.columnName = 'numberofrequests';
            break;
        case 4:
            sortObject.columnName = 'numberofmistakes';
            break;
        case 5:
            sortObject.columnName = 'lastip';
            break;
        default:
            sortObject.columnName = 'numberofrequests';
            break;
    }
  
    return sortObject;
}

module.exports.requestsFiltering = function(query) {
    global.ispravimeLogger.debug("executing requestsFiltering");
    if (!query.columns) {
        return {};
    }

    const requestObject = filterDateColumn(query.columns[0], 'requesttime');
    const processingTimeObject = filterNumericColumn(query.columns[1], 'processingtime');
    const userObject = filterTextColumn(query.columns[2], 'name');
    const numberOfMistakesObject = filterNumericColumn(query.columns[3], 'numberofmistakes');
    const numberOfWordsObject = filterNumericColumn(query.columns[4], 'wordcount');

    return Object.assign({}, requestObject, processingTimeObject, userObject, numberOfMistakesObject, numberOfWordsObject);
}

module.exports.requestsSorting = function(query) {
    global.ispravimeLogger.debug("executing requestsSorting");
    const sortObject = {};
    var order;
    try {
        order = +query.order[0].column;
    }
    catch(e) {
        order = 0;
    }
  
    var orderDirection;
    try {
        orderDirection = query.order[0].dir;
        const allowedDirections = ['asc', 'desc'];
        if (allowedDirections.indexOf(orderDirection) > -1) {
            sortObject.direction = orderDirection;
        }
        else {
            sortObject.direction = 'asc';
        }
    }
    catch(e) {
        sortObject.direction = 'asc';
    }
  
    switch(order) {
        case 0:
            sortObject.columnName = 'requesttime'
            break;
        case 1:
            sortObject.columnName = 'processingtime';
            break;
        case 2:
            sortObject.columnName = 'name';
            break;
        case 3:
            sortObject.columnName = 'mistakecount';
            break;
        case 4:
            sortObject.columnName = 'wordcount';
            break;
        default:
            sortObject.columnName = 'requesttime';
            break;
    }
  
    return sortObject;
}

module.exports.mistakesFiltering = function(query) {
    global.ispravimeLogger.debug("executing mistakesFiltering");
    if (!query.columns) {
        return {};
    }

    const suspiciousObject = filterTextColumn(query.columns[0], 'suspicious');
    const severityObject = filterTextColumn(query.columns[1], 'severity');
    const countObject = filterNumericColumn(query.columns[2], 'occurrences');

    return Object.assign({}, suspiciousObject, severityObject, countObject);
}

module.exports.mistakesSorting = function(query) {
    global.ispravimeLogger.debug("executing mistakesSorting");
    const sortObject = {};
    var order;
    try {
        order = +query.order[0].column;
    }
    catch(e) {
        order = 0;
    }
  
    var orderDirection;
    try {
        orderDirection = query.order[0].dir;
        const allowedDirections = ['asc', 'desc'];
        if (allowedDirections.indexOf(orderDirection) > -1) {
            sortObject.direction = orderDirection;
        }
        else {
            sortObject.direction = 'asc';
        }
    }
    catch(e) {
        sortObject.direction = 'asc';
    }
  
    switch(order) {
        case 0:
            sortObject.columnName = 'suspicious'
            break;
        case 1:
            sortObject.columnName = 'severity';
            break;
        case 2:
            sortObject.columnName = 'occurrences';
            break;
        default:
            sortObject.columnName = 'occurrences';
            break;
    }
  
    return sortObject;
}

module.exports.adminsFiltering = function(query) {
    global.ispravimeLogger.debug("executing adminsFiltering");
    if (!query.columns) {
        return {};
    }

    const userIDObject = filterNumericColumn(query.columns[0], 'id');
    const emailObject = filterTextColumn(query.columns[1], 'email');
    const registrationDateObject = filterDateColumn(query.columns[2], 'dateregistered');
    const tokensObject = filterNumericColumn(query.columns[3], 'numtokens');

    return Object.assign({}, userIDObject, emailObject, registrationDateObject, tokensObject);
}

module.exports.adminsSorting = function(query) {
    global.ispravimeLogger.debug("executing adminsSorting");
    const sortObject = {};
    var order;
    try {
        order = +query.order[0].column;
    }
    catch(e) {
        order = 0;
    }
  
    var orderDirection;
    try {
        orderDirection = query.order[0].dir;
        const allowedDirections = ['asc', 'desc'];
        if (allowedDirections.indexOf(orderDirection) > -1) {
            sortObject.direction = orderDirection;
        }
        else {
            sortObject.direction = 'asc';
        }
    }
    catch(e) {
        sortObject.direction = 'asc';
    }
  
    switch(order) {
        case 0:
            sortObject.columnName = 'id'
            break;
        case 1:
            sortObject.columnName = 'email';
            break;
        case 2:
            sortObject.columnName = 'dateregistered';
            break;
        case 3:
            sortObject.columnName = 'numtokens';
            break;
        default:
            sortObject.columnName = 'dateregistered';
            break;
    }
  
    return sortObject;
}