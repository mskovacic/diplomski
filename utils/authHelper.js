'use strict';

const jwt = require('jsonwebtoken');
const appRoot = require('app-root-path');
const config = require('../config.js');
const adminBL = require(appRoot + '/bl/admin.js');
const serviceBL = require(appRoot + '/bl/services.js');
const tokenBL = require(appRoot + '/bl/token.js');

var apiTokenPermissionsCheck = function(req, res, next) {
  global.ispravimeLogger.debug("apiTokenPermissionsCheck");
  // Get token.
  const apiToken = req.headers['authorization'] || req.headers['Authorization'];
  if (!apiToken) {
    res.status(403);
    return res.json({
      'success': false,
      'response': 'no_token_provided'
    });
  }

  // Verify permissions.
  jwt.verify(apiToken, config.secretPublic, { algorithm: 'RS256' }, function(errToken, token) {
    if (errToken) {
      // Throw error.
      res.status(403);
      return res.json({
        'success': false,
        'response': errToken
      });
    }

    if (token.tokenType == 'adminToken') {
      // Admin token always passes.
      return next();
    }

    // Check if revoked.
    tokenBL.getRevokedToken(apiToken, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
      }

      if (success.length > 0) {
        res.status(403);
        return res.json({
          'success': false,
          'response': 'token_revoked',
          'revokedAt': success[0].revokedAt,
          'revocationReason': success[0].revocationReason
        });
      }

      var hostAllowed = false;
      var allowedHosts = token.allowedHosts;
      var clientHost = req.headers['host'].replace(/:.*$/, '');
      for (var i in allowedHosts) {
        if (allowedHosts[i] == clientHost) {
          hostAllowed = true;
          break;
        }
      }

      if (hostAllowed) {
        // Check permissions.
        var tokenPermissions = token.endpoints;
        var requestedUri = req.originalUrl.match('^[^?]*')[0];
        var requestedMethod = req.method;
        var hasAccess = false;
        for (var j in tokenPermissions) {
          var tokenPermission = tokenPermissions[j];
          if (tokenPermission.method == requestedMethod && tokenPermission.uri == requestedUri)
          {
            hasAccess = true;
            break;
          }
        }

        if (hasAccess) {
          return next();
        }

        res.status(403);
        return res.json({
          'success': false,
          'response': 'access_denied'
        });
      }

      res.status(403);
      return res.json({
        'success': false,
        'response': 'client_host_disallowed',
        'allowedHosts': allowedHosts
      });
    });
  });
};

var sameAdminCheck = function(checkType) {
  return function(req, res, next) {
    var adminId = null;
    if (typeof checkType === 'undefined' || checkType === null) {
      adminId = req.params.id;
    }
    else {
      switch (checkType) {
        case 'adminId': adminId = req.params.adminId;
      }
    }

    var adminToken = req.headers['authorization'] || req.headers['Authorization'];

    // Compare Tokens.
    jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(errUToken, uToken) {
      if (errUToken) {
        // Throw error.
        res.status(403);
        return res.json({
          'success': false,
          'response': 'admin_token_invalid'
        });
      }
      else {
        var isSameAdmin = false;
        if (uToken.roles.indexOf('admin') > -1) {
          isSameAdmin = true;
        }
        else if (uToken.adminId == adminId) {
          isSameAdmin = true;
        }
        if (isSameAdmin) {
          next();
        }
        else {
          // No rights
          res.status(403);
          return res.json({
            'success': false,
            'response': 'no_rights'
          });
        }
      }
    });
  };
};

var sameAdminCheckRevocation = function(req, res, next) {
  var revokeToken = req.params.rtoken;
  var adminToken = req.headers['authorization'] || req.headers['Authorization'];

  // Compare Tokens.
  jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(errUToken, uToken) {
    if (errUToken) {
      // Throw error.
      res.status(403);
      return res.json({
        'success': false,
        'response': 'admin_token_invalid'
      });
    }
    else {
      jwt.verify(revokeToken, config.secretPublic, { algorithm: 'RS256' }, function(errRToken, rToken) {
        if (errRToken) {
          // Throw error.
          res.status(403);
          return res.json({
            'success': false,
            'response': 'revocation_token_invalid'
          });
        }
        else {
          if (rToken.tokenType != 'apiAccess') {
            // Can only revoke API access tokens.
            res.status(403);
            return res.json({
              'success': false,
              'response': 'token_expect_api'
            });
          }

          var canRevoke = false;
          if (uToken.roles.indexOf('admin') > -1) {
            canRevoke = true;
          }
          else if (uToken.adminId == rToken.adminId) {
            canRevoke = true;
          }
          if (canRevoke) {
            req.revocationRequester = uToken.adminId;
            req.tokenOwner = rToken.adminId;
            req.revokedUntil = rToken.exp;
            next();
          }
          else {
            // No revocation rights
            res.status(403);
            return res.json({
              'success': false,
              'response': 'no_revocation_rights'
            });
          }
        }
      });
    }
  });
};

var roleCheck = function(roles) {
  return function(req, res, next) {
    global.ispravimeLogger.debug('executing roleCheck');
    const apiToken = req.headers['authorization'] || req.headers['Authorization'];

    // Check if token is revoked.
    tokenBL.getRevokedToken(apiToken, (err, success) => {
      const rtoken = success[0];
      if (rtoken) {
        global.ispravimeLogger.debug('token_revoked');
        res.status(403);
        return res.json({
          'success': false,
          'response': 'token_revoked',
          'revokedAt': rtoken.revokedat,
          'revocationReason': rtoken.revocationreason
        });
      }

      jwt.verify(apiToken, config.secretPublic, { algorithm: 'RS256' }, function(errApiToken, apiToken) {
        if (errApiToken) {
          global.ispravimeLogger.error(errApiToken);
          res.status(403);
          return res.json({
            'success': false,
            'response': 'token_invalid'
          });
        }

        var adminId = apiToken.adminId;
        adminBL.getAdminDetails(adminId, (err, success) => {
          if (err || !success) {
            global.ispravimeLogger.error(err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_admin_find'
            });
          }

          if (roles === undefined) {
            // No argument supplied, check for allowed roles in DB.
            var hasAllowedRole = false;
            var requestedUri = req.originalUrl.match('^[^?]*')[0];
            var requestedMethod = req.method;
            serviceBL.getAllowedRolesForEndpoint(requestedMethod, requestedUri, (err, allowedRoles) => {
              if (err) {
                global.ispravimeLogger.error(err);
                res.status(503);
                return res.json({
                  'success': false,
                  'response': 'err_roles_find'
                });
              }

              if (allowedRoles.length == 0) {
                return next();
              } else {
                adminBL.getAdminRoles(adminId, (err, adminRoles) =>{
                  if (err) {
                    global.ispravimeLogger.error(err);
                    res.status(503);
                    return res.json({
                      'success': false,
                      'response': 'err_roles_find'
                    });
                  }

                  if (adminRoles.filter((n) => allowedRoles.includes(n)).length > 0) {
                    // Admin has at least one of allowed roles.
                    return next();
                  }

                  if (!hasAllowedRole) {
                    global.ispravimeLogger.error(err);
                    res.status(403);
                    return res.json({
                      'success': false,
                      'response': 'no_allowed_roles',
                      'allowed_roles': allowedRoles
                    });
                  }
                });
              }
            });
          }

          // List of allowed roles is supplied as an argument.
          var hasAllowedRole = false;          
          adminBL.getAdminRoles(adminId, (err, adminRoles) =>{
            if (err) {
              global.ispravimeLogger.error(err);
              res.status(503);
              return res.json({
                'success': false,
                'response': 'err_roles_find'
              });
            }

            var isArray = Array.isArray(roles);
            if (isArray) {
              // If supplied argument is a list of roles.
              if (adminRoles.filter((n) => roles.includes(n)).length > 0) {
                return next();
              }
            } 
              
            // If supplied argument is only one role (string).
            if (typeof roles === 'string' || roles instanceof String) {
              if (adminRoles.indexOf(roles) > -1) {
                return next();
              }
            }

            global.ispravimeLogger.error('no_allowed_roles');
            res.status(403);
            return res.json({
              'success': false,
              'response': 'no_allowed_roles',
              'allowed_roles': roles
            });
          });
        });
      });
    });
  };
};

module.exports = {
  'apiTokenPermissionsCheck': apiTokenPermissionsCheck,
  'sameAdminCheck': sameAdminCheck,
  'sameAdminCheckRevocation': sameAdminCheckRevocation,
  'roleCheck': roleCheck
};
