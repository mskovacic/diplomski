var fs = require('fs');
var appRoot = require('app-root-path');

module.exports = {
  'version': '1.0.0',
  //'listenAddress': '161.53.19.114',
  'listenAddress': 'localhost',
  'maxCPU': 1,
  //'hostname': 'statistika.ispravi.me',
  'httpPort': '8080',
  'httpsPort': '8443',
  'useCompression': false,
  'useRedis': false,
  'cacheDebugging': false,
  'onlyApi': false,
  'logLevel': 'debug',
  'listenOnExternalAddress': false,
  'secretPrivate': fs.readFileSync(appRoot + '/tokenKeys/private_unencrypted.pem'),
  'secretPublic': fs.readFileSync(appRoot + '/tokenKeys/public.pem'),
  'adminTokenExpire': '1d',
  'resetTokenExpire': '1h',
  'apiTokenExpire': '365d',
  'cacheDuration': '30 minutes',
  'defaultPageSize': 10,
  'database': 'mongodb://diplomski:diplomski@161.53.19.115:27017/diplomski?authSource=diplomski&socketTimeoutMS=1000000&connectTimeoutMS=1000000',
  'databaseTest': 'mongodb://diplomski:diplomski@161.53.19.115:27017/diplomskiTest?authSource=diplomski',
  'reconnectTimeout': 5000,
  'email': {
    'connection': 'smtps://postmaster@fiouch.com:BYBYrdDE@smtp.mailgun.org',
    'from': '"Ispravi.me Statistika" <no-reply@ispravi.me>'
  },
  'sslKey': fs.readFileSync('./ssl/server.key'),
  'sslCert': fs.readFileSync('./ssl/server.crt'),
  'sslCACert': fs.readFileSync('./ssl/ca.crt'),
  'textFilesDir': '/home/log/ispravi.me/input',
  'logDir': appRoot + '/logs',
  'testUA': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
  //'postgresqlDB': 'postgresql://postgres:postgres@localhost/db1'
  //'postgresqlDB': 'Driver={PostgreSQL};Server=localhost;Port=5432;Database=db1;Uid=postgres;Pwd=postgres;'
  'postgresqlDB':{
    database: 'db1',
    port: 5432,
    host: 'localhost',
    user: 'postgres',
    password: 'postgres'
  },
  'postgresqlDBTest' : {
    database: 'db1_test',
    port: 5432,
    host: 'localhost',
    user: 'postgres',
    password: 'postgres'
  }
  // 'postgresqlDB':{
  //   database: 'db1',
  //   port: 5432,
  //   host: '161.53.19.115',
  //   user: 'postgres',
  //   password: 'postgres'
  // }
};
