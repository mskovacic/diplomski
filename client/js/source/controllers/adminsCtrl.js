angular.module('diplomski-fvoska').controller('adminsCtrl', ['$scope', '$rootScope', '$location', '$stateParams', 'cfpLoadingBar', function($scope, $rootScope, $location, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $('#adminNotAdminModal').on('hidden.bs.modal', function () {
    document.location = '/';
  });

  $scope.start();

  $(document).ready(function() {
    adminCheck(function(isAdmin) {
      $scope.isAdmin = isAdmin;
      if (!isAdmin) {
        $('#adminNotAdminModal').modal('show');
        setTimeout(function() {
          $scope.complete();
        }, config.artificialDelay);
      }
      else {
        if ($stateParams.id) {
          // admin details.
          checkToken();
          var adminPath = config.apiUrl + 'admins/details/' + $stateParams.id;
          $.ajax({
            method: 'GET',
            contentType: 'application/json',
            url: adminPath,
            success: function(data) {
              $scope.$apply(function() {
                $scope.adminDetails = data.response;
                $rootScope.titleDetail = ' - ' + data.response.email;
              });
            },
            error: function(data) {
              console.log(data);
            },
            complete: function() {
              setTimeout(function() {
                $scope.complete();
              }, config.artificialDelay);
            }
          });
        }
        else {
          // admins list.
          var table;

          // Header translations.
          $('#dataTables-admins thead th').each(function() {
            var txt = $(this).text();
            $(this).html(trans(txt));
          });
          // Footer translations.
          $('#dataTables-admins tfoot th').each(function() {
            var txt = $(this).text();
            if (txt != 'actions') {
              $(this).html('<input type="search" class="form-control input-sm footer-search" placeholder="' + trans('filter_by') + ' \'' + trans(txt) + '\'" />');
            }
            else {
              $(this).html('');
            }
          });

          // Create DataTable.
          table = $('#dataTables-admins').DataTable({
            'order': [
              [3, 'desc']
            ],
            'responsive': true,
            'language': {
              'lengthMenu': trans('lengthMenu'),
              'search': trans('search'),
              'zeroRecords': trans('zeroRecords'),
              'info': trans('info'),
              'infoEmpty': trans('infoEmpty'),
              'infoFiltered': trans('infoFiltered'),
              'processing': trans('processing'),
              'paginate': {
                'first': trans('first'),
                'last': trans('last'),
                'next': trans('next'),
                'previous': trans('previous')
              }
            },
            'processing': true,
            'serverSide': true,
            'ajax': {
              'url': config.apiUrl + 'admins',
              'type': 'GET',
              'beforeSend': function (request) {
                $scope.start();
                request.setRequestHeader('Authorization', localStorage.getItem('token'));
              },
              'dataSrc': function(json) {
                $scope.complete();
                return json.response;
              }
            },
            'columns': [{
              'data': 'id'
            }, {
              'data': 'email'
            }, {
              'data': 'dateregistered'
            }, {
              'data': 'numtokens'
            }, {
              'data': 'button'
            }],
            'columnDefs': [{
              'targets': [-1],
              'orderable': false,
              'render': function(data, type, full, meta) {
                return '<a href="/admins/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
              }
            }]
          });

          // Set search handler.
          table.columns().every(function() {
            var that = this;
            $('input', this.footer()).on('keydown', function(ev) {
              if (ev.keyCode == 13) {
                that
                  .search(this.value)
                  .draw();
              }
            });
          });
        }
      }
    });
  });
}]);
