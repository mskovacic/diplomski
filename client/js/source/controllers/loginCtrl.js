angular.module('diplomski-fvoska').controller('loginCtrl', ['$scope', '$rootScope', 'cfpLoadingBar', function($scope, $rootScope, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $(document).ready(function() {
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    localStorage.removeItem('adminId');

    setTimeout(function() {
      $scope.complete();
    }, config.artificialDelay);

    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    function getToken(email, password) {
      var resourceUrl = config.apiUrl + 'admins/auth';
      $.ajax({
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          'email': email,
          'password': password
        }),
        url: resourceUrl,
        success: function(data) {
          if (data.success === true) {
            // Got token
            localStorage.setItem('email', email);
            localStorage.setItem('token', data.token);
            localStorage.setItem('adminId', data.adminId);
            document.location = '/panel';
          } else if (data.success === false) {
            $('#loginDanger').html(data.response);
            $('#loginDanger').slideDown(config.slideDownSpeed);
          } else {
            console.log(data);
          }
        },
        error: function(data) {
          var res = JSON.parse(data.responseText);
          $('#loginDanger').html(trans(res.response));
          $('#loginDanger').slideDown(config.slideDownSpeed);
        },
        complete: function() {
          adminCheck(function(isAdmin) {
            $rootScope.isAdmin = isAdmin;
          });
          setTimeout(function() {
            $('.btn-ajax').prop('disabled', false);
          }, config.slideDownSpeed);
        }
      });
    }

    function submit() {
      $('.alert').slideUp(config.slideUpSpeed);
      $('.btn-ajax').prop('disabled', true);
      var email = $('#loginEmail').val();
      var pwd = $('#loginPassword').val();
      getToken(email, pwd);
    }

    $('input').keypress(function (e) {
      if (e.which == 13) {
        submit();
        return false;
      }
    });

    $('#loginContainer #login-submit').click(function() {
      submit();
    });
  });
}]);
