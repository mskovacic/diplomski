angular.module('diplomski-fvoska').controller('panelCtrl', ['$scope', 'cfpLoadingBar', function($scope, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $(document).ready(function() {
    checkToken();

    var apiPath = config.apiUrl + 'admins/roles/' + localStorage.getItem('adminId');
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: apiPath,
      success: function(data) {
        if (data.success === true) {
          $scope.$apply(function() {
            $scope.adminDetails = {
              "roles": data.response,
              "id": localStorage.getItem('adminId')
            };
          });
        } else {
          console.log(data);
        }
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        setTimeout(function() {
          $scope.complete();
        }, config.artificialDelay);
      }
    });

    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    $('#logout-submit').click(function () {
      logOut();
    });
  });
}]);
