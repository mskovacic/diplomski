angular.module('diplomski-fvoska').controller('tokensCtrl', ['$scope', '$state', '$stateParams', 'cfpLoadingBar', function($scope, $state, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  if (!$scope.isAdmin) {
    myTokens();
  }
  else {
    getAdmins();
  }

  $scope.numHosts = 1;
  $scope.minHosts = 1;
  $scope.maxHosts = 7;
  $scope.canAdd = true;
  $scope.canRemove = false;
  $scope.specificAdmin = $stateParams.admin;

  $scope.endpointRowClass = function(endpoint) {
    if (endpoint.noAuthRequired === true) {
      return 'tableEndpoint noAuthRequired';
    } else if (endpoint.adminHasAccess === true) {
      return 'tableEndpoint hasAccess';
    } else {
      return 'tableEndpoint noAccess';
    }
  };
  $scope.endpointChkDisabled = function(endpoint) {
    if (endpoint.noAuthRequired === true) {
      return false;
    } else if (endpoint.adminHasAccess === true) {
      return false;
    } else {
      return true;
    }
  };

  $scope.getNumHosts = function(n) {
    return new Array(n);
  };

  function chkButtons() {
    if ($scope.numHosts >= $scope.maxHosts) {
      $scope.numHosts = $scope.maxHosts;
      $scope.canAdd = false;
    }
    else {
      $scope.canAdd = true;
    }
    if ($scope.numHosts <= $scope.minHosts) {
      $scope.numHosts = $scope.minHosts;
      $scope.canRemove = false;
    }
    else {
      $scope.canRemove = true;
    }
  }

  $scope.hostAdd = function() {
    if ($scope.canAdd) {
      if ($scope.numHosts < $scope.maxHosts) {
        $scope.numHosts++;
      }
      chkButtons();
    }
  };
  $scope.hostRemove = function() {
    if ($scope.canRemove) {
      if ($scope.numHosts > $scope.minHosts) {
        $scope.numHosts--;
      }
      chkButtons();
    }
  };

  function getAdmins() {
    checkToken();
    var getAdminsPath = config.apiUrl + 'admins/emails';
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: getAdminsPath,
      success: function(data) {
        $scope.$apply(function() {
          $scope.admins = data.response;
          if ($stateParams.admin) {
            $('.nav-tabs a[href="#tokenList"]').tab('show');
            getAdminTokensById($stateParams.admin);
            $('#adminSelect').val($stateParams.admin);
            $scope.selectedAdmin = $stateParams.admin;
          }
          else {
            //myTokens();
          }
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  }

  function getAdminTokensById(id) {
    $scope.start();
    $('.btn-ajax').prop('disabled', true);
    checkToken();
    var getTokensPath = config.apiUrl + 'admins/' + id + '/tokens';
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: getTokensPath,
      success: function(data) {
        $scope.$apply(function() {
          $scope.tokens = data.response;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        $scope.complete();
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
    });
  }

  function getAdminId(callback) {
    checkToken();
    var tokenInfoPath = config.apiUrl + 'admins/auth/' + localStorage.getItem('token');
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: tokenInfoPath,
      success: function(data) {
        callback(data.response.adminId);
      },
      error: function(data) {
        console.log(data);
      }
    });
  }

  function myTokens() {
    getAdminId(function(id) {
      getAdminTokensById(id);
      $scope.$apply(function() {
        $scope.selectedAdmin = id;
        $scope.changeAdmin(id);
      });
    });
  }

  $scope.changeAdmin = function(id) {
    var admId = $('#adminSelect').val() || id;
    $state.go('tokens.viewTokensHere', {admin: admId});
  };

  $scope.getTokenDetails = function(token, admin) {
    var apiPath = config.apiUrl + 'token/' + token;
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: apiPath,
      success: function(data) {
        $scope.$apply(function() {
          $scope.tokenDetails = {};
          $scope.tokenDetails.token = token;
          $scope.tokenDetails.admin = data.response.adminId;
          $scope.tokenDetails.tokenType = data.response.tokenType;
          $scope.tokenDetails.iat = new Date(data.response.iat * 1000);
          $scope.tokenDetails.exp = new Date(data.response.exp * 1000);
          $scope.tokenDetails.endpoints = data.response.endpoints;
          $scope.tokenDetails.hosts = data.response.allowedHosts;
          $scope.tokenDetails.adminId = data.response.adminId;
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  };

  $scope.createToken = function() {
    $('.alert-token-creation').slideUp(config.slideUpSpeed);
    $('.btn-ajax').prop('disabled', true);
    checkToken();
    var allowedHosts = [];
    $('.allowedHosts').each(function() {
      var h = $(this).val();
      if (h !== '') {
        allowedHosts.push(h);
      }
    });
    var requestedEndpoints = [];
    $('.endpointChk:checked').each(function() {
      var json = $(this).attr('value').replace(/'/g, '"');
      requestedEndpoints.push(JSON.parse(json));
    });
    var putTokenPath = config.apiUrl + 'token';
    var sendData = {
      'endpoints': requestedEndpoints,
      'clientHosts': allowedHosts
    };
    $.ajax({
      method: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify(sendData),
      url: putTokenPath,
      success: function(data) {
        if (data.success === true) {
          $('#tokenSuccess').html(trans(data.response));
          $('#tokenSuccess').slideDown(config.slideDownSpeed);

          $scope.$apply(function() {
            $scope.getTokenDetails(data.token);
            $('#newTokenDetailsModal').modal('show');
          });
        } else if (data.success === false) {
          console.log(data);
        } else {
          console.log(data);
        }
      },
      error: function(data) {
        try {
          var res = JSON.parse(data.responseText);
          $('#tokenAlert').html(trans(res.response));
          $('#tokenAlert').slideDown(config.slideDownSpeed);
        } catch (e) {
          $('#tokenAlert').html(trans('err_generic'));
          $('#tokenAlert').slideDown(config.slideDownSpeed);
        }
      },
      complete: function() {
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
    });
  };

  $(document).ready(function() {
    $('.nav-tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    checkToken();
    var apiPath = config.apiUrl + '';
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: apiPath,
      success: function(data) {
        if (data.success === true) {
          $scope.$apply(function() {
            $scope.availableServices = data.services;
          });
        } else if (data.success === false) {
          console.log(data);
        } else {
          console.log(data);
        }
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        setTimeout(function() {
          $scope.complete();
        }, config.artificialDelay);
      }
    });
  });
}]);
