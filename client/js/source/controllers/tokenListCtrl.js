angular.module('diplomski-fvoska').controller('tokenListCtrl', ['$scope', '$stateParams', 'cfpLoadingBar', function($scope, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  function getAdminTokensById(id) {
    $scope.start();
    $('.btn-ajax').prop('disabled', true);
    checkToken();
    var getTokensPath = config.apiUrl + 'admins/' + id + '/tokens';
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: getTokensPath,
      success: function(data) {
        $scope.$apply(function() {
          $scope.tokens = data.response;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        $scope.complete();
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
    });
  }

  $scope.getTokenDetails = function(token, admin) {
    var apiPath = config.apiUrl + 'token/' + token;
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: apiPath,
      success: function(data) {
        $scope.$apply(function() {
          console.log(data.response);
          $scope.tokenDetails = {};
          $scope.tokenDetails.token = token;
          $scope.tokenDetails.admin = data.response.adminid;
          $scope.tokenDetails.tokenType = data.response.tokenType;
          $scope.tokenDetails.iat = new Date(data.response.iat * 1000);
          $scope.tokenDetails.exp = new Date(data.response.exp * 1000);
          $scope.tokenDetails.endpoints = data.response.endpoints;
          $scope.tokenDetails.hosts = data.response.allowedHosts;
          $scope.tokenDetails.adminId = data.response.adminId;
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  };

  $scope.tokenRevoke = function(token, admin) {
    if($scope.isAdmin);
    $('.btn-ajax').prop('disabled', true);
    checkToken();
    var apiPath = config.apiUrl + 'token/revoke/' + token;
    $.ajax({
      method: 'PUT',
      contentType: 'application/json',
      url: apiPath,
      success: function(data) {
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
        getAdminTokensById($scope.adminId);
      }
    });
  };

  $scope.refresh = function() {
    getAdminTokensById($scope.adminId);
  };

  $(document).ready(function() {
    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    $scope.adminId = $stateParams.admin;
    getAdminTokensById($stateParams.admin);
  });
}]);
