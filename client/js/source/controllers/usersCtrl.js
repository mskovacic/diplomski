angular.module('diplomski-fvoska').controller('usersCtrl', ['$scope', '$rootScope', '$compile', '$stateParams', '$location', 'cfpLoadingBar', function($scope, $rootScope, $compile, $stateParams, $location, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };
  $scope.complete = function() {
    cfpLoadingBar.complete();
  };
  $scope.start();

  $scope.userID = $stateParams.id;
  if ($scope.userID) {
    $rootScope.titleDetail = ' ' + $scope.userID;
  }

  function seconds2days(seconds) {
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours = hours - (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

    return {
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds.toFixed(0)
    };
  }

  function long2ip(ip) {
    //  discuss at: http://phpjs.org/functions/long2ip/
    // original by: Waldo Malqui Silva
    //   example 1: long2ip( 3221234342 );
    //   returns 1: '192.0.34.166'

    if (!isFinite(ip))
      return false;

    return [ip >>> 24, ip >>> 16 & 0xFF, ip >>> 8 & 0xFF, ip & 0xFF].join('.');
  }

  function ip2long(IP) {
    //  discuss at: http://phpjs.org/functions/ip2long/
    // original by: Waldo Malqui Silva
    // improved by: Victor
    //  revised by: fearphage (http://http/my.opera.com/fearphage/)
    //  revised by: Theriault
    //   example 1: ip2long('192.0.34.166');
    //   returns 1: 3221234342
    //   example 2: ip2long('0.0xABCDEF');
    //   returns 2: 11259375
    //   example 3: ip2long('255.255.255.256');
    //   returns 3: false

    var i = 0;
    // PHP allows decimal, octal, and hexadecimal IP components.
    // PHP allows between 1 (e.g. 127) to 4 (e.g 127.0.0.1) components.
    IP = IP.match(
      /^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i
    ); // Verify IP format.
    if (!IP) {
      return false; // Invalid format.
    }
    // Reuse IP variable for component counter.
    IP[0] = 0;
    for (i = 1; i < 5; i += 1) {
      IP[0] += !!((IP[i] || '')
        .length);
      IP[i] = parseInt(IP[i]) || 0;
    }
    // Continue to use IP for overflow values.
    // PHP does not allow any component to overflow.
    IP.push(256, 256, 256, 256);
    // Recalculate overflow of last component supplied to make up for missing components.
    IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
    if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) {
      return false;
    }
    return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
  }

  function cidrToRange(cidr) {
    var range = [2];
    cidr = cidr.split('/');
    if (cidr.length < 2) {
      range[0] = cidr[0];
      range[1] = cidr[0];
      return range;
    }
    var cidr_1 = parseInt(cidr[1]);
    range[0] = long2ip((ip2long(cidr[0])) & ((-1 << (32 - cidr_1))));
    start = ip2long(range[0]);
    range[1] = long2ip(start + Math.pow(2, (32 - cidr_1)) - 1);
    return range;
  }

  var getNetworks = function(cb) {
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'networks',
      success: function(data) {
        cb(data.response);
      },
      error: function(data) {
        cb(false);
      }
    });
  };

  function createTable() {
    // Header translations.
    // $('#dataTables-users thead th').each(function() {
    //   var title = $(this).text();
    //   $(this).html(trans(title));
    // });
    // Footer translations.
    $('#dataTables-users tfoot th').each(function() {
      var title = $(this).text();
      if (title == 'last_ip') {
        $(this).html('<div class="IPSearchContainer" ng-hide="enteredIPMin || enteredIPMax"><input id="lastIPSerach" ng-model="enteredIP" type="search" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" /></div>');
        $(this).append('<div class="IPSearchContainer IPSCs" ng-hide="enteredIP"><span class="IPSearchLabel">' + trans('ip_min') + '</span><input id="IPRangeMin" ng-model="enteredIPMin" type="search" class="form-control input-sm footer-search" placeholder="0.0.0.0" /></div>');
        $(this).append('<div class="IPSearchContainer IPSCs" ng-hide="enteredIP"><span class="IPSearchLabel">' + trans('ip_max') + '</span><input id="IPRangeMax" ng-model="enteredIPMax" type="search" class="form-control input-sm footer-search" placeholder="255.255.255.255" /></div>');
        $(this).append('<div class="IPSearchContainer" ng-hide="enteredIP"><label for="selectNet"><span class="IPSearchLabel">' + trans('ip_net') + '</span></label><select class="form-control" id="selectNet"><option ng-repeat="net in nets" value="{{ net.min }}-{{ net.max }}">{{ net.name }} {{ net.mask }}</option></select></div>');
        $(this).append('<div class="IPSearchContainer"><div class="checkbox SearchChk"><label><input id="ChkLatestIP" type="checkbox" value="" checked>' + trans('only_latest') + '</label></div></div>');
        $compile($('.IPSearchContainer'))($scope);
      } else if (title == 'first_appear' || title == 'last_appear') {
        $(this).html('<datepicker button-prev="<i class=\'fa fa-arrow-left\'></i>" button-next="<i class=\'fa fa-arrow-right\'></i>" date-format="yyyy-MM-dd" class="datepicker-search">' +
            '<input type="text" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" />' +
        '</datepicker>');
        $compile($('.datepicker-search'))($scope);
      } else if (title != 'actions') {
        $(this).html('<input type="search" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" />');
      }
    });

    // Create DataTable.
    table = $('#dataTables-users').DataTable({
      'order': [
        [3, 'desc']
      ],
      'responsive': true,
      'language': {
        'lengthMenu': trans('lengthMenu'),
        'search': trans('search'),
        'zeroRecords': trans('zeroRecords'),
        'info': trans('info'),
        'infoEmpty': trans('infoEmpty'),
        'infoFiltered': trans('infoFiltered'),
        'processing': trans('processing'),
        'paginate': {
          'first': trans('first'),
          'last': trans('last'),
          'next': trans('next'),
          'previous': trans('previous')
        }
      },
      'processing': true,
      'serverSide': true,
      'ajax': {
        'url': config.apiUrl + 'users',
        'type': 'GET',
        'cache': false,
        'data': function(d) {
          delete d.draw; // Don't append draw order, we cache server-side.
          d.ipMin = $('#IPRangeMin').val();
          d.ipMax = $('#IPRangeMax').val();
          d.onlyLatestIP = $('#ChkLatestIP').is(':checked');
        },
        'beforeSend': function(request) {
          $scope.start();
        },
        'dataSrc': function(json) {
          $scope.complete();
          $scope.$apply(function() {
            $scope.numUsers = json.response.length;
            $scope.numRequests = json.count.numRequests;
            $scope.numMistakesTotal = json.count.numMistakesTotal;
            $scope.numUniqueMistakes = json.count.numUniqueMistakes;
            if (json.count.avgRequestTime) {
              $scope.avgProcessingTime = json.count.avgRequestTime;
            }
            else {
              $scope.avgProcessingTime = 0;
            }
            if (json.count.totalRequestTime) {
              $scope.totalRequestTime = json.count.totalRequestTime;
              $scope.totalRequestTimeDays = seconds2days(json.count.totalRequestTime);
            }
            else {
              $scope.totalRequestTime = 0;
              $scope.totalRequestTimeDays = 0;
            }
          });
          return json.response;
        }
      },
      'columns': [{
        'data': 'userId'
      }, {
        'data': 'firstaccess'
      }, {
        'data': 'lastaccess'
      }, {
        'data': 'numberofrequests'
      }, {
        'data': 'numberofmistakes'
      }, {
        'data': 'lastip'
      }, {
        'data': 'button'
      }],
      'columnDefs': [{
        'targets': 0,
        'render': function(data, type, full, meta) {
          var userText = full.id;
          if (full.name) {
            userText = full.name;
          }
          return '<a href="users/' + full.id + '">' + userText + '</a>';
        }
      },
      {
        'targets': 5,
        'render': function(data, type, full, meta) {
          var networkName = false;
          var ipNumber = ip2long(full.lastip);
          for (var n = 0; n < $scope.nets.length; n++) {
            var net = $scope.nets[n];
            if (ipNumber >= net.minNumber && ipNumber <= net.maxNumber) {
              networkName = net.name;
              break;
            }
          }
          var userText = full.userid;
          if (full.name) {
            userText = full.name;
          }
          if (networkName) {
            return full.lastip + ' <span class="ipName">(' + networkName + ')</span>';
          }
          else {
            return full.lastip;
          }
        }
      },
      {
        'orderable': false,
        'targets': -1,
        'render': function(data, type, full, meta) {
          return '<a href="users/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
        }
      }]
    });

    $('#selectNet').change(function() {
      var that = this;
      $scope.$apply(function() {
        $scope.enteredIPMin = $(that).val().split('-')[0];
        $scope.enteredIPMax = $(that).val().split('-')[1];
        setTimeout(function() {
          table.draw();
        }, 100);
      });
    });

    // Set search handler.
    $('.footer-search').on('keyup', function(ev) {
      var searchElement = this;
      table.columns().every(function() {
        var column = this;
        var footer = $(column.footer()).find('input');
        column.search($(footer).val());
      });
    });
    table.columns().every(function() {
      var that = this;
      $('input', this.footer()).on('keydown', function(ev) {
        var id = $(this).attr('id');
        if (id == 'IPRangeMin' || id == 'IPRangeMax') {
          if (ev.keyCode == 13) {
            table.draw();
          }
        } else {
          if (ev.keyCode == 13) {
            that
              .search(this.value)
              .draw();
          }
        }
      });
    });
  }

  $scope.nets = [{
    'name': '-',
    'min': '',
    'max': '',
    'mask': ''
  }];

  $(document).ready(function() {
    var location = $location.$$path.split('/');
    var table;
    switch (location[location.length - 1]) {
      // Users list view.
      case 'users':
        // Get networks.
        getNetworks(function(nets) {
          if (nets) {
            $scope.$apply(function() {
              for (var i = 0; i < nets.length; i++) {
                var range = cidrToRange(nets[i].mask);
                $scope.nets.push({
                  'name': nets[i].name,
                  'min': range[0],
                  'minNumber': ip2long(range[0]),
                  'max': range[1],
                  'maxNumber': ip2long(range[1]),
                  'mask': nets[i].mask
                });
              }
              createTable();
            });
          }
          else {
            createTable();
          }
        });
        $scope.complete();
        break;
      case 'requests':
        // User's requests view.
        break;
      default:
        // User's details.
        google.charts.load('current', {'packages':['corechart']});
        $scope.toggleReqs = function() {
          if (!$scope.seeRequest) {
            $scope.seeRequest = true;
            $('#users-requests').slideDown(config.slideDownSpeed);
            $('#reqs-toggler').html(trans('hide'));
          }
          else {
            $scope.seeRequest = false;
            $('#users-requests').slideUp(config.slideDownSpeed);
            $('#reqs-toggler').html(trans('open_here'));
          }
        };
        $scope.changeUserName = function(id, name) {
          $('.btn-ajax').prop('disabled', true);
          $.ajax({
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify({
              'name': name
            }),
            url: config.apiUrl + 'users/' + id,
            success: function(data) {
              if (data.success === true) {
                $scope.$apply(function() {
                  $scope.userDetails.name = data.response.name;
                });
              }
            },
            error: function(data) {
              console.log(data);
              $scope.$apply(function() {
                $scope.userDetails.name = false;
              });
            },
            complete: function() {
              setTimeout(function() {
                $('.btn-ajax').prop('disabled', false);
              }, config.artificialDelay);
            }
          });
        };
        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'users/' + $scope.userID,
          success: function(data) {
            console.log(data);
            if (data.response) {
              $scope.$apply(function() {
                data.response.firstAccess = new Date(data.response.firstaccess);
                data.response.lastAccess = new Date(data.response.lastaccess);
                $scope.userDetails = data.response;
                $scope.userDetails.lastIP = {
                  'ip': data.response.lastip,
                  'network': false
                };
                getNetworks(function(nets) {
                  var rangedNets = [];
                  if (nets) {
                    for (var n = 0; n < nets.length; n++) {
                      var range = cidrToRange(nets[n].mask);
                      rangedNets.push({
                        'name': nets[n].name,
                        'min': range[0],
                        'minNumber': ip2long(range[0]),
                        'max': range[1],
                        'maxNumber': ip2long(range[1]),
                        'mask': nets[n].mask
                      });
                    }
                  }
                  var ipNumber = ip2long(data.response.lastip);
                  for (var rn = 0; rn < rangedNets.length; rn++) {
                    if (ipNumber >= rangedNets[rn].minNumber && ipNumber <= rangedNets[rn].maxNumber) {
                      $scope.userDetails.lastIP.network = rangedNets[rn].name;
                      break;
                    }
                  }

                  $scope.userDetails.historyIPnets = [];
                  for (var hip = 0; hip < data.response.historyip.length; hip++) {
                    var hipNumber = ip2long(data.response.historyip[hip].ip);
                    var found = false;
                    for (var rnh = 0; rnh < rangedNets.length; rnh++) {
                      if (hipNumber >= rangedNets[rnh].minNumber && hipNumber <= rangedNets[rnh].maxNumber) {
                        $scope.userDetails.historyIPnets.push({
                          'ip': data.response.historyip[hip].ip,
                          'network': rangedNets[rnh].name
                        });
                        found = true;
                        break;
                      }
                    }
                    if (!found) {
                      $scope.userDetails.historyIPnets.push({
                        'ip': data.response.historyip[hip].ip,
                        'network': false
                      });
                    }
                  }
                });
                $rootScope.titleDetail = ' ' + data.response.userid + ' ' + trans('details_lower');
              });
            }
          },
          error: function(data) {
            console.log(data);
          },
          complete: function() {
            setTimeout(function() {
              $scope.complete();
            }, config.artificialDelay);
          }
        });

        // User's mistakes types.
        function drawChartErrors() {
          var chartData = new google.visualization.DataTable();
          chartData.addColumn('string', 'Mistake type');
          chartData.addColumn('number', 'Count');
          if ($scope.errorTypes.length === 0) {
            chartData.addRow([trans('no_user_errors'), 1]);
          }
          for (var i = 0; i < $scope.errorTypes.length; i++) {
            chartData.addRow([
              trans($scope.errorTypes[i].label),
              parseInt($scope.errorTypes[i].value)
            ]);
          }

          var options = {
            'chartArea': {
              'width': '100%',
              'height': '80%'
            },
            'legend': {
              'position': 'labeled'
            }
          };

          var chart = new google.visualization.PieChart(document.getElementById('chart-error-types'));
          chart.draw(chartData, options);
        }

        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'users/' + $scope.userID + '/mistakeTypes',
          success: function(data) {
            if (data.response) {
              $scope.$apply(function() {
                $scope.errorTypes = data.response;
                google.charts.setOnLoadCallback(drawChartErrors);
              });
            }
          },
          error: function(data) {
            console.log(data);
          }
        });

        // User's request stats.
        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'users/' + $scope.userID + '/requestsStats',
          success: function(data) {
            if (data.response) {
              $scope.$apply(function() {
                $scope.requestsStats = data.response;
                $scope.avgWordCount = data.response.avgwordcount;
                $scope.avgMistakes = data.response.avgmistakes;
                $scope.totalWordCount = data.response.totalwordcount;
                $scope.totalMistakes = data.response.totalmistakes;
              });
            }
            else {
              $scope.$apply(function() {
                $scope.requestsStats = false;
                $scope.avgWordCount = 0;
                $scope.avgMistakes = 0;
                $scope.totalWordCount = 0;
                $scope.totalMistakes = 0;
              });
            }
          },
          error: function(data) {
            console.log(data);
          }
        });

        // User's request times.
        function drawChartActivity() {
          var chartData = new google.visualization.DataTable();
          if ($scope.requestsTimes.data.length === 0) {
            chartData.addColumn('string', 'Time');
            chartData.addColumn('number', trans('num_requests'));
            chartData.addRow([trans('infoEmpty'), 1]);
          }
          else {
            chartData.addColumn('date', trans($scope.requestsTimes.selected + '_c'));
            chartData.addColumn('number', trans('num_requests'));
          }
          for (var i = 0; i < $scope.requestsTimes.data.length; i++) {
            chartData.addRow([
              new Date($scope.requestsTimes.data[i].label),
              parseInt($scope.requestsTimes.data[i].value)
            ]);
          }

          var options = {
            /*hAxis: {
              title: trans($scope.requestsTimes.selected + '_c'),
              textPosition: 'in'
            },
            vAxis: {
              title: trans('num_requests'),
              textPosition: 'in'
            },*/
            pointSize: 10,
            chartArea: {
          		width: '90%',
          		height: '70%'
          	},
        		legend: {
              position: 'none'
            },
        		explorer: {
              maxZoomIn: 0.1,
        			maxZoomOut: 2,
        			keepInBounds: true,
              axis: 'horizontal'
        		}
          };

          var chart = new google.visualization.AreaChart(document.getElementById('chart-activity'));
          chart.draw(chartData, options);
        }

        $scope.requestsTimes = {};
        $scope.requestsTimes.hasData = false;
        $scope.requestsTimes.selected = 'month';
        $scope.requestsTimesChange = function(range) {
          $('#chart-activity').empty();
          $.ajax({
            method: 'GET',
            contentType: 'application/json',
            url: config.apiUrl + 'users/' + $scope.userID + '/requestsTimesBy/' + $scope.requestsTimes.selected,
            success: function(data) {
              $scope.$apply(function() {
                $scope.requestsTimes.hasData = true;
                $scope.requestsTimes.data = data.response;
                google.charts.setOnLoadCallback(drawChartActivity);
              });
            },
            error: function(data) {
              console.log(data);
            }
          });
        };

        $(document).ready(function() {
          $scope.requestsTimesChange('month');
        });

        function redrawGraphs() {
          drawChartErrors();
          drawChartActivity();
        }
        var timer = false;
        window.onresize = function(){
          if (timer) clearTimeout(timer);
          timer = setTimeout(redrawGraphs, 200);
        };
        break;
    }
  });
}]);
