angular.module('diplomski-fvoska').controller('registerCtrl', ['$scope', 'cfpLoadingBar', function($scope, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $(document).ready(function() {
    setTimeout(function() {
      $scope.complete();
    }, config.artificialDelay);

    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    function getToken(email, password) {
      var resourceUrl = config.apiUrl + 'admins/auth';
      $.ajax({
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify({
          'email': email,
          'password': password
        }),
        url: resourceUrl,
        success: function(data) {
          if (data.success === true) {
            // Got token
            localStorage.setItem('email', email);
            localStorage.setItem('token', data.token);
            localStorage.setItem('adminId', data.adminId);
            document.location = '/panel';
          } else if (data.success === false) {
            $('#regDanger').html(trans(data.response));
            $('#regDanger').slideDown(config.slideDownSpeed);
          } else {
            console.log(data);
          }
        },
        error: function(data) {
          $('#regDanger').html(trans('service_unavailable'));
          $('#regDanger').slideDown(config.slideDownSpeed);
        }
      });
    }

    function createAdmin(usrEmail, usrPwd) {
      var resourceUrl = config.apiUrl + 'admins';
      $.ajax({
        method: "PUT",
        contentType: "application/json",
        data: JSON.stringify({
          email: usrEmail,
          password: usrPwd
        }),
        url: resourceUrl,
        success: function(data) {
          if (data.success === true) {
            getToken(usrEmail, usrPwd);
          } else if (data.success === false) {
            $('#regDanger').html(data.response);
            $('#regDanger').slideDown(config.slideDownSpeed);
          } else {
            console.log(data);
          }
        },
        error: function(data) {
          if (data.status === 0) {
            $('#regDanger').html(trans('service_unavailable'));
            $('#regDanger').slideDown(config.slideDownSpeed);
            return;
          }

          var res = false;
          try {
            res = JSON.parse(data.responseText);
          }
          catch (e) {
            $('#regDanger').html(trans('json_error'));
            $('#regDanger').slideDown(config.slideDownSpeed);
            return;
          }
          if (res !== false) {
            if (data.status == 400) {
              if (res.response.requiredFields) {
                for(var i in res.response.requiredFields) {
                  var rf = res.response.requiredFields[i];
                  $('#alert-' + rf).html(trans(rf));
                  $('#alert-' + rf).slideDown(config.slideDownSpeed);
                }
              }
            }
            else if (data.status == 409) {
              $('#alert-required_email').html(trans(res.response));
              $('#alert-required_email').slideDown(config.slideDownSpeed);
            }
            else if (data.status == 503){
              $('#regDanger').html(trans(res.response));
              $('#regDanger').slideDown(config.slideDownSpeed);
            }
            else {
              $('#regDanger').html(trans(res.response));
              $('#regDanger').slideDown(config.slideDownSpeed);
            }
          }
        },
        complete: function() {
          setTimeout(function() {
            $('.btn-ajax').prop('disabled', false);
          }, config.slideDownSpeed);
        }
      });
    }

    function submit() {
      $('#registerContainer .alert').slideUp(config.slideUpSpeed);
      $('.btn-ajax').prop('disabled', true);
      var email = $('#regEmail').val();
      var pwd1 = $('#regPassword1').val();
      var pwd2 = $('#regPassword2').val();
      if (pwd1 == pwd2) {
        createAdmin(email, pwd1);
      }
      else {
        $('#alert-confirm_password').html(trans('password_confirm_mismatch'));
        $('#alert-confirm_password').slideDown(config.slideDownSpeed);
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
    }

    $('input').keypress(function (e) {
      if (e.which == 13) {
        submit();
        return false;
      }
    });

    $('#registerContainer #registration-submit').click(function() {
      submit();
    });
  });
}]);
