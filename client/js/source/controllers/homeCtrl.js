angular.module('diplomski-fvoska').controller('homeCtrl', ['$scope', 'cfpLoadingBar', function($scope, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $(document).ready(function() {
    var numReady = 0;
    function gotAllCheck() {
      numReady++;
      if (numReady == 5) {
        $scope.complete();
      }
    }

    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'count/users',
      success: function(data) {
        $scope.$apply(function() {
          $scope.numUsers = data.response;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        gotAllCheck();
      }
    });

    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'count/requests',
      success: function(data) {
        $scope.$apply(function() {
          $scope.numRequests = data.response;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        gotAllCheck();
      }
    });

    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'count/mistakes',
      success: function(data) {
        $scope.$apply(function() {
          $scope.numMistakesTotal = data.response.total;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        gotAllCheck();
      }
    });

    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'count/uniquemistakes',
      success: function(data) {
        $scope.$apply(function() {
          $scope.numUniqueMistakes = data.response.total;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        gotAllCheck();
      }
    });

    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'avg',
      success: function(data) {
        $scope.$apply(function() {
          $scope.avgProcessingTime = data.response.processing;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        gotAllCheck();
      }
    });
  });
}]);
