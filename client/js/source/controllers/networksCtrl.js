angular.module('diplomski-fvoska').controller('networksCtrl', ['$scope', 'cfpLoadingBar', function($scope, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $scope.nets = [];

  $scope.updateNet = function(id, name, mask, allowed) {
    $('.btn-ajax').prop('disabled', true);
    $.ajax({
      method: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        'name': name,
        'mask': mask,
        'allowed': allowed
      }),
      url: config.apiUrl + 'networks/' + id,
      success: function(data) {
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        $('.btn-ajax').prop('disabled', false);
      }
    });
  };

  $scope.deleteNet = function(id) {
    var sure = confirm(trans('delete_check'));
    if (sure) {
      $('.btn-ajax').prop('disabled', true);
      $.ajax({
        method: 'DELETE',
        contentType: 'application/json',
        url: config.apiUrl + 'networks/' + id,
        success: function(data) {
          getNetworks();
        },
        error: function(data) {
          console.log(data);
        },
        complete: function() {
          $('.btn-ajax').prop('disabled', false);
        }
      });
    }
  };

  $scope.addNet = function(name, mask, allowed) {
    $('.btn-ajax').prop('disabled', true);
    $.ajax({
      method: 'PUT',
      contentType: 'application/json',
      data: JSON.stringify({
        'name': name,
        'mask': mask,
        'allowed': allowed
      }),
      url: config.apiUrl + 'networks',
      success: function(data) {
        getNetworks();
        $('#inputName').val('');
        $('#inputMask').val('');
      },
      error: function(data) {
        if (data.responseJSON.response === "network_exists") {
          alert(trans('network_exists'));
        }
        else {
          console.log(data);
        }
      },
      complete: function() {
        $('.btn-ajax').prop('disabled', false);
      }
    });
  };

  var getNetworks = function() {
    $.ajax({
      method: 'GET',
      contentType: 'application/json',
      url: config.apiUrl + 'networks',
      success: function(data) {
        $scope.$apply(function() {
          $scope.nets = data.response;
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        $scope.complete();
      }
    });
  };

  $(document).ready(function() {
    getNetworks();
  });
}]);
