angular.module('diplomski-fvoska').controller('requestsCtrl', ['$scope', '$rootScope', '$location', '$stateParams', 'cfpLoadingBar', function($scope, $rootScope, $location, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };
  $scope.complete = function() {
    cfpLoadingBar.complete();
  };

  $scope.requestID = $stateParams.id;
  if ($scope.requestID) {
    $rootScope.titleDetail = ' ' + $scope.requestID + ' ' + trans('details_lower');
  }

  $scope.showText = false;
  $scope.toggleText = function() {
    $scope.showText = !$scope.showText;
    if ($scope.showText) {
      $('#textContainer').slideDown(config.slideDownSpeed);
    }
    else {
      $('#textContainer').slideUp(config.slideUpSpeed);
    }
  };

  $(document).ready(function() {
    var location = $location.$$path.split('/');
    var table;
    switch (location[location.length - 1]) {
      case 'requests':
        // Requests list view.
        break;
      default:
        // Details.
        $scope.start();
        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'requests/' + $scope.requestID,
          success: function(data) {
            $scope.$apply(function() {
              $scope.requestID = data.response.id;
              $scope.userID = data.response.userid;
              $scope.hUserID = data.response.hUserID;
              $scope.name = data.response.name ? data.response.name : data.response.userid;
              $scope.requestTime = new Date(data.response.requesttime);
              if (data.response.processingtime) {
                $scope.processingTime = data.response.processingtime;
              }
              else {
                $scope.processingTime = '-';
              }
              $scope.numberOfMistakes = data.response.mistakecount;
              $scope.numberOfUniqueMistakes = data.response.uniquemistakecount;
              $scope.wordCount = data.response.wordcount;
              $scope.context = data.response.context;
              $scope.textFile = data.response.textfile;
              $scope.text = data.response.text;
            });

            // Errors.
            // Header translations.
            // $('#dataTables-requests-errors thead th').each(function() {
            //   var title = $(this).text();
            //   $(this).html(trans(title));
            // });
            // Footer translations.
            $('#dataTables-requests-errors tfoot th').each(function() {
              var title = $(this).text();
              if (title != 'actions') {
                $(this).html('<input type="search" class="form-control input-sm footer-search" placeholder="' + trans('filter_by') + ' \'' + title + '\'" />');
              }
            });
            // Create DataTable.
            table = $('#dataTables-requests-errors').DataTable({
              'order': [[ 2, 'desc' ]],
              'responsive': true,
              'language': {
                'lengthMenu': trans('lengthMenu'),
                'search': trans('search'),
                'zeroRecords': trans('zeroRecords'),
                'info': trans('info'),
                'infoEmpty': trans('infoEmpty'),
                'infoFiltered': trans('infoFiltered'),
                'processing': trans('processing'),
                'paginate': {
                  'first': trans('first'),
                  'last': trans('last'),
                  'next': trans('next'),
                  'previous': trans('previous')
                }
              },
              'processing': true,
              'data': data.response.mistakes,
              'columns': [{
                'data': 'suspicious'
              }, {
                'data': 'severity',
                'render': function (data, type, full, meta) {
                  return trans(full.severity);
                }
              }, {
                'data': 'occurrences'
              }, {
                'data': 'button'
              }],
              'columnDefs': [{
                'orderable': false,
                'targets': -1,
                'render': function(data, type, full, meta) {
                  return '<a href="errors/group/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
                }
              }]
            });
            // Set search handler.
            table.columns().every(function() {
              var that = this;
              $('input', this.footer()).on('keyup', function(ev) {
                that.search(this.value).draw();
              });
            });
          },
          error: function(data) {
            console.log(data);
          },
          complete: function() {
            setTimeout(function() {
              $scope.complete();
            }, config.artificialDelay);
          }
        });
        break;
    }
  });
}]);
