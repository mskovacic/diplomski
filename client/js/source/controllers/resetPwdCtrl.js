angular.module('diplomski-fvoska').controller('resetPwdCtrl', ['$scope', '$stateParams', 'cfpLoadingBar', function($scope, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  };

  $scope.start();

  $scope.resetId = $stateParams.adminId;
  $scope.resetEmail = $stateParams.adminEmail;
  $scope.resetToken = $stateParams.resetToken;

  if ($scope.loggedIn) {
    adminCheck(function(isAdmin) {
      $scope.isAdmin = isAdmin;
    });
  }

  $(document).ready(function() {
    setTimeout(function() {
      $scope.complete();
    }, config.artificialDelay);

    $('.alert').click(function() {
      $(this).slideUp(config.slideUpSpeed);
    });

    function resetPassword(password) {
      var resourceUrl = config.apiUrl + 'admins/reset-password/' + $scope.resetToken;
      var sendData = {};
      sendData.password = password;
      if ($scope.resetId) {
        sendData.adminId = $scope.resetId;
      }
      else if ($scope.resetEmail) {
        sendData.email = $scope.resetEmail;
      }
      $.ajax({
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(sendData),
        url: resourceUrl,
        success: function(data) {
          if (data.success === true) {
            // Password was reset
            document.location = '/login';
          } else if (data.success === false) {
            $('#rstDanger').html(trans(data.response));
            $('#rstDanger').slideDown(config.slideDownSpeed);
          } else {
            console.log(data);
          }
        },
        error: function(data) {
          if (data.status === 0) {
            $('#rstDanger').html(trans('service_unavailable'));
            $('#rstDanger').slideDown(config.slideDownSpeed);
            return;
          }

          var res = false;
          try {
            res = JSON.parse(data.responseText);
          }
          catch (e) {
            $('#rstDanger').html(trans('json_error'));
            $('#rstDanger').slideDown(config.slideDownSpeed);
            return;
          }
          if (res !== false) {
            if (data.status == 400) {
              if (res.response.requiredFields) {
                for(var i in res.response.requiredFields) {
                  var rf = res.response.requiredFields[i];
                  console.log(rf);
                  $('#alert-' + rf).html(trans(rf));
                  $('#alert-' + rf).slideDown(config.slideDownSpeed);
                }
              }
            }
            else if (data.status == 503){
              $('#rstDanger').html(trans(res.response));
              $('#rstDanger').slideDown(config.slideDownSpeed);
            }
            else {
              $('#rstDanger').html(trans(res.response));
              $('#rstDanger').slideDown(config.slideDownSpeed);
            }
          }
        },
        complete: function() {
          setTimeout(function() {
            $('.btn-ajax').prop('disabled', false);
          }, config.slideDownSpeed);
        }
      });
    }

    function submitReset() {
      $('#resetPwdContainer .alert').slideUp(config.slideUpSpeed);
      $('.btn-ajax').prop('disabled', true);
      var pwd1 = $('#rstPassword1').val();
      var pwd2 = $('#rstPassword2').val();
      if (pwd1 == pwd2) {
        resetPassword(pwd1);
      }
      else {
        $('#alert-confirm_password').html(trans('password_confirm_mismatch'));
        $('#alert-confirm_password').slideDown(config.slideDownSpeed);
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
    }

    $('#resetPwdContainer input').keypress(function (e) {
      if (e.which == 13) {
        submitReset();
        return false;
      }
    });

    $('#resetPwdContainer #reset-submit').click(function() {
      submitReset();
    });

    function submitResetRequest() {
      $('#resetPwdReqContainer .alert').slideUp(config.slideUpSpeed);
      $('.btn-ajax').prop('disabled', true);
      var sendData = {};
      if ($scope.resetId) {
        sendData.adminId = $scope.resetId;
      }
      else if ($scope.resetEmail) {
        sendData.email = $scope.resetEmail;
      }
      if (!sendData.adminId && !sendData.email) {
        $('#rstInfo').html(trans('reset_password_info'));
        $('#rstInfo').slideDown(config.slideDownSpeed);
        setTimeout(function() {
          $('.btn-ajax').prop('disabled', false);
        }, config.slideDownSpeed);
      }
      else {
        var resourceUrl = config.apiUrl + 'admins/reset-password';
        if ($scope.loggedIn) {
          checkToken();
        }
        $.ajax({
          method: 'PUT',
          contentType: 'application/json',
          data: JSON.stringify(sendData),
          url: resourceUrl,
          success: function(data) {
            if (data.success === true) {
              // Password was reset
              $('#rstSuccess').html(trans(data.response));
              $('#rstSuccess').slideDown(config.slideDownSpeed);
              if (data.passwordReset) {
                console.log(data);
                $('#rstInfo').html('<a href="reset-password/' + data.passwordReset.adminEmail + '/' + data.passwordReset.adminId + '/' + data.passwordReset.token + '">' + trans('reset_link') + '</a>');
                $('#rstInfo').slideDown(config.slideDownSpeed);
              }
            } else if (data.success === false) {
              $('#rstDanger').html(trans(data.response));
              $('#rstDanger').slideDown(config.slideDownSpeed);
            } else {
              console.log(data);
            }
          },
          error: function(data) {
            console.log(data);
            if (data.status === 0) {
              $('#rstDanger').html(trans('service_unavailable'));
              $('#rstDanger').slideDown(config.slideDownSpeed);
              return;
            }

            var res = false;
            try {
              res = JSON.parse(data.responseText);
            }
            catch (e) {
              $('#rstDanger').html(trans('json_error'));
              $('#rstDanger').slideDown(config.slideDownSpeed);
              return;
            }
            if (res !== false) {
              if (data.status == 400) {
                if (res.response.requiredFields) {
                  for(var i in res.response.requiredFields) {
                    var rf = res.response.requiredFields[i];
                    console.log(rf);
                    $('#alert-' + rf).html(trans(rf));
                    $('#alert-' + rf).slideDown(config.slideDownSpeed);
                  }
                }
                else {
                  $('#rstDanger').html(trans(res.response));
                  $('#rstDanger').slideDown(config.slideDownSpeed);
                }
              }
              else if (data.status == 503){
                $('#rstDanger').html(trans(res.response));
                $('#rstDanger').slideDown(config.slideDownSpeed);
              }
              else {
                $('#rstDanger').html(trans(res.response));
                $('#rstDanger').slideDown(config.slideDownSpeed);
              }
            }
          },
          complete: function() {
            setTimeout(function() {
              $('.btn-ajax').prop('disabled', false);
            }, config.slideDownSpeed);
          }
        });
      }
    }

    $('#resetPwdReqContainer input').keypress(function (e) {
      if (e.which == 13) {
        submitResetRequest();
        return false;
      }
    });

    $('#resetPwdReqContainer #reset-submit').click(function() {
      submitResetRequest();
    });
  });
}]);
