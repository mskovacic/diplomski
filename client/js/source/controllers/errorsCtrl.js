angular.module('diplomski-fvoska').controller('errorsCtrl', ['$scope', '$rootScope', '$location', '$stateParams', 'cfpLoadingBar', function($scope, $rootScope, $location, $stateParams, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };
  $scope.complete = function() {
    cfpLoadingBar.complete();
  };

  $scope.errorID = $stateParams.id;
  if ($scope.errorID) {
    $rootScope.titleDetail = ' ' + $scope.errorID;
  }

  $scope.group = $stateParams.group;
  if ($scope.group) {
    $rootScope.titleDetail = ' ' + $scope.group;
  }

  Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
      (mm > 9 ? '' : '0') + mm,
      (dd > 9 ? '' : '0') + dd
    ].join('-');
  };

  $(document).ready(function() {
    var location = $location.$$path.split('/');
    switch (location[location.length - 1]) {
      case 'errors':
        // Errors list view.
        google.charts.load('current', {'packages':['corechart']});

        // Get latest request date.
        $scope.range = {};
        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'lastRequest',
          success: function(data) {
            var dateMax = new Date(data.response);
            var dateMin = new Date(data.response);
            dateMax = new Date(dateMax.setDate(dateMax.getDate() + 1));
            dateMin = new Date(dateMin.setDate(dateMin.getDate() - config.errorsDefaultRange));
            $scope.$apply(function() {
              $scope.range.dateMin = dateMin.yyyymmdd();
              $scope.range.dateMax = dateMax.yyyymmdd();
            });

            // Mistakes types.
            function drawChartErrors() {
              var chartData = new google.visualization.DataTable();
              chartData.addColumn('string', 'Mistake type');
              chartData.addColumn('number', 'Count');
              if ($scope.errorTypes.length === 0) {
                chartData.addRow([trans('no_user_errors'), 1]);
              }

              for (var i = 0; i < $scope.errorTypes.length; i++) {
                chartData.addRow([
                  trans($scope.errorTypes[i].id),
                  parseInt($scope.errorTypes[i].count)
                ]);
              }

              var options = {
                'chartArea': {
                  'width': '100%',
                  'height': '80%'
                },
                'legend': {
                  'position': 'labeled'
                }
              };
              var chart = new google.visualization.PieChart(document.getElementById('chart-error-types-total'));
              chart.draw(chartData, options);
            }

            $scope.types = {};
            $scope.types.unique = true;
            var graphUpdate = function() {
              $('#chart-error-types-total').animate({ 'opacity': 0.5 }, config.slideDownSpeed, 'linear', function() {
                $.ajax({
                  method: 'GET',
                  contentType: 'application/json',
                  url: config.apiUrl + 'mistakesRatio?unique=' + $scope.types.unique + '&dateMin=' + $scope.range.dateMin + '&dateMax=' + $scope.range.dateMax,
                  success: function(data) {
                    $scope.$apply(function() {
                      $scope.errorTypes = data.response;
                      google.charts.setOnLoadCallback(drawChartErrors);
                      $('#chart-error-types-total').animate({ 'opacity': 1 }, config.slideUpSpeed);
                    });
                  },
                  error: function(data) {
                    console.log(data);
                  }
                });
              });
            };
            $scope.graphUpdate = graphUpdate;
            graphUpdate();
            function redrawGraphs() {
              drawChartErrors();
            }

            var timer = false;
            window.onresize = function() {
              if (timer) clearTimeout(timer);
              timer = setTimeout(redrawGraphs, 200);
            };
            $scope.topMistakes = {};
            $scope.topMistakes.limit = 15;
            $scope.topMistakes.unique = true;
            $scope.topMistakes.toLower = false;
            $scope.topMistakes.data = false;

            var topMistakesUpdate = function() {
              $('#top-mistakes').animate({ 'opacity': 0.5 }, config.slideDownSpeed, 'linear', function() {
                $.ajax({
                  method: 'GET',
                  contentType: 'application/json',
                  url: config.apiUrl + 'top/mistakes' +
                  '?limit=' + $scope.topMistakes.limit +
                  '&toLower=' + $scope.topMistakes.toLower +
                  '&unique=' + $scope.topMistakes.unique  +
                  '&dateMin=' + $scope.range.dateMin +
                  '&dateMax=' + $scope.range.dateMax,
                  success: function(data) {
                    $scope.$apply(function() {
                      $scope.topMistakes.data = data.response;
                      $('#top-mistakes').animate({ 'opacity': 1 }, config.slideUpSpeed);
                    });
                  },
                  error: function(data) {
                    console.log(data);
                  }
                });
              });
            };
            $scope.topMistakesUpdate = topMistakesUpdate;
            var updateAvg = function() {
              $.ajax({
                method: 'GET',
                contentType: 'application/json',
                url: config.apiUrl + 'avg' +
                '?dateMin=' + $scope.range.dateMin +
                '&dateMax=' + $scope.range.dateMax,
                success: function(data) {
                  $scope.$apply(function() {
                    $scope.avgWordCount = data.response.words;
                    $scope.avgMistakes = data.response.mistakes;
                  });
                },
                error: function(data) {
                  console.log(data);
                }
              });
            };

            $scope.mistakes = {};
            $scope.mistakes.unique = true;
            $scope.mistakes.toLower = false;

            // Header translations.
            $('#dataTables-errors thead th').each(function() {
              var title = $(this).text();
              $(this).html(trans(title));
            });
            // Footer translations.
            $('#dataTables-errors tfoot th').each(function() {
              var title = $(this).text();
              if (title != 'actions') {
                $(this).html('<input type="search" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" />');
              }
            });
            // Create DataTable.
            var table = $('#dataTables-errors').DataTable({
              'order': [],
              'responsive': true,
              'language': {
                'lengthMenu': trans('lengthMenu'),
                'search': trans('search'),
                'zeroRecords': trans('zeroRecords'),
                'info': trans('info'),
                'infoEmpty': trans('infoEmpty'),
                'infoFiltered': trans('infoFiltered'),
                'processing': trans('processing'),
                'paginate': {
                  'first': trans('first'),
                  'last': trans('last'),
                  'next': trans('next'),
                  'previous': trans('previous')
                }
              },
              'processing': true,
              'serverSide': true,
              'ajax': {
                'url': config.apiUrl + 'mistakes',
                'type': 'GET',
                'cache': false,
                'data': function(d) {
                  delete d.draw; // Don't append draw order, we cache server-side.
                  d.toLower = $scope.mistakes.toLower;
                  d.unique = $scope.mistakes.unique;
                  d.dateMin = $scope.range.dateMin;
                  d.dateMax = $scope.range.dateMax;
                },
                'beforeSend': function(request) {
                  $scope.start();
                },
                'dataSrc': function(json) {
                  $scope.complete();
                  return json.response;
                }
              },
              'columns': [{
                'data': 'suspicious'
              }, {
                'data': 'severity',
                'render': function (data, type, full, meta) {
                  return trans(full.severity);
                }
              }, {
                'data': 'occurrences'
              }, {
                'data': 'button'
              }],
              'columnDefs': [
                {
                  'targets': 2,
                  'render': function(data, type, full, meta) {
                    return full.occurrences.toLocaleString();
                  }
                },
                {
                  'orderable': false,
                  'targets': -1,
                  'render': function(data, type, full, meta) {
                    return '<a href="errors/group/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
                  }
                }
              ]
            });

            // Set search handler.
            $('.footer-search').on('keyup', function(ev) {
              var searchElement = this;
              table.columns().every(function() {
                var column = this;
                var footer = $(column.footer()).find('input');
                column.search($(footer).val());
              });
            });
            table.columns().every(function() {
              var that = this;
              $('input', this.footer()).on('keydown', function(ev) {
                if (ev.keyCode == 13) {
                  that
                    .search(this.value)
                    .draw();
                }
              });
            });

            $scope.mistakesUpdate = function() {
              table.draw();
            };

            $scope.updateAll = function(drawTable) {
              graphUpdate($scope.types.unique);
              topMistakesUpdate();
              updateAvg();
              if (drawTable) {
                table.draw();
              }
            };

            $scope.clearRange = function () {
              $scope.range.dateMin = '';
              $scope.range.dateMax = '';
              $scope.updateAll(true);
            };

            $scope.updateAll(false);
          },
          error: function(data) {
            console.log(data);
          }
        });
        break;
      default:
        // Details view by group.
        $scope.found = 1;
        $scope.start();
        $.ajax({
          method: 'GET',
          contentType: 'application/json',
          url: config.apiUrl + 'mistakes/' + $scope.group,
          success: function(data) {
            $scope.$apply(function() {
              if (data.response === 0) {
                $scope.found = 0;
              }
              else {
                $scope.found = 2;
                $scope.suspicious = data.response.suspicious;
                $scope.type = data.response.severity;
                $scope.numOccurReq = data.response.requestcount;
                $scope.numOccur = data.response.totaloccurrences;
                $scope.suggestions = JSON.parse(data.response.suggestions);
                $scope.errorReqPercent = ($scope.numOccurReq * 100 / $scope.numRequests).toLocaleString() + '%';
              }
            });

            // Header translations.
            $('#dataTables-errors-requests thead th').each(function() {
              var title = $(this).text();
              $(this).html(trans(title));
            });
            // Footer translations.
            $('#dataTables-errors-requests tfoot th').each(function() {
              var title = $(this).text();
              if (title != 'actions') {
                $(this).html('<input type="search" class="form-control input-sm footer-search" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" />');
              }
            });
            // Create DataTable.
            table = $('#dataTables-errors-requests').DataTable({
              'order': [[ 2, 'desc' ]],
              'responsive': true,
              'language': {
                'lengthMenu': trans('lengthMenu'),
                'search': trans('search'),
                'zeroRecords': trans('zeroRecords'),
                'info': trans('info'),
                'infoEmpty': trans('infoEmpty'),
                'infoFiltered': trans('infoFiltered'),
                'processing': trans('processing'),
                'paginate': {
                  'first': trans('first'),
                  'last': trans('last'),
                  'next': trans('next'),
                  'previous': trans('previous')
                }
              },
              'processing': true,
              'data': data.response.requests,
              'columns': [{
                'data': 'requesttime'
              }, {
                'data': 'userid'
              }, {
                'data': 'mistakecount'
              }, {
                'data': 'button'
              }],
              'columnDefs': [
                {
                  'targets': 1,
                  'render': function(data, type, full, meta) {
                    var userText = full.userid;
                    if (full.userName) {
                      userText = full.userName;
                    }
                    return '<a href="users/' + full.userid + '">' + userText + '</a>';
                  }
                },
                {
                  'targets': 2,
                  'render': function(data, type, full, meta) {
                    return full.mistakecount.toLocaleString();
                  }
                },
                {
                'orderable': false,
                'targets': -1,
                'render': function(data, type, full, meta) {
                  return '<a href="requests/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
                }
              }]
            });
            // Set search handler.
            table.columns().every(function() {
              var that = this;
              $('input', this.footer()).on('keyup', function(ev) {
                that.search(this.value).draw();
              });
            });
            $scope.complete();
          },
          error: function(data) {
            $scope.complete();
            console.log(data);
          }
        });
        break;
    }
  });
}]);
