var app = angular.module('diplomski-fvoska', ['ui.router', 'angular-loading-bar', 'ngAnimate', '720kb.datepicker']);

// Animations
app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
  // Set up states (routes).

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: config.baseUrl + 'views/home.html',
      navId: 'home',
      title: 'home',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('login', {
      url: '/login',
      templateUrl: config.baseUrl + 'views/administration/login.html',
      navId: 'login',
      title: 'login',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('register', {
      url: '/register',
      templateUrl: config.baseUrl + 'views/administration/register.html',
      navId: 'register',
      title: 'registration',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('admins', {
      url: '/admins',
      templateUrl: config.baseUrl + 'views/administration/admins.html',
      navId: 'admins',
      title: 'admins',
      requiresLogin: true,
      requiresRoles: ['admin']
    })
    .state('admins.details', {
      url: '/:id',
      views: {
        '@': {
          templateUrl: config.baseUrl + 'views/administration/admin-details.html'
        }
      },
      navId: 'admins',
      title: 'admin_details',
      requiresLogin: true,
      requiresRoles: ['admin']
    })
    .state('admins.tokens', {
      url: '/:admin/tokens',
      views: {
        '@': {
          templateUrl: config.baseUrl + 'views/administration/token-list.html',
          controller: 'tokenListCtrl'
        }
      },
      navId: 'admins',
      title: 'manage_tokens_by_admin',
      requiresLogin: true,
      requiresRoles: false
    })
    .state('reset-password', {
      url: '/reset-password',
      navId: 'panel',
      templateUrl: config.baseUrl + 'views/administration/reset-password-request.html',
      title: 'reset_password_request',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('reset-password.email', {
      url: '/:adminEmail',
      views: {
        '@': {
          templateUrl: config.baseUrl + 'views/administration/reset-password-request.html',
        }
      },
      navId: 'panel',
      title: 'reset_password_request',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('reset-password.token', {
      url: '/:adminEmail/:adminId/:resetToken',
      views: {
        '@': {
          templateUrl: config.baseUrl + 'views/administration/reset-password.html',
        }
      },
      navId: 'panel',
      title: 'reset_password',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('panel', {
      url: '/panel',
      navId: 'panel',
      templateUrl: config.baseUrl + 'views/administration/panel.html',
      title: 'panel',
      requiresLogin: true,
      requiresRoles: false
    })
    .state('tokens', {
      url: '/tokens',
      navId: 'tokens',
      templateUrl: config.baseUrl + 'views/administration/tokens.html',
      title: 'manage_tokens',
      requiresLogin: true,
      requiresRoles: ['tokenCreation']
    })
    .state('tokens.viewTokensHere', {
      url: '/admin/:admin',
      views: {
        'token-list': {
          templateUrl: config.baseUrl + 'views/administration/token-list.html',
          controller: 'tokenListCtrl'
        }
      },
      navId: 'tokens',
      title: 'manage_tokens',
      requiresLogin: true,
      requiresRoles: ['tokenCreation']
    })
    .state('users', {
      url: '/users',
      navId: 'users',
      templateUrl: config.baseUrl + 'views/users/users.html',
      title: 'users',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('user-details', {
      url: '/users/:id',
      navId: 'users',
      templateUrl: config.baseUrl + 'views/users/users-details.html',
      title: 'users-details',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('users-requests', {
      url: '/users/:id/requests',
      navId: 'users',
      templateUrl: config.baseUrl + 'views/users/users-requests.html',
      title: 'users-requests',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('requests', {
      url: '/requests',
      navId: 'requests',
      templateUrl: config.baseUrl + 'views/requests/requests.html',
      title: 'requests',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('request-details', {
      url: '/requests/:id',
      navId: 'requests',
      templateUrl: config.baseUrl + 'views/requests/requests-details.html',
      title: 'requests-details',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('errors', {
      url: '/errors',
      navId: 'errors',
      templateUrl: config.baseUrl + 'views/errors/errors.html',
      title: 'errors',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('error-group', {
      url: '/errors/group/:group',
      navId: 'errors',
      templateUrl: config.baseUrl + 'views/errors/errors-group.html',
      title: 'errors-group',
      requiresLogin: false,
      requiresRoles: false
    })
    .state('networks', {
      url: '/networks',
      navId: 'networks',
      templateUrl: config.baseUrl + 'views/networks/networks.html',
      title: 'networks',
      requiresLogin: false,
      requiresRoles: false
    });

  // Use the HTML5 History API.
  $locationProvider.html5Mode(true);
}]);

function logOut() {
  localStorage.removeItem('email');
  localStorage.removeItem('token');
  localStorage.removeItem('adminId');
  document.location = '/';
}

function adminCheck(callback) {
  var adminToken = localStorage.getItem('token');
  if (!adminToken) {
    return callback(false);
  }
  $.ajax({
    method: 'GET',
    contentType: 'application/json',
    url: config.apiUrl + 'admins/auth/' + adminToken,
    success: function(data) {
      if (data.response.roles.indexOf('admin') > -1) {
        callback(true, true);
      } else {
        callback(false, true);
      }
    },
    error: function(data) {
      callback(false, false);
    }
  });
}

function checkToken(callback) {
  if (localStorage.getItem('token')) {
    $.ajaxSetup({
      beforeSend: function(jqXHR, settings) {
        jqXHR.setRequestHeader('Authorization', localStorage.getItem('token'));
      }
    });
    var tokenCheckUrl = config.apiUrl + 'admins/auth/' + localStorage.getItem('token');
    $.ajax({
      method: "GET",
      contentType: "application/json",
      url: tokenCheckUrl,
      success: function(data) {
        if (data.success === true) {
          if (callback) callback(true);
        } else {
          localStorage.removeItem('email');
          localStorage.removeItem('token');
          localStorage.removeItem('adminId');
          document.location = config.baseUrl + 'login';
          if (callback) callback(false);
        }
      },
      error: function(data) {
        var res = JSON.parse(data.responseText);
        if (res.success === false) {
          localStorage.removeItem('email');
          localStorage.removeItem('token');
          localStorage.removeItem('adminId');
          document.location = config.baseUrl + 'login';
          if (callback) callback(false);
        }
        if (callback) callback(false);
      }
    });
  } else {
    $.ajaxSetup({
      beforeSend: function(jqXHR, settings) {
        jqXHR.setRequestHeader('Authorization', "");
      }
    });
    if (callback) callback(false);
  }
}

function loginCheck($rootScope) {
  var adminEmail = localStorage.getItem('email');
  var adminToken = localStorage.getItem('token');
  if (adminEmail && adminToken) {
    $rootScope.email = adminEmail;
    $rootScope.token = adminToken;
    $rootScope.loggedIn = true;
  } else {
    $rootScope.loggedIn = false;
  }
}

// Copy to clipboard.
function clip(value) {
	var el = document.createElement('input');
	el.setAttribute('type', 'text');
	el.setAttribute('value', value);
	el = document.body.appendChild(el);
	el.select();
	try {
		if(!document.execCommand('copy')) throw 'Not allowed.';
	} catch(e) {
		el.remove();
		console.log("document.execCommand('copy'); is not supported");
		prompt('Copy the text below. (ctrl c, enter)', value);
	} finally {
		if (typeof e == 'undefined') {
			el.remove();
		}
	}
}

// Clipboard copy wrapper.
function copy(value, alert) {
  $(alert).hide();
  clip(value);
  $(alert).slideDown(config.slideDownSpeed);
  $(alert).click(function() {
    $(this).slideUp(config.slideUpSpeed);
  });
}

function getAdminRoles(callback) {
  var adminId = localStorage.getItem('adminId');
  if (!adminId) {
    return callback(false);
  }
  $.ajax({
    method: 'GET',
    contentType: 'application/json',
    url: config.apiUrl + 'admins/roles/' + adminId,
    success: function(data) {
      if (data.response.length > 0) {
        callback(data.response);
      } else {
        callback(false);
      }
    },
    error: function(data) {
      callback(false);
    }
  });
}

// Default sorting on click descending.
$.fn.dataTable.defaults.column.asSorting = ['desc', 'asc'];

app.run(['$rootScope', '$state', function($rootScope, $state) {
  // Check which roles user has.
  getAdminRoles(function(roles) {
    // Run app after roles fetch.
    $rootScope.roles = roles || [];

    // Set language.
    changeLanguage();

    // All routes should have access to config and translations.
    $rootScope.config = config;
    $rootScope.trans = trans;
    $rootScope.copy = copy;
    $rootScope.changeLanguage = changeLanguage;
    $rootScope.stateInfo = function(state) {
      return $state.get(state);
    };

    loginCheck($rootScope);

    $rootScope.adminHasRoles = function (requiredRoles) {
      if ($rootScope.roles.indexOf('admin') > -1) {
        return true;
      }
      if (requiredRoles && $rootScope.roles) {
        var hasAllRoles = true;
        for (var i = 0; i < requiredRoles.length; i++) {
          if ($rootScope.roles.indexOf(requiredRoles[i]) == -1) {
            hasAllRoles = false;
            break;
          }
        }
        if (hasAllRoles) {
          return true;
        }
        else {
          return false;
        }
      }
      else {
        return false;
      }
    };

    // Check if user is admin and logged in.
    adminCheck(function(isAdmin, isLoggedIn) {
      $rootScope.isAdmin = isAdmin;
      if (!isLoggedIn) {
        localStorage.removeItem('email');
        localStorage.removeItem('token');
        localStorage.removeItem('adminId');
        $rootScope.email = false;
        $rootScope.token = false;
        $rootScope.loggedIn = false;
      }
    });

    // Clean route details at route change start.
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      // Close modals
      $('.modal').modal('hide');
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();

      // Check login at every route change (including initial load).
      loginCheck($rootScope);

      // Clean route details at route change start.
      $rootScope.titleDetail = '';
      $rootScope.stateName = toState.name;

      // Check if route we are going to needs login or specific roles.
      var noAccess = false;
      if (toState.requiresRoles) {
        var hasRoles = $rootScope.adminHasRoles(toState.requiresRoles);
        if (toState.requiresRoles && !hasRoles) {
          noAccess = true;
        }
      }
      if (toState.requiresLogin && !$rootScope.loggedIn) {
        noAccess = true;
      }
      if (noAccess) {
        event.preventDefault();
        document.location = config.baseUrl + 'login';
      }
    });

    // Route changed.
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      // Scroll to top.
      document.body.scrollTop = document.documentElement.scrollTop = 0;

      // Update navbar.
      $rootScope.title = $state.current.title;
      if ($state.current.redirectTo == '/') {
        $('.sidebar-nav').find('.active').removeClass('active');
        $('#home').addClass('active');
      }
      else {
        $('.sidebar-nav').find('.active').removeClass('active');
        $('#' + $state.current.navId).addClass('active');
      }
    });
  });
}]);
