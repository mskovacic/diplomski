var translations = {};

function getLanguage() {
  if (!config.language) {
    config.language = config.defaultLanguage;
    localStorage.setItem('language', config.defaultLanguage);
  }
  return config.language;
}

function changeLanguage(language) {
  // Get language from session storage.
  var currentLanguage = localStorage.getItem('language');
  if (!currentLanguage) {
    // No entry in session storage.
    if (!language) {
      // Set to default.
      config.language = config.defaultLanguage;
    }
    else {
      // Set to passed parameter.
      config.language = language;
    }
    // Save.
    localStorage.setItem('language', config.language);
  }
  else {
    // There is an entry in session storage.
    if (!language) {
      // Set to localStorage
      config.language = currentLanguage;
    }
    else {
      // Set to passed parameter.
      config.language = language;
    }
    // Save.
    localStorage.setItem('language', config.language);
  }
}

function trans(code) {
  if (code === undefined) {
    return '';
  }
  var t = false;
  if (translations[config.language]) {
    t = translations[config.language][code];
  }
  if (t !== undefined) {
    return t;
  }
  else {
    return 'NO TRANSLATION FOR ' + code + '';
  }
}
