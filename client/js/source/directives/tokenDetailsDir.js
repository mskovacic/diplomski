angular.module('diplomski-fvoska').directive('tokenDetails', function() {
  return {
    scope: {
      tokenDetails: '=tokenData'
    },
    templateUrl: config.baseUrl + 'views/administration/token-details.html'
  };
});
