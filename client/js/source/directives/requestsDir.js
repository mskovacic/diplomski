angular.module('diplomski-fvoska')
.controller('requestsListCtrl', ['$scope', '$rootScope', '$compile', 'cfpLoadingBar', function($scope, $rootScope, $compile, cfpLoadingBar) {
  $scope.start = function() {
    cfpLoadingBar.start();
  };
  $scope.complete = function() {
    cfpLoadingBar.complete();
  };

  // Header translations.
  $('#dataTables-requests thead th').each(function() {
    var title = $(this).text();
    $(this).html(trans(title));
  });

  // Default sorting only if all requests.
  var searchCols = [
    null,
    null,
    null,
    null,
    null,
    null
  ];
  var defaultSearching = true;
  if ($scope.admin) {
    defaultSearching = false;
  }
  var defaultSearchValues = {};
  defaultSearchValues['num_errors'] = '> 1000';

  // Footer translations.
  $('#dataTables-requests tfoot th').each(function() {
    var title = $(this).text();
    if (title != 'actions') {
      var searchValue = '';
      if (defaultSearching) {
        if (title == 'num_errors') {
          searchValue = defaultSearchValues['num_errors'];
          searchCols[3] = { 'search': searchValue };
        }
      }
      if (title == 'req_time') {
        $(this).html('<datepicker button-prev="<i class=\'fa fa-arrow-left\'></i>" button-next="<i class=\'fa fa-arrow-right\'></i>" date-format="yyyy-MM-dd" class="datepicker-search">' +
            '<input type="text" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" value="' + searchValue + '" />' +
        '</datepicker>');
        $compile($('.datepicker-search'))($scope);
      }
      else {
        $(this).html('<input type="search" class="form-control input-sm footer-search" data-col="' + title + '" placeholder="' + trans('filter_by') + ' \'' + trans(title) + '\'" value="' + searchValue + '" />');
      }
    }
  });

  // Create DataTable.
  table = $('#dataTables-requests').DataTable({
    'order': [
      [3, 'desc']
    ],
    'searchCols': searchCols,
    'responsive': true,
    'language': {
      'lengthMenu': trans('lengthMenu'),
      'search': trans('search'),
      'zeroRecords': trans('zeroRecords'),
      'info': trans('info'),
      'infoEmpty': trans('infoEmpty'),
      'infoFiltered': trans('infoFiltered'),
      'processing': trans('processing'),
      'paginate': {
        'first': trans('first'),
        'last': trans('last'),
        'next': trans('next'),
        'previous': trans('previous')
      }
    },
    'processing': true,
    'serverSide': true,
    'ajax': {
      'url': config.apiUrl + 'requests',
      'type': 'GET',
      'cache': false,
      'data': function(d) {
        if ($scope.admin) {
          d.admin = $scope.admin;
        }
        delete d.draw; // Don't append draw order, we cache server-side.
      },
      'beforeSend': function (request) {
        $scope.start();
      },
      'dataSrc': function(json) {
        $scope.complete();
        return json.response;
      }
    },
    'columns': [{
      'data': 'requestTime'
    }, {
      'data': 'processingTime'
    }, {
      'data': 'userID'
    }, {
      'data': 'report.numberOfMistakes'
    }, {
      'data': 'wordCount'
    }, {
      'data': 'button'
    }],
    'columnDefs': [
    {
      'targets': 0,
      'render': function(data, type, full, meta) {
        if (full.requesttime)
          return full.requesttime.toLocaleString();
        else
          return '-';
      }
    },
    {
      'targets': 1,
      'render': function(data, type, full, meta) {
        if (full.processingtime)
          return full.processingtime.toLocaleString() + 's';
        else
          return '-';
      }
    },
    {
      'targets': 2,
      'render': function(data, type, full, meta) {
        var userText = full.userid;
        if (full.name) {
          userText = full.name;
        }
        return '<a href="users/' + full.userid + '">' + userText + '</a>';
      }
    },
    {
      'targets': 3,
      'render': function(data, type, full, meta) {
        return full.mistakecount.toLocaleString();
      }
    },
    {
      'targets': 4,
      'render': function(data, type, full, meta) {
        return full.wordcount.toLocaleString();
      }
    },
    {
      'orderable': false,
      'targets': -1,
      'render': function(data, type, full, meta) {
        return '<a href="requests/' + full.id + '" class="btn btn-primary">' + trans('details') + '</a>';
      }
    }]
  });
  // Set search handler.
  $('.footer-search').on('keyup', function(ev) {
    var searchElement = this;
    table.columns().every(function() {
      var column = this;
      var footer = $(column.footer()).find('input');
      column.search($(footer).val());
    });
  });
  table.columns().every(function() {
    var that = this;
    $('input', this.footer()).on('keydown', function(ev) {
      if (ev.keyCode == 13) {
        that
          .search(this.value)
          .draw();
      }
    });
  });
}])
.directive('requests', function() {
  return {
    scope: {
      admin: '=admin'
    },
    templateUrl: config.baseUrl + 'views/requests/requests-list.html'
  };
});
