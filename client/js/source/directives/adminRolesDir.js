angular.module('diplomski-fvoska')
.controller('adminRolesCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
  $('.alert').click(function() {
    $(this).slideUp(config.slideUpSpeed);
  });
  function updateRole(admId, role, method) {
    $('.btn-ajax').prop('disabled', true);
    checkToken();
    var adminPath = config.apiUrl + 'admins/roles/' + admId;
    $.ajax({
      method: method,
      contentType: 'application/json',
      data: JSON.stringify({
        'roles': [role]
      }),
      url: adminPath,
      success: function(data) {
        getAdminRoles(function(roles) {
          if (admId == localStorage.getItem('adminId')) {
            $rootScope.roles = roles;
          }
          $scope.$apply(function() {
            $scope.adminDetails.roles = data.response.updatedRoles;
          });
        });
      },
      error: function(data) {
        console.log(data);
      },
      complete: function() {
        $('.btn-ajax').prop('disabled', false);
      }
    });
  }

  $scope.removeRole = function(admId, role) {
    updateRole(admId, role, 'DELETE');
  };

  $scope.addRole = function(admId, role) {
    role = typeof role !== 'undefined' ? role : $scope.adminDetails.newRole;
    if (role) {
      updateRole(admId, role, 'PUT');
      $scope.adminDetails.newRole = '';
    }
  };
}])
.directive('adminRoles', function() {
  return {
    scope: {
      adminDetails: '=adminDetails'
    },
    templateUrl: config.baseUrl + 'views/administration/admin-roles.html'
  };
});
