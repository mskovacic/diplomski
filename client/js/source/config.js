var protocol = '';
var port = '';
if (window.location.protocol === 'http:') {
    protocol = 'http://';
    port = 8080;
} else {
    protocol = 'https://';
    port = 8443;
}
var config = {
  //baseUrl: protocol + 'diplomski.com:' + port + '/',
  //apiUrl: protocol + 'diplomski.com:' + port + '/api/',
  baseUrl: '/',
  apiUrl: '/api/',
  defaultLanguage: 'hr',
  language: false,
  slideDownSpeed: 250,
  slideUpSpeed: 250,
  artificialDelay: 250,
  errorsDefaultRange: 14
};
