var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');

var should = require('should');

describe('Token signing/verifying', function() {
  var dataToSign = { foo: 'bar' };
  var jwt = require('jsonwebtoken');
  var token;
  it('Should sign successfuly (sync)', function(done) {
    token = jwt.sign(dataToSign, config.secretPrivate, { algorithm: 'RS256' });
    if (token) {
      done();
    }
  });
  it('Should sign successfuly (async)', function(done) {
    var d = done;
    jwt.sign(dataToSign, config.secretPrivate, { algorithm: 'RS256' }, function(err, rtoken) {
      if (rtoken) {
        d();
      }
    });
  });
  it('Should verify successfuly (sync)', function(done) {
    var decoded = jwt.verify(token, config.secretPublic, { algorithm: 'RS256' });
    decoded.foo.should.equal('bar');
    done();
  });
  it('Should verify successfuly (async)', function(done) {
    var d = done;
    jwt.verify(token, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      decoded.foo.should.equal('bar');
      d();
    });
  });
});
