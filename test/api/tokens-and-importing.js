const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
//var model = require(appRoot + '/models/_models.js');
const supertest = require('supertest');
const should = require('should');
//const async = require('async');
const server = supertest.agent('http://' + config.listenAddress + ':' + config.httpPort + '/api');
const usersBL = require(appRoot + '/bl/users.js');
const tokenBL = require(appRoot + '/bl/token.js');
const requestsBL = require(appRoot + '/bl/requests.js');

describe('Access token creation and importing', function() {
  before(function(done) {
    // async.parallel([
    //   function(callback) {
    //     model.request.remove({}, function(err) {
    //       global.ispravimeLogger.debug('    Requests collection cleared');
    //       callback(null);
    //     });
    //   },
    //   function(callback) {
    //     model.user.remove({}, function(err) {
    //       global.ispravimeLogger.debug('    Users collection cleared');
    //       callback(null);
    //     });
    //   }
    // ], function(err, results) {
    //   if (!err) {
    //     done();
    //   }
    // });
    usersBL.deleteUsers((err, success) => {
      if (success) {
        global.ispravimeLogger.debug('    Users collection cleared');
        global.ispravimeLogger.debug('    Requests collection cleared');
        global.ispravimeLogger.debug('    MistakeRequest collection cleared');
      }

      requestsBL.deleteMistakes((err, success) => {
        if (success) {
          global.ispravimeLogger.debug('    Mistakes collection cleared');
        }

        tokenBL.deleteRevokedTokens((err, success) => {
          if (success) {
            global.ispravimeLogger.debug('    RevokedTokens collection cleared');
          }
  
          done();
        });
      });
    });
  });

  var apiToken;
  var importToken;
  var exampleImportJSON = {
    "timeProcessed": 2.127723,
    "requestTime": "2017-01-16 16:25:13.000000",
    "wordCounter": 409,
    "ip": "90.137.157.137",
    "userID": "C954EDB8-D8CF-11E6-B401-8207F2F150C7",
    "context": "on",
    "report": {
      "errors": [{
        "suspicious": "Electroblemma",
        "severity": "xx",
        "occurrences": 1
      }, {
        "suspicious": "Odontomachus",
        "severity": "xx",
        "occurrences": 1
      }, {
        "suspicious": "Creataceous",
        "suggestions": ["Cretaceous"],
        "severity": "ll",
        "occurrences": 1
      }, {
        "suspicious": "znastvenici",
        "suggestions": ["znanstvenici"],
        "severity": "mm",
        "occurrences": 1
      }],
      "numberOfErrors": 4
    },
    "textFile": "2017-01-16/hacheck_90.137.157.137_C954EDB8-D8CF-11E6-B401-8207F2F150C7_beb9d2d621ddeb1d2ca30002baaff46c"
  };

  it('PUT /token should not allow creation without authorization', function(done) {
    server
      .put('/token')
      .set('User-Agent', config.testUA)
      .send({
        'endpoints': ['/api/some-endpoint']
      })
      .expect('Content-type', /json/)
      .expect(401)
      .end(function(err, res) {
        res.status.should.equal(401);
        res.body.response.should.equal('no_token');
        done();
      });
  });

  it('PUT /token should not allow creation without endpoints', function(done) {
    server
      .put('/token')
      .set('User-Agent', config.testUA)
      .send()
      .expect('Content-type', /json/)
      .expect(400)
      .end(function(err, res) {
        res.status.should.equal(400);
        res.body.response.should.equal('no_endpoints');
        done();
      });
  });

  it('PUT /token should create token', function(done) {
    server
      .put('/token')
      .set('User-Agent', config.testUA)
      .set('Authorization', global.firstToken)
      .send({
        'endpoints': [{
          'uri': '/api/some-endpoint',
          'method': 'GET'
        }]
      })
      .expect('Content-type', /json/)
      .expect(200)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.response.should.equal('enjoy_token');
        apiToken = res.body.token;
        done();
      });
  });

  it('GET /token/{token} should get token details', function(done) {
    server
      .get('/token/' + apiToken)
      .set('User-Agent', config.testUA)
      .send()
      .expect('Content-type', /json/)
      .expect(200)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.response.endpoints.should.containEql({
          'uri': '/api/some-endpoint',
          'method': 'GET'
        });
        done();
      });
  });

  describe('Grant access to importing service', function() {
    it('Can\'t grant endpoint access without admin permission', function(done) {
      server
        .put('/admins/' + global.secondAdminId + '/endpointAccess')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.secondToken)
        .send()
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('no_allowed_roles');
          done();
        });
    });

    it('Request for endpoint access granting should be complete', function(done) {
      server
        .put('/admins/' + global.secondAdminId + '/endpointAccess')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.firstToken)
        .send({})
        .expect('Content-type', /json/)
        .expect(400)
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.success.should.equal(false);
          res.body.response.requiredFields.should.containEql('required_serviceName');
          res.body.response.requiredFields.should.containEql('required_uri');
          res.body.response.requiredFields.should.containEql('required_method');
          done();
        });
    });

    it('User needs role tokenCreation to be able to create apiToken', function(done) {
      server
        .put('/token')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.secondToken)
        .send({
          'endpoints': [{
            'uri': '/api/importer/v1',
            'method': 'PUT'
          }]
        })
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('admin_not_token_creation_role');
          done();
        });
    });

    it('Admin can grant roles', function(done) {
      server
        .put('/admins/roles/' + global.secondAdminId)
        .set('User-Agent', config.testUA)
        .set('Authorization', global.firstToken)
        .send({
          'roles': ['tokenCreation', 'importer']
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.addedRoles.should.containEql('tokenCreation');
          res.body.response.updatedRoles.should.containEql('tokenCreation');
          res.body.response.addedRoles.should.containEql('importer');
          res.body.response.updatedRoles.should.containEql('importer');
          done();
        });
    });

    it('User without access to /api/importer/v1 shouldn\'t be able to create apiToken', function(done) {
      server
        .put('/token')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.secondToken)
        .send({
          'endpoints': [{
            'uri': '/api/importer/v1',
            'method': 'PUT'
          }]
        })
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('no_permissions');
          done();
        });
    });

    it('Admin can grant endpoint access to other users', function(done) {
      server
        .put('/admins/' + global.secondAdminId + '/endpointAccess')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.firstToken)
        .send({
          'serviceName': 'importer',
          'uri': '/api/importer/v1',
          'method': 'PUT'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          done();
        });
    });

    it('User with tokenCreation role and access to /api/importer/v1 should be able to create apiToken', function(done) {
      server
        .put('/token')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.secondToken)
        .send({
          'endpoints': [{
            'uri': '/api/importer/v1',
            'method': 'PUT'
          }],
          'clientHosts': [config.listenAddress]
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.response.should.equal('enjoy_token');
          importToken = res.body.token;
          done();
        });
    });

    it('Admin with role \'importer\' should be able to import requests', function(done) {
      server
        .put('/importer/v1')
        .set('User-Agent', config.testUA)
        .set('Authorization', importToken)
        .send(exampleImportJSON)
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          global.ispravimeLogger.debug(err);
          res.status.should.equal(200);
          res.body.response.should.equal('request_added');
          done();
        });
    }).timeout(0);

    it('Admin can revoke tokens', function(done) {
      server
      .put('/token/revoke/' + importToken)
      .set('User-Agent', config.testUA)
      .set('Authorization', global.firstToken)
      .send()
      .expect('Content-type', /json/)
      .expect(200)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.response.should.equal('token_revoked');
        res.body.revokedToken.should.equal(importToken);
        done();
      });
    });

    it('Admin should not be able to import using revoked token', function(done) {
      server
        .put('/importer/v1')
        .set('User-Agent', config.testUA)
        .set('Authorization', importToken)
        .send(exampleImportJSON)
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('token_revoked');
          done();
        });
    });

    it('Admin should not be able to import without access token', function(done) {
      server
        .put('/importer/v1')
        .set('User-Agent', config.testUA)
        .send(exampleImportJSON)
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('no_token_provided');
          done();
        });
    });

    it('Admin can remove endpoint access from other users', function(done) {
      server
        .delete('/admins/' + global.secondAdminId + '/endpointAccess')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.firstToken)
        .send({
          'uri': '/api/importer/v1',
          'method': 'PUT'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          done();
        });
    });

    it('User with tokenCreation role but without access to /api/importer/v1 should not be able to create apiToken', function(done) {
      server
        .put('/token')
        .set('User-Agent', config.testUA)
        .set('Authorization', global.secondToken)
        .send({
          'endpoints': [{
            'uri': '/api/importer/v1',
            'method': 'PUT'
          }]
        })
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.response.should.equal('no_permissions');
          done();
        });
    });
  });
});
