var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');

var supertest = require('supertest');
var should = require('should');

var server = supertest.agent('http://' + config.listenAddress + ':' + config.httpPort + '/api');

describe('API Home', function() {
  var firstUserId;
  var secondUserId;
  var token;
  var token2nd;
  it('GET / should return available services', function(done) {
    server
      .get('/')
      .set('User-Agent', config.testUA)
      .expect('Content-type', /json/)
      .expect(200)
      .end(function(err, res) {
        res.status.should.equal(200);
        res.body.success.should.equal(true);
        res.body.should.have.property('services').which.is.a.Array();
        done();
      });
  }).timeout(0);
});
