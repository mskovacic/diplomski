const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
//var model = require(appRoot + '/models/_models.js');
const adminBL = require(appRoot + '/bl/admin.js');
const supertest = require('supertest');
const should = require('should');
const server = supertest.agent('http://' + config.listenAddress + ':' + config.httpPort + '/api');

describe('Administrators', function() {
  before(function(done) {
    // Flush DB.
    // model.admin.remove({}, function(err) {
    //   global.ispravimeLogger.debug('    Admins collection cleared');
    //   done();
    // });
    adminBL.deleteAdmins((err, success) => {
      if (success) {
        global.ispravimeLogger.debug('    Admins collection cleared');
      }
      
      done();
    });
  });

  var firstAdminId;
  var secondAdminId;
  var firstToken;
  var secondToken;

  describe('Registration', function() {
    it('PUT /admins should add new admin', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'admin@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.should.equal('admin_added');
          firstAdminId = res.body.newAdminID;
          global.firstAdminId = firstAdminId;
          done();
        });
    });

    it('PUT /admins should add second admin', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'user@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.should.equal('admin_added');
          secondAdminId = res.body.newAdminID;
          global.secondAdminId = secondAdminId;
          done();
        });
    });

    it('PUT /admins with existing email should report error', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'user@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(409)
        .end(function(err, res) {
          res.status.should.equal(409);
          res.body.success.should.equal(false);
          res.body.response.should.equal('email_exists');
          done();
        });
    });

    it('PUT /admins without email should report error', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(400)
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.success.should.equal(false);
          res.body.response.requiredFields[0].should.equal('required_email');
          done();
        });
    });

    it('PUT /admins without password should report error', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'other.user@test.com'
        })
        .expect('Content-type', /json/)
        .expect(400)
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.success.should.equal(false);
          res.body.response.requiredFields[0].should.equal('required_password');
          done();
        });
    });

    it('PUT /admins without email and password should report error', function(done) {
      server
        .put('/admins')
        .set('User-Agent', config.testUA)
        .send({})
        .expect('Content-type', /json/)
        .expect(400)
        .end(function(err, res) {
          res.status.should.equal(400);
          res.body.success.should.equal(false);
          res.body.response.requiredFields[0].should.equal('required_email');
          res.body.response.requiredFields[1].should.equal('required_password');
          done();
        });
    });
  });

  describe('Authorization', function() {
    it('POST /admins/auth should authorize', function(done) {
      server
        .post('/admins/auth')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'admin@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.should.equal('token_received');
          firstToken = res.body.token;
          global.firstToken = firstToken;
          done();
        });
    });

    it('POST /admins/auth should authorize second user', function(done) {
      server
        .post('/admins/auth')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'user@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.should.equal('token_received');
          secondToken = res.body.token;
          global.secondToken = secondToken;
          done();
        });
    });

    it('POST /admins/auth shouldn\'t authorize non-existing user', function(done) {
      server
        .post('/admins/auth')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'nouser@test.com',
          'password': '1234'
        })
        .expect('Content-type', /json/)
        .expect(401)
        .end(function(err, res) {
          res.status.should.equal(401);
          res.body.success.should.equal(false);
          res.body.response.should.equal('admin_not_exist');
          done();
        });
    });

    it('POST /admins/auth shouldn\'t authorize with wrong password', function(done) {
      server
        .post('/admins/auth')
        .set('User-Agent', config.testUA)
        .send({
          'email': 'user@test.com',
          'password': '4321'
        })
        .expect('Content-type', /json/)
        .expect(401)
        .end(function(err, res) {
          res.status.should.equal(401);
          res.body.success.should.equal(false);
          res.body.response.should.equal('password_wrong');
          done();
        });
    });

    it('GET /admins/auth/{token} should return token info', function(done) {
      server
        .get('/admins/auth/' + firstToken)
        .set('User-Agent', config.testUA)
        .send()
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.tokenType.should.equal('adminToken');
          done();
        });
    });

    it('First user should be admin', function(done) {
      server
        .get('/admins/auth/' + firstToken)
        .set('User-Agent', config.testUA)
        .send()
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.roles.should.containEql('admin');
          done();
        });
    });

    it('Second user should not be admin', function(done) {
      server
        .get('/admins/auth/' + secondToken)
        .set('User-Agent', config.testUA)
        .send()
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.roles.should.not.containEql('admin');
          done();
        });
    });
  });
  describe('Rolecheck middleware', function() {
    it('First user (admin) should see other user\'s details', function(done) {
      server
        .get('/admins/details/' + secondAdminId)
        .set('User-Agent', config.testUA)
        .set('Authorization', firstToken)
        .send()
        .expect('Content-type', /json/)
        .expect(200)
        .end(function(err, res) {
          res.status.should.equal(200);
          res.body.success.should.equal(true);
          res.body.response.email.should.equal('user@test.com');
          done();
        });
    });

    it('Second user (not admin) should not see other user\'s details', function(done) {
      server
        .get('/admins/details/' + secondAdminId)
        .set('User-Agent', config.testUA)
        .set('Authorization', secondToken)
        .send()
        .expect('Content-type', /json/)
        .expect(403)
        .end(function(err, res) {
          res.status.should.equal(403);
          res.body.success.should.equal(false);
          res.body.response.should.equal('no_allowed_roles');
          done();
        });
    });
  });
});
