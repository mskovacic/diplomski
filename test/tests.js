global.testing = true;
global.ispravimeLogger = {
    'debug': function(x) { if (x) console.log(x);}
};
require('./keys/keys.js');
require('./api/home.js');
require('./api/admins.js');
require('./api/tokens-and-importing.js');
