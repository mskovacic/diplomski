# Graduation Thesis

Programska potpora ISPRAVI.ME STATISTIKA izrađena je 2017. na Fakultetu elektrotehnike i računarstva u Zagrebu u okviru diplomskog rada Filipa Voske pod nazivom «Praćenje aktivnosti korisnika usluge računalne provjere pravopisa» i vodstvom izv. prof. dr. sc. Gordana Gledeca.

# 1. Setup

API server runs under node.js environment. The application itself is a stand-alone HTTP(S) server. Web client can be served using node.js as well, or alternatively as static HTML/JS/CSS content via apache or some other HTTP server. Edit config.js and change 'onlyApi' from false to true in order tu run only the API in node.js, without client. Web client will then have to be served via some other HTTP server. By default, both API server and Web client content will be served via node.js HTTP(S) server.

Follow the steps to install and run the application.

## 1.1. Install dependencies

Application runs on all platforms that support main dependencies/requirements. Ubuntu 16.04 was used during development. Tested on Windows 10 - works, but slower (python import script is much slower).

Main dependencies/requirements:
*   node.js >= v4.0.0 (v.6.9.2 used during development)
*   npm packages which should be installed globally (as superuser):
    *   grunt (for minification and monitoring tasks):
        * ```sudo npm install -g grunt```
    *   bower (for fetching client dependencies like angular and bootstrap):
        * ```sudo npm install -g bower```
*   mongodb >= v3.2 (v3.4 used during development)
*   redis server >= v1.0 (v3.0.7 used during development)
*   python v2.x for report parsing and importing script

In terminal:

```bash
# Install all server dependencies.
$ npm install

# Or without development dependencies (testing framework, etc)
$ npm install --production
```

```bash
# Install client dependencies.
$ cd client
$ bower install

# Package all source CSS and JS files and minify them.
$ grunt
```

Any change in client source JS and CSS files requires grunt to be run again. Following command can monitor for changes in the background and minify whenever JS files are modified:
```
$ grunt watch
```

If CSS files are modified, run ```grunt cssmin```.

## 1.2. Create needed keys

OpenSSL is used for creating encryption keys. It is available for various Linux distributions (access from Terminal). On Windows, something like Git for Windows (Git Bash) can be used.

### 1.2.1. Token signing/verifying keys

These keys are used for signing and verifying tokens.

```bash
# From project's root folder
$ mkdir tokenKeys
$ cd tokenKeys
# Create passworded private key
$ openssl genrsa -des3 -out private.pem 2048
# Extract public key
$ openssl rsa -in private.pem -outform PEM -pubout -out public.pem
# Extract unencrypted private key (jsonwebtoken ^5.4.1 currently does not support passworded keys)
$ openssl rsa -in private.pem -out private_unencrypted.pem -outform PEM
```

### 1.2.2. SSL certificate (self-signed)

These keys and certificates are used for SSL. If you own a valid SSL certificate, you can use that. Skip this section and modify ```config.js``` file to point to correct certificate/key files.

If you do not own a SSL certificate, you can create a self-signed certificate. For HTTPS, application needs server.key (server private key), server.crt (server public key + certificate signed by CA) and ca.crt (self-signing CA). Browser will give warnings because CA isn't trusted - certificate is self-signed.
*Source: [How to generate self-signed certificate for usage in Express4 or Node.js HTTP](http://blog.matoski.com/articles/node-express-generate-ssl/)*

```bash
mkdir ssl
cd ssl
# Create passworded CA private key
openssl genrsa -des3 -out ca.key 2048
# Signing request
openssl req -new -key ca.key -out ca.csr
# Sign CA certificate
openssl x509 -req -days 365 -in ca.csr -out ca.crt -signkey ca.key

# Create passworded server private key
openssl genrsa -des3 -out server.key 2048
# Signing request
openssl req -new -key server.key -out server.csr
# Remove password from private key
cp server.key server.key.passphrase
openssl rsa -in server.key.passphrase -out server.key
# Sign server certificate
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```

### 1.2.3. SSL certificate (letsencrypt)

Use free SSL certificate instead of self-signed. Needs to be run on production server with port 80 open and unbound (temporarily shut down apache until certificate is created).

In following commands, replace values in square brackets (*\[VALUE\]*) with actual values.

```bash
sudo npm install -g letsencrypt-cli
sudo letsencrypt certonly --standalone --config-dir /home/[DOMAIN]/letsencrypt/etc --agree-tos --domains [DOMAIN] --email [EMAIL] --server https://acme-v01.api.letsencrypt.org/directory
```

Edit config.js to point to correct keys:
```
'sslKey': fs.readFileSync('/home/[USER]/letsencrypt/etc/live/[DOMAIN]/privkey.pem'),
'sslCert': fs.readFileSync('/home/[USER]/letsencrypt/etc/live/[DOMAIN]/fullchain.pem'),
'sslCACert': fs.readFileSync('/home/[USER]/letsencrypt/etc/live/[DOMAIN]/chain.pem'),
```

## 1.3. Set up mongodb

Minimum version: 3.2

```js
# Create root mongo user.
use admin

db.createUser({
  user: "root",
  pwd: "password",
  roles: [ "root" ]
})
```
```js
# Create application user.
use diplomski

db.createUser({
  user: "diplomski",
  pwd: "diplomski",
  roles: [
    { role: "dbOwner", db: "diplomski" },
    { role : "dbOwner", db : "diplomskiTest" }
  ]
})
```

To enable authorization, edit ```/etc/mongod.conf```, uncomment "# security" and add ```authorization: enabled```.

Example config file with authorization enabled and connection listening on 127.0.0.1 (localhost) and 95.85.6.210 (to allow remote acccess to server's database):
```
# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIp: 127.0.0.1,95.85.6.210


#processManagement:

security:
  authorization: enabled

#operationProfiling:

#replication:

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
```

## 1.4. Set up redis-server

Minimum version: 1.0

Needed for caching.

```
wget http://download.redis.io/releases/redis-3.2.6.tar.gz
tar xzf redis-3.2.6.tar.gz
cd redis-3.2.6
make
# Run Redis:
src/redis-server
```

More detailed instruction for installing on Ubuntu 16.04 are available [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04).

## 2. Configuring and running the server

Check config.js and set up correct hostname, httpPort and httpsPort. If listenOnExternalAddress is set to true, application will listen on all external interfaces. This is needed for production, otherwise it would only be available from localhost (IP specified in listenAddress). If application shouldn't listen on all external interfaces, set listenAddress to some specific interface's IP and set listenOnExternalAddress: false. That way application will listen on only one specific (non-localhost) IP.

Make sure that hostnames, IP addreses and ports in config.js (server config) and client/js/source/config.js (client config) match. Server and client can be run completely separately, as long as client knows where the API server is.

If client is served using apache, make sure to include client/.htaccess file when serving the client. Rewrite engine is required.

### 2.1. Config example 1

Example configuration in case that server is run using node.js on a machine with IP address 1.2.3.4, and client is hosted via apache on a machine with IP address 4.3.2.1 (and in DNS statistika.ispravi.me points to 4.3.2.1):

config.js:
```json
...
'listenAddress': '1.2.3.4',
'hostname': 'statistika.ispravi.me',
'httpPort': '8080',
'httpsPort': '8443',
'onlyApi': true,
'listenOnExternalAddress': false,
...
```
client/js/source/config.js:
```js
var protocol = '';
var port = '';
if (window.location.protocol === 'http:') {
    protocol = 'http://';
    port = 8080;
} else {
    protocol = 'https://';
    port = 8443;
}
var config = {
  baseUrl: '/',
  apiUrl: protocol + 'statistika.ispravi.me:' + port + '/api/',
  defaultLanguage: 'hr',
  language: false,
  slideDownSpeed: 250,
  slideUpSpeed: 250
};
```

### 2.2. Config example 2

In this example, both API server and client are run using node.js on same machine with IP address 95.85.6.210 (and in DNS statistika.ispravi.me points to 95.85.6.210):

config.js:
```json
...
'listenAddress': '95.85.6.210',
'hostname': 'statistika.ispravi.me',
'httpPort': '80',
'httpsPort': '443',
'onlyApi': false,
'listenOnExternalAddress': false,
...
```
client/js/source/config.js:
```js
/*
NOT IMPORTANT IN THIS CASE
var protocol = '';
var port = '';
if (window.location.protocol === 'http:') {
    protocol = 'http://';
    port = 8080;
} else {
    protocol = 'https://';
    port = 8443;
}
*/
var config = {
  baseUrl: '/',
  apiUrl: '/api/',
  defaultLanguage: 'hr',
  language: false,
  slideDownSpeed: 250,
  slideUpSpeed: 250
};
```

### 2.3. Running the server

Please note that if onlyApi is set to true, client application will not be served. In that case, serve contents of ```client``` directory via some other HTTP server.

There are multiple ways to start the server. If httpPort and httpsPort are in reserved port range (ex. 80 and 443), use sudo when starting server. Terminal, from project's root folder:

*   Start using startup script from ```package.json```:

```bash
$ [sudo] npm start
```

*   Start using ```node```:

```bash
$ [sudo] node index.js
```

For production, if you want to start the API in background, monitor it and auto-restart if it crashes:

```bash
$ sudo npm install -g forever
$ [sudo] forever start index.js
$ [sudo] forever list
```

For development, use ```nodemon```. Whenever you change source files, server will restart:

```bash
$ sudo npm install -g nodemon
$ [sudo] nodemon index.js
```

# 3. Importing from text files

Required: Python 2.7 and pip.

Parsing and importing speed is ~100 request per second using:
*   Python 2.7.12,
*   MongoDB 3.4.0,
*   node.js 6.9.2,

on i5-2410M (dual-core with HT at 2.30HGz). PHP and MySQL solution was ~15 requests per second.

```bash
cd reportParser
# Install dateutil if needed.
pip install python-dateutil
chmod +x parseReport.py
# Required arguments are: matching statistika (-s) and report (-r) files, import endpoint (-u), authentication token (-a)(admin's login token or API token with access to /api/importer/v1) and start index (-i, starts at 1)
./parseReport.py -s statistika.primjer -r report.primjer -u http://localhost:8080/api/importer/v1 -a eyJhbG... -i 1
```
