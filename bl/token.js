'use strict';

var appRoot = require('app-root-path');
//var config = require(appRoot + '/config.js');
var db = require(appRoot + '/models/_postgres.js');

module.exports.revokeToken = function(token, tokenOwner, revokedAt, revokedUntil, revokedBy, revocationReason, callback) {
    global.ispravimeLogger.debug("executing revokeToken");
    const query = {
        name: 'revokeToken',
        text: "INSERT INTO revokedtoken(token, tokenOwner, revokedAt, revokedUntil, revokedBy, revocationReason) VALUES($1, $2, $3, $4, $5, $6) RETURNING *",
        values: [token, tokenOwner, revokedAt, revokedUntil, revokedBy, revocationReason],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.deleteOldRevokedTokens = function(callback) {
    global.ispravimeLogger.debug("executing deleteOldRevokedTokens");
    const query = {
        name: 'deleteOldRevokedTokens',
        text: "DELETE FROM revokedToken WHERE revokedUntil < now() RETURNING *",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getRevokedToken = function(token, callback) {
    global.ispravimeLogger.debug("executing getRevokedToken");
    const query = {
        name: 'getRevokedToken',
        text: "SELECT * FROM revokedToken WHERE token = $1",
        values: [token],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteRevokedTokens = function(callback) {
    global.ispravimeLogger.debug("executing deleteRevokedTokens");
    const query = {
        name: 'deleteRevokedTokens',
        text: "DELETE FROM revokedToken RETURNING *",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};