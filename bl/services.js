'use strict';

var appRoot = require('app-root-path');
//var config = require(appRoot + '/config.js');
var db = require(appRoot + '/models/_postgres.js');

module.exports.getServices = function(callback) {
    global.ispravimeLogger.debug("executing getServices");
    const query = {
        name: 'getServices',
        text: "SELECT * FROM Service",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getServiceByName = function(name, callback) {
    global.ispravimeLogger.debug("executing getServiceByName");
    const query = {
        name: 'getServiceByName',
        text: "SELECT * FROM Service WHERE name = $1",
        values: [name],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            callback(err); 
        }

        callback(null, res.rows);
    });
};

module.exports.createService = function(name, description, callback) {
    global.ispravimeLogger.debug("executing createService");
    const query = {
        name: 'createService',
        text: "INSERT INTO Service(name, description) VALUES ($1, $2) RETURNING *",
        values: [name, description],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            callback(err); 
        }

        callback(null, res.rows[0]);
    });
};

module.exports.getServiceDetails = function(id, callback) {
    global.ispravimeLogger.debug("executing getServiceDetails");
    const query = {
        name: 'getServiceDetails',
        text: "SELECT * FROM Service WHERE id = $1",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`Service with id ${id} does not exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.deleteService = function(id, callback) {
    global.ispravimeLogger.debug("executing deleteService");
    const query = {
        name: 'deleteService',
        text: "DELETE FROM Service WHERE id = $1 RETURNING id, name, description",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`Service with id ${id} does not exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.deleteServiceByName = function(name, callback) {
    global.ispravimeLogger.debug("executing deleteServiceByName");
    const query = {
        name: 'deleteServiceByName',
        text: "DELETE FROM Service WHERE name = $1 RETURNING id, name, description",
        values: [name],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.createServiceEndpoints = function(method, description, noauthrequired, allowedbydefault, serviceid, uri, callback) {
    global.ispravimeLogger.debug("executing createServiceEndpoints");
    const query = {
        name: 'createServiceEndpoints',
        text: "INSERT INTO Serviceendpoint(method, description, noAuthRequired, allowedByDefault, serviceid, uri) VALUES($1, $2, $3, $4, $5, $6) RETURNING Id, method, description, noAuthRequired, allowedByDefault, serviceid, uri",
        values: [method, description, noauthrequired, allowedbydefault, serviceid, uri],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getServiceEndpoints = function(callback) {
    global.ispravimeLogger.debug("executing getServiceEndpoints");
    const query = {
        name: 'getServiceEndpoints',
        text: "SELECT * FROM Serviceendpoint",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.createAllowedRoles = function(name, serviceendpointid, callback) {
    global.ispravimeLogger.debug("executing createAllowedRoles");
    const query = {
        name: 'createAllowedRoles',
        text: "INSERT INTO AllowedRoles(name, serviceendpointid) VALUES($1, $2) RETURNING Id, name, serviceendpointid",
        values: [name, serviceendpointid],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

// module.exports.createServiceEndpointsWithRolesBulk = function(endpoints, callback) {
//     global.ispravimeLogger.debug("executing createServiceEndpointsWithRolesBulk");
//     var endpointsAdded = [];
//     for (var i = 0; i < endpoints.length; i++) {
//     }

//     return callback(null, endpointsAdded);
// };

module.exports.deleteServiceEndpoint = function(serviceid, uri, method, callback) {
    global.ispravimeLogger.debug("executing deleteServiceEndpoint");
    const query = {
        name: 'deleteServiceEndpoint',
        text: "DELETE FROM ServiceEndpoint WHERE serviceId = $1 AND uri = $2 AND method = $3 RETURNING id, method, description, noauthrequired, allowedbydefault, serviceid, uri",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getServiceEndpointByMethodAndUri = function(mehod, uri, callback){
    //hmm čemu ovo
    global.ispravimeLogger.debug("executing getServiceEndpointByMethodAndUri");
    const query = {
        name: 'test',
        text: "SELECT * FROM serviceendpoint WHERE method = $1 AND uri = $2",
        values: [method, uri],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getAllowedRolesForEndpoint = function(method, uri) {
    global.ispravimeLogger.debug("executing getAllowedRolesForEndpoint");
    const query = {
        name: 'getAllowedRolesForEndpoint',
        text: "SELECT name FROM ServiceEnpointAllowedRoles JOIN ServiceEndpoint ON serviceEndpointId = serviceEndpoint.id WHERE method = $1 AND uri = $2",
        values: [method, uri],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getFullServices = function(callback) {
    global.ispravimeLogger.debug("executing getFullServices");
    const query = {
        name: 'getFullServices',
        text: "SELECT s.*, (SELECT ARRAY(SELECT to_json(subq.*) FROM (SELECT se.*, (SELECT ARRAY(SELECT to_json(sear.*) FROM AllowedRoles AS sear WHERE sear.serviceendpointid = se.id)) AS allowedRoles FROM serviceEndpoint AS se WHERE se.serviceid = s.id) as subq)) AS endpoints FROM service AS s",
        values: [],
        //rowMode: 'array'
    };
    
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getServiceEndpointsByService = function(serviceName, method, uri, callback) {
    global.ispravimeLogger.debug("executing getServiceEndpointsByService");
    const query = {
        name: 'getServiceEndpointsByService',
        text: "SELECT se.* FROM serviceendpoint AS se JOIN service AS s ON s.id = se.serviceid WHERE se.method = $1 AND se.uri = $2 AND s.name = $3",
        values: [method, uri, serviceName],
        //rowMode: 'array'
    };
    
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteServiceAccess = function(adminId, uri, method, callback) {
    global.ispravimeLogger.debug("executing deleteServiceAccess");
    const query = {
        name: 'deleteServiceAccess',
        text: "DELETE FROM serviceaccess WHERE uri = $1 AND method = $2 AND adminid = $3 RETURNING *",
        values: [uri, method, adminId],
        //rowMode: 'array'
    };
    
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getServiceAccessByAdmin = function(adminId, callback) {
    global.ispravimeLogger.debug("executing getServiceAccessByAdmin");
    const query = {
        name: 'getServiceAccessByAdmin',
        text: "SELECT * FROM serviceaccess WHERE adminid = $1",
        values: [adminId],
        //rowMode: 'array'
    };
    
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}
