'use strict';

var appRoot = require('app-root-path');
//var config = require(appRoot + '/config.js');
var db = require(appRoot + '/models/_postgres.js');

module.exports.getNetworks = function(callback) {
    global.ispravimeLogger.debug("executing getNetworks");
    const query = {
        name: 'getNetworks',
        text: "SELECT * FROM _network ORDER BY name",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getNetworksCountByNameAndMask = function(name, mask, callback) {
    global.ispravimeLogger.debug("executing getNetworksCount");
    const query = {
        name: 'getNetworksCount',
        text: "SELECT COUNT(*) as count FROM _network  WHERE name LIKE $1 AND mask LIKE $2",
        values: [name, mask],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.createNetwork = function(name, mask, allowed, callback) {
    global.ispravimeLogger.debug("executing createNetwork");
    const query = {
        name: 'createNetwork',
        text: "INSERT INTO _network(name, mask, allowed) VALUES ($1, $2, $3) RETURNING *",
        values: [name, mask, allowed],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.updateNetwork = function(id, name, mask, allowed, callback) {
    global.ispravimeLogger.debug("executing updateNetwork");
    const query = {
        name: 'updateNetwork',
        text: "UPDATE _network SET name = $1, mask = $2, allowed = $3 OR allowed WHERE id = $4 RETURNING *",
        values: [name, mask, allowed, id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`Network with id ${id} does no exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.deleteNetwork = function(id, callback) {
    global.ispravimeLogger.debug("executing deleteNetwork");
    const query = {
        name: 'deleteNetwork',
        text: "DELETE FROM _network WHERE id = $1 RETURNING *",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`Network with id ${id} does no exist!`);
        }

        callback(null, res.rows[0])
    });
};

module.exports.deleteNetworkByNameAndMask = function(name, mask, callback) {
    global.ispravimeLogger.debug("executing deleteNetworkByNameAndMask");
    const query = {
        name: 'deleteNetworkByNameAndMask',
        text: "DELETE FROM _network WHERE name = $1 AND mask = $2 RETURNING id, name, mask, allowed",
        values: [name, mask],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            callback(err); 
        }

        callback(null, res.rows)
    });
}
