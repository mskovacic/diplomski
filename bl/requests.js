'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const db = require(appRoot + '/models/_postgres.js');

module.exports.getRequestsByUser = function(userId, skip, limit, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`executing getRequestsByUser ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = ["userid = $1"];
    const name = filterObject.name;
    if (name) {
        whereFilters.push(`name LIKE '%${name}%'`);
    }

    const rt = filterObject.requesttime;
    if (rt) {
        if (rt.comparison && rt.value) {
            whereFilters.push(`requesttime ${rt.comparison} '${rt.value}'`);
        }
        
        if (rt.from) {
            whereFilters.push(`requesttime >= '${rt.from}'`);
        }

        if (rt.until) {
            whereFilters.push(`requesttime <= '${rt.until}'`);
        }
    }

    const pt = filterObject.processingtime;
    if (pt) {
        if (pt.comparison && pt.value)
        {
            whereFilters.push(`processingtime ${pt.comparison} '${pt.value}'`);
        }

        if (pt.from) {
            whereFilters.push(`processingtime >= '${pt.from}'`);
        }
        
        if (pt.until) {
            whereFilters.push(`processingtime <= '${pt.until}'`);
        }
    }

    const wc = filterObject.wordcount;
    if (wc) {
        if (wc.comparison && wc.value) {
            whereFilters.push(`wordcount ${wc.comparison} '${wc.value}'`);
        }

        if (wc.from) {
            whereFilters.push(`wordcount >= '${wc.from}'`);
        }

        if (wc.until) {
            whereFilters.push(`wordcount <= '${wc.until}'`);
        }
    }

    const nm = filterObject.numberofmistakes;
    if (nm) {
        if (nm.comparison && nm.value) {
            whereFilters.push(`mistakecount ${nm.comparison} '${nm.value}'`);
        }

        if (nm.from) {
            whereFilters.push(`mistakecount >= '${nm.from}'`);
        }

        if (nm.until) {
            whereFilters.push(`mistakecount <= '${nm.until}'`);
        } 
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }
    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";    
    const query = {
        name: 'getRequestsByUser',
        text: `SELECT r.*, name FROM request AS r JOIN _user AS u ON r.userid = u.id  ${WHERE} ${ORDER} OFFSET $2 LIMIT $3`,
        values: [userId, SKIP, LIMIT]
    };
    const query2 = {
        name: 'getRequestsCount',
        text: `SELECT COUNT(*) as count FROM request ${WHERE}`,
        values: [userId]
    };
    const requests = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        requests.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            requests.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, requests);
        });
    });
};

module.exports.getRequests = function(skip, limit, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`executing getRequests ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = [];
    const name = filterObject.name;
    if (name) {
        whereFilters.push(`name LIKE '%${name}%'`);
    }

    const rt = filterObject.requesttime;
    if (rt) {
        if (rt.comparison && rt.value) {
            whereFilters.push(`requesttime ${rt.comparison} '${rt.value}'`);
        }
        
        if (rt.from) {
            whereFilters.push(`requesttime >= '${rt.from}'`);
        }

        if (rt.until) {
            whereFilters.push(`requesttime <= '${rt.until}'`);
        }
    }

    const pt = filterObject.processingtime;
    if (pt) {
        if (pt.comparison && pt.value)
        {
            whereFilters.push(`processingtime ${pt.comparison} '${pt.value}'`);
        }

        if (pt.from) {
            whereFilters.push(`processingtime >= '${pt.from}'`);
        }
        
        if (pt.until) {
            whereFilters.push(`processingtime <= '${pt.until}'`);
        }
    }

    const wc = filterObject.wordcount;
    if (wc) {
        if (wc.comparison && wc.value) {
            whereFilters.push(`wordcount ${wc.comparison} '${wc.value}'`);
        }

        if (wc.from) {
            whereFilters.push(`wordcount >= '${wc.from}'`);
        }

        if (wc.until) {
            whereFilters.push(`wordcount <= '${wc.until}'`);
        }
    }

    const nm = filterObject.numberofmistakes;
    if (nm) {
        if (nm.comparison && nm.value) {
            whereFilters.push(`mistakecount ${nm.comparison} '${nm.value}'`);
        }

        if (nm.from) {
            whereFilters.push(`mistakecount >= '${nm.from}'`);
        }

        if (nm.until) {
            whereFilters.push(`mistakecount <= '${nm.until}'`);
        } 
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }

    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";    
    const query = {
        name: 'getRequests',
        text: `SELECT r.*, name FROM request AS r JOIN _user AS u ON r.userid = u.id  ${WHERE} ${ORDER} OFFSET $1 LIMIT $2`,
        values: [SKIP, LIMIT]
    };
    const query2 = {
        name: 'getRequestsCount',
        text: `SELECT COUNT(*) as count FROM request ${WHERE}`,
        values: []
    };
    const requests = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        requests.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            requests.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, requests);
        });
    });
}

module.exports.getMistakes = function(from, until, skip, limit, perRequest, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`executing getMistakes ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const OPERATION = perRequest == 'true' ? 'COUNT' : 'SUM';
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = ["requesttime >= $1", "requesttime <= $2"];
    const havingFilters = [];
    const severity = filterObject.severity;
    if (severity) {
        whereFilters.push(`severity LIKE '%${severity}%'`);
    }

    const suspicious = filterObject.suspicious;
    if (suspicious) {
        whereFilters.push(`suspicious LIKE '%${suspicious}%'`);
    }

    const nm = filterObject.occurrences;
    if (nm) {
        if (nm.comparison && nm.value) {
            havingFilters.push(`${OPERATION}(occurrences) ${nm.comparison} '${nm.value}'`);
        }

        if (nm.from) {
            havingFilters.push(`${OPERATION}(occurrences) >= '${nm.from}'`);
        }

        if (nm.until) {
            havingFilters.push(`${OPERATION}(occurrences) <= '${nm.until}'`);
        } 
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }

    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";    
    const HAVING = havingFilters.length > 0 ? `HAVING ${havingFilters.join(' AND ')}`: "";
    const query = {
        name: 'getMistakes',
        text: `SELECT m.*, ${OPERATION}(occurrences) AS occurrences FROM mistake AS m JOIN mistakerequest AS mr ON m.id = mr.mistakeid JOIN request AS r ON r.id = mr.requestid ${WHERE} GROUP BY m.id ${HAVING} ${ORDER} OFFSET $3 LIMIT $4`,
        values: [from, until, skip, limit]
    };
    global.ispravimeLogger.debug(`${query.text}`)
    const query2 = {
        name: 'getMistakesCount',
        text: `SELECT COUNT(DISTINCT m.id) AS count FROM mistake AS m JOIN mistakerequest AS mr ON m.id = mr.mistakeid JOIN request AS r ON r.id = mr.requestid ${WHERE} GROUP BY m.id ${HAVING}`,
        values: [from, until]
    };
    const mistakes = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        mistakes.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            mistakes.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, mistakes);
        });
    });
};

module.exports.getUniqueMistakes = function(from, until, skip, limit, perRequest, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`executing getUniqueMistakes ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const OPERATION = perRequest == 'true' ? 'COUNT' : 'SUM';
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = ["requesttime >= $1", "requesttime <= $2"];
    const havingFilters = [];
    const severity = filterObject.severity;
    if (severity) {
        whereFilters.push(`severity LIKE '%${severity}%'`);
    }

    const suspicious = filterObject.suspicious;
    if (suspicious) {
        whereFilters.push(`suspicious LIKE '%${suspicious}%'`);
    }

    const nm = filterObject.occurrences;
    if (nm) {
        if (nm.comparison && nm.value) {
            havingFilters.push(`${OPERATION}(occurrences) ${nm.comparison} '${nm.value}'`);
        }

        if (nm.from) {
            havingFilters.push(`${OPERATION}(occurrences) >= '${nm.from}'`);
        }

        if (nm.until) {
            havingFilters.push(`${OPERATION}(occurrences) <= '${nm.until}'`);
        } 
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }

    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";    
    const HAVING = havingFilters.length > 0 ? `HAVING ${havingFilters.join(' AND ')}`: "";
    const query = {
        name: 'getUniqueMistakes',
        text: `SELECT MIN(mr.mistakeid) as id, u.severity, u.suspicious, ${OPERATION}(occurrences) AS occurrences FROM uniquemistake AS u JOIN mistakerequest AS mr ON u.id = mr.uniquemistakeid JOIN request AS r ON r.id = mr.requestid ${WHERE} GROUP BY u.id ${HAVING} ${ORDER} OFFSET $3 LIMIT $4`,
        values: [from, until, skip, limit]
    };
    global.ispravimeLogger.debug(`${query.text}`)
    const query2 = {
        name: 'getUniqueMistakesCount',
        text: `SELECT COUNT(DISTINCT u.*) AS count FROM uniquemistake AS u JOIN mistakerequest AS mr ON u.id = mr.uniquemistakeid JOIN request AS r ON r.id = mr.requestid ${WHERE} GROUP BY u.id ${HAVING}`,
        values: [from, until]
    };
    const mistakes = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        mistakes.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            mistakes.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, mistakes);
        });
    });
};

module.exports.getRequestDetails = function(id, callback) {
    global.ispravimeLogger.debug("executing getRequestDetails");
    const query = {
        name: 'getRequestDetails',
        text: "SELECT r.*, name FROM request AS r JOIN _user AS u ON r.userid = u.id WHERE r.id = $1",
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`Zahtjev ${id} ne postoji`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getMistakeDetails = function(id, callback) {
    global.ispravimeLogger.debug("executing getMistakeDetails");
    const query = {
        name: 'getMistakeDetails',
        text: "SELECT * FROM mistake WHERE id = $1",
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

function andOr(val) {
    if (val == undefined) {
        return "OR";
    }

    return "AND";
}

module.exports.getRequestsCount = function(from, until, callback) {
    global.ispravimeLogger.debug("executing getRequestsCount");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const query = {
        name: 'getRequestsCount',
        text: `SELECT COUNT(*) as a FROM request WHERE requesttime >= $1 AND requesttime <= $2`,
        values: [from, until],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0][0]);
    });
};

module.exports.getRequestCountByUser = function(from, until, userId, callback) {
    global.ispravimeLogger.debug("executing getRequestCountByUser");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    if (userId == undefined) {
        userId = null
    }

    const query = {
        name: 'getRequestCountByUser',
        text: `SELECT COUNT(*) as a FROM request WHERE requesttime >= $1 AND requesttime <= $2 AND userId = $3`,
        values: [from, until, userId],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0][0]);
    });
};

module.exports.getMistakesSumEver = function(callback) {
    global.ispravimeLogger.debug("executing getMistakesSumEver");
    const query = {
        name: 'getMistakesSumEver',
        text: 'SELECT SUM(occurrences) as total FROM mistakerequest',
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getMistakesSum = function(from, until, callback) {
    global.ispravimeLogger.debug("executing getMistakesSum");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const query = {
        name: 'getMistakesSum',
        text: 'SELECT SUM(occurrences) as total FROM mistakerequest AS mr JOIN request AS r ON mr.requestid = r.id WHERE requesttime >= $1 AND requesttime <= $2',
        values: [from, until]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getMistakesCountEver = function(callback) {
    global.ispravimeLogger.debug("executing getMistakesCountEver");
    const query = {
        name: 'getMistakesCountEver',
        text: 'SELECT COUNT(DISTINCT m.id) FROM mistake AS m',
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getMistakesCount = function(from, until, callback) {
    global.ispravimeLogger.debug("executing getMistakesCount");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const query = {
        name: 'getMistakesCount',
        text: 'SELECT COUNT(DISTINCT m.id) FROM mistake AS m JOIN mistakerequest AS mr ON m.id = mr.mistakeid JOIN request AS r ON mr.requestid = r.id WHERE requesttime >= $1 AND requesttime <= $2',
        values: [from, until]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getUniqueMistakesCountEver = function(callback) {
    global.ispravimeLogger.debug("executing getUniqueMistakesCountEver");
    const query = {
        name: 'getUniqueMistakesCountEver',
        text: 'SELECT COUNT(DISTINCT u.id) AS total FROM uniquemistake AS u',
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getUniqueMistakesCount = function(from, until, callback) {
    global.ispravimeLogger.debug("executing getUniqueMistakesCount");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const query = {
        name: 'getUniqueMistakesCount',
        text: 'SELECT COUNT(DISTINCT u.id) AS count FROM uniquemistake AS u JOIN mistakerequest AS mr ON u.id = mr.uniquemistakeid JOIN request AS r ON mr.requestid = r.id WHERE requesttime >= $1 AND requesttime <= $2',
        values: [from, until]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getTopMistakes = function(from, until, limit, perRequest, callback) {
    global.ispravimeLogger.debug("executing getTopMistakes");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    if (limit == undefined) {
        limit = config.defaultPageSize;
    }

    const OCCURRENCES = perRequest == 'true' ? 'COUNT' : 'SUM';
    const query = {
        name: 'getTopMistakes',
        text: `SELECT m.id as id, suspicious, ${OCCURRENCES}(occurrences) as occurrences 
                FROM mistake AS m
                JOIN mistakerequest AS mr ON mr.mistakeid = m.id
                JOIN request AS r ON r.id = mr.requestid
                WHERE requesttime >= $1 AND requesttime <= $2
                GROUP BY suspicious, m.id
                ORDER BY ${OCCURRENCES}(occurrences) 
                DESC LIMIT $3`,
        values: [from, until, limit]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getTopUniqueMistakes = function(from, until, limit, perRequest, callback) {
    global.ispravimeLogger.debug("executing getTopUniqueMistakes");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    if (limit == undefined) {
        limit = config.defaultPageSize;
    }

    const occurrences = perRequest == 'true' ? 'COUNT' : 'SUM';
    const query = {
        name: 'getTopUniqueMistakes',
        text: `SELECT MIN(mr.mistakeid) AS id, suspicious, ${occurrences}(occurrences) as occurrences
                FROM uniquemistake as u 
                JOIN mistakerequest AS mr ON mr.uniquemistakeid = u.id
                JOIN request AS r ON r.id = mr.requestid
                WHERE requesttime >= $1 AND requesttime <= $2
                GROUP BY suspicious
                ORDER BY ${occurrences}(occurrences) DESC
                LIMIT $3`,
        values: [from, until, limit]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getMistakesRatio = function(from, until, perRequest, callback) {
    global.ispravimeLogger.debug("executing getMistakesRatio");
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const operation = perRequest == 'true' ? "COUNT" : "SUM";
    const query = {
        name: 'getMistakesRatio',
        text: `SELECT severity AS id, ${operation}(occurrences) AS count
                FROM uniquemistake AS u 
                JOIN mistakerequest AS mr ON u.id = mr.uniquemistakeid
                JOIN request AS r ON mr.requestid = r.id
                WHERE requesttime >= $1 AND requesttime <= $2
                GROUP BY severity
                ORDER BY ${operation}(occurrences) DESC`,
        values: [from, until],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getAvg = function(from, until, callback) {
    global.ispravimeLogger.debug(`executing getAvg ${from} ${until}`);
    if (from == undefined || from == 'undefined') {
        from = new Date('1970-01-01T00:00:00').toISOString();
    }

    if (until == undefined || until == 'undefined') {
        until = new Date().toISOString();
    }

    const query = {
        name: 'getAvg',
        text: "SELECT avg(processingtime) as processing, avg(wordcount) as words, avg(mistakecount) as mistakes FROM request WHERE requesttime >= $1 AND requesttime <= $2",
        values: [from, until]
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getLastRequest = function(callback) {
    global.ispravimeLogger.debug("executing getLastRequest");
    const query = {
        name: 'getLastRequest',
        text: 'SELECT max(requesttime) FROM request',
        values: [],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0][0]);
    });
};

module.exports.createRequest = function(
    requestTime,
    processingTime,
    wordCount,
    ip,
    userID,
    context,
    mistakes,
    textFile,
    callback) {
    global.ispravimeLogger.debug("executing createRequest");
    const mistakeCount = mistakes.map(x => x.occurrences).reduce((a, b) => a + b, 0);
    const uniqueMistakeCount = mistakes.length;
    const query1 = {
        name: 'createRequest',
        text: 'INSERT INTO request(requesttime, processingtime, ipaddress, wordcount, userid, context, ignore, mistakeCount, uniqueMistakeCount, textfile) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING *',
        values: [requestTime, processingTime, ip, wordCount, userID, context, false, mistakeCount, uniqueMistakeCount, textFile]
    };
    db.execute(query1, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.createMistake = function(suspicious, suggestions, severity, occurrences, requestId, callback) {
    global.ispravimeLogger.debug("executing createMistake");
    const suggestionsJSON = JSON.stringify(suggestions);
    const query1 = {
        name: 'createMistake1',
        text: `SELECT * FROM Mistake WHERE suspicious = $1`,
        values: [suspicious]
    };
    const query2 = {
        name: 'createMistake2',
        text: `SELECT * FROM UniqueMistake WHERE suspicious = LOWER($1)`,
        values: [suspicious],
        //rowMode: 'array'
    };
    return callback('not_implemented_exception');
    // db.execute(query1, (err, res) => {
    //     if (err){
    //         return callback(err); 
    //     }

    //     var mistakeId;
    //     if (res.rows.length == 0) {
    //         mistakeId = (function() {
    //             db.execute(query2, (err, res) => {
    //                 return res.rows[0];  
    //             });
    //         }) ();
    //     }

    //     mistake = res.rows[0];
    //     mistakeId = mistake.id;
    //     const query3 = {
    //         name: 'createMistake3',
    //         text: 'INSERT INTO mistakerequest(mistakeid, requestId, occurrences) VALUES ($1, $2, $3) RETURNING *',
    //         values: [mistakeId,  requestId, occurrences],
    //     };
    //     db.execute(query3, (err, res) => {
    //         return callback(null, mistake);
    //     });  
    // });
    db.execute(query1, (err, res) => {
        if (err) {
            return callback(err);
        }

        var mistake = res.rows[0];
        if (mistake == undefined) {
            //INSERT
            (function (suspicious, suggestions, occurrences, callback) {
                var query = {
                    name: 'createMistake',
                    text: `INSERT INTO mistake(suspicious, suggestions, totaloccurrences) VALUES ($1, $2, $3) RETURNING *`,
                    values: [suspicious, JSON.stringify(suggestions), occurrences]
                };
                db.execute(query, (err, res) => {
                    if (err) {
                        return callback(err);
                    }

                    return callback(null, res.rows[0]);
                });                    
            }) (suspicious, suggestions, occurrences, (err, success) => {
                if (err) {
                    global.ispravimeLogger.error(err);
                }

                mistake = success;
            });
        } else {
            //UPDATE
            (function(mistakeId, occurrences) {

            })(mistake.id, occurrences);
        } 
        const mistakeId = mistake.id;
        db.execute(query2, (err, res) => {
            if (err) {
                return callback(err);
            }
    
            const uniqueMistake = res.rows[0];
            if (uniqueMistake == undefined) {
                //INSERT
                uniqueMistake = (function (suspicious, suggestions, occurrences, callback) {
                    var query = {
                        name: 'createMistakeUnique',
                        text: `INSERT INTO uniquemistake(suspicious, suggestions, totaloccurrences) VALUES ($1, $2, $3) RETURNING *`,
                        values: [suspicious, JSON.stringify(suggestions), occurrences]
                    };
                    db.execute(query, (err, res) => {
                        if (err) {
                            callback(err);
                        }

                        return callback(null, res.rows[0]);
                    });                    
                }) (suspicious, suggestions, occurrences, (err, success) => {
                    if (err) {
                        global.ispravimeLogger.error(err);
                    }

                    uniqueMistake = success;
                });
            } else {
                //UPDATE
                (function(uniqueMistakeId, occurrences) {

                })(uniqueMistake.id, occurrences);
            }
            const uniqueMistakeId = uniqueMistake.id;
            const query3 = {
                name: 'createMistake3',
                text: `INSERT INTO mistakerequest(mistakeid, uniquemistakeid, occurrences) VALUES ($1, $2, $3) RETURNING *`,
                values: [mistakeId, uniqueMistakeId, occurrences],
                //rowMode: 'array'
            };
            db.execute(query3, (err, res) => {
                if (err) {
                    return callback(err);
                }

                return callback(null, res.rows[0]);
            });
        });
    });
}

// module.exports.getFullUsers = function(callback) {
//     global.ispravimeLogger.debug("executing getFullUsers");
//     const query = {
//         name: 'getFullUsers',
//         text: "SELECT u.*, (SELECT ARRAY(SELECT to_json(subq.*) FROM (SELECT r.*, (SELECT ARRAY(SELECT to_json(m.*) FROM mistake AS m WHERE m.requestid = r.id)) AS mistakes FROM request AS r WHERE r.userid = u.id) as subq)) AS requests from _user as u",
//         values: [],
//         //rowMode: 'array'
//     };
//     db.execute(query, (err, res) => {
//         if (err){
//             return callback(err); 
//         }

//         return callback(null, res.rows);
//     });
// }

module.exports.getMistakesByRequest = function(requestId, callback) {
    global.ispravimeLogger.debug("executing getMistakesByRequest");
    const query = {
        name: 'getMistakesByRequest',
        text: "SELECT * FROM Mistake AS m JOIN mistakerequest AS mr on m.id = mr.mistakeid WHERE requestid = $1",
        values: [requestId]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getRequestsBySuspicious = function(suspicious, callback) {
    global.ispravimeLogger.debug("executing getRequestsBySuspicious");
    const query = {
        name: 'getRequestsBySuspicious',
        text: "SELECT r.*, name, occurrences FROM mistake AS m JOIN mistakerequest AS mr ON m.id = mr.mistakeid JOIN request AS r ON mr.requestid = r.id JOIN _user AS u ON r.userid = u.id WHERE suspicious = $1",
        values: [suspicious]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getRequestsByMistakeId = function(mistakeid, callback) {
    global.ispravimeLogger.debug("executing getRequestsByMistakeId");
    const query = {
        name: 'getRequestsByMistakeId',
        text: "SELECT r.*, name, occurrences FROM mistakerequest AS mr JOIN request AS r ON mr.requestid = r.id JOIN _user AS u ON r.userid = u.id WHERE mistakeid = $1",
        values: [mistakeid]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.updateRequest = function(id, requestTime, processingTime, ipAddress, wordCount, mistakeCount, uniqueMistakeCount, userId, textFile, context, ignore, callback) {
    global.ispravimeLogger.debug("executing updateRequest");
    const query = {
        name: 'updateRequest',
        text: "UPDATE request SET requestTime = $1, processingTime = $2, ipAddress = $3, wordCount = $4, mistakeCount = $5, uniqueMistakeCount = $6, userId =  $7, textFile = $8, context = $9, ignore = $10 WHERE id = $11 RETURNING *",
        values: [requestTime, processingTime, ipAddress, wordCount, mistakeCount, uniqueMistakeCount, userId, textFile, context, ignore, id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.deleteMistakes = function(callback) {
    global.ispravimeLogger.debug("executing deleteMistakes");
    const query = {
        name: 'deleteMistakes',
        text: "DELETE FROM mistake RETURNING *",
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}
