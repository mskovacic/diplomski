'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');
var db = require(appRoot + '/models/_postgres.js');

module.exports.getUsers = function(skip, limit, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`getUsers ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = [];
    const name = filterObject.name;
    if (name) {
        whereFilters.push(`name LIKE '%${name}%'`);
    }

    const fa = filterObject.firstAccess;
    if (fa) {
        if (fa.comparison && fa.value) {
            whereFilters.push(`firstaccess ${fa.comparison} '${fa.value}'`);
        }
        
        if (fa.from) {
            whereFilters.push(`firstaccess >= '${fa.from}'`);
        }

        if (fa.until) {
            whereFilters.push(`firstaccess <= '${fa.until}'`);
        }
    }

    const la = filterObject.lastAccess;
    if (la) {
        if (la.comparison && la.value)
        {
            whereFilters.push(`lastaccess ${la.comparison} '${la.value}'`);
        }

        if (la.from) {
            whereFilters.push(`lastaccess >= '${la.from}'`);
        }
        
        if (la.until) {
            whereFilters.push(`lastaccess <= '${la.until}'`);
        }
    }

    const nr = filterObject.numberOfRequests;
    if (nr) {
        if (nr.comparison && nr.value) {
            whereFilters.push(`numberofrequests ${nr.comparison} '${nr.value}'`);
        }

        if (nr.from) {
            whereFilters.push(`numberofrequests >= '${nr.from}'`);
        }

        if (nr.until) {
            whereFilters.push(`numberofrequests <= '${nr.until}'`);
        }
    }

    const nm = filterObject.numberOfMistakes;
    if (nm) {
        if (nm.comparison && nm.value) {
            whereFilters.push(`numberofmistakes ${nm.comparison} '${nm.value}'`);
        }

        if (nm.from) {
            whereFilters.push(`numberofmistakes >= '${nm.from}'`);
        }

        if (nm.until) {
            whereFilters.push(`numberofmistakes <= '${nm.until}'`);
        } 
    }

    const ip = filterObject.lastIP;
    if (ip) {
        if (ip.from) {
            whereFilters.push(`lastip >= '${ip.from}'`);
        }

        if (ip.until) {
            whereFilters.push(`lastip <= '${ip.until}'`);
        }  
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }
    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";    
    const query = {
        name: 'getUsers',
        text: `SELECT * FROM _user ${WHERE} ${ORDER} OFFSET $1 LIMIT $2`,
        values: [SKIP, LIMIT]
    };
    const query2 = {
        name: 'getUsersCount',
        text: `SELECT COUNT(*) AS count FROM _user ${WHERE}`,
        values: []
    };
    const users = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        users.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }

            users.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, users);
        });
    });
};

module.exports.getUserRequestStatistics = function(userIds, callback) {
    global.ispravimeLogger.debug("executing getUserRequestStatistics");
    var params = [];
    for(var i = 1; i <= userIds.length; i++) {
        params.push('$' + i);
    }

    if (params.length > 0) {
        const WHERE = ` WHERE userid IN (${params.join(',')})`;
        const query = {
            name: 'getUserRequestStatistics',
            text: `SELECT AVG(processingtime) AS avgrequesttime, COUNT(*) AS numberofrequests, SUM(uniquemistakecount) AS totaluniquemistakes, SUM(mistakecount) AS totalmistakes, SUM(processingtime) AS totalrequesttime FROM request ${WHERE}`,
            values: userIds
        };
        db.execute(query, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            return callback(null, res.rows[0]);
        });
    } 
    else {
        return callback(null, {avgrequesttime: 0, numberofrequests: 0, totaluniquemistakes: 0, totalmistakes: 0, totalrequesttime: 0});
    }
}

module.exports.getUserDetails = function(id, callback) {
    global.ispravimeLogger.debug("executing getUserDetails");
    const query = {
        name: 'getUserDetails',
        text: "SELECT * FROM _user where id = $1",
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`User with id ${id} does not exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.setUserDetails = function(id, name, callback) {
    global.ispravimeLogger.debug("executing setUserDetails");
    const query = {
        name: 'setUserDetails',
        text: "UPDATE _user SET name = $1 WHERE id = $2 RETURNING *",
        values: [name, id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`User with id ${id} does not exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getUserMistakeTypes = function(id, callback) {
    global.ispravimeLogger.debug("executing getUserMistakeTypes");
    const query = {
        name: 'getUserMistakeTypes',
        text: "SELECT severity AS label, SUM(occurrences) AS value FROM mistake AS m JOIN mistakerequest AS mr ON m.id = mr.mistakeid JOIN request AS r ON mr.requestid = r.id WHERE r.userid = $1 GROUP BY severity",
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getUserRequestTimes = function(id, range, callback){
    global.ispravimeLogger.debug("executing getUserRequestTimes");
    var grouping;
    var ordering;
    switch (range) {
        case 'year':
            grouping = "date_part('year', requesttime)";
            ordering = "date_part('year', requesttime)";
            break;
        case 'month':
            grouping = "date_part('year', requesttime) || '-' || date_part('month', requesttime)";
            ordering = "date_part('year', requesttime), date_part('month', requesttime)";
            break;
        case 'day':
            grouping = "date_part('year', requesttime) || '-' || date_part('month', requesttime) || '-' || date_part('day', requesttime)";
            ordering = "date_part('year', requesttime), date_part('month', requesttime), date_part('day', requesttime)";
            break;
        default:
            return callback(`Value of range is invalid!`);
    }
    const query = {
        name: 'getUserRequestTimes',
        text: `SELECT ${grouping} AS label, COUNT(*) AS value FROM request AS r JOIN _user AS u ON r.userid = u.id WHERE u.id = $1 GROUP BY ${grouping}, ${ordering} ORDER BY ${ordering}`,
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getUserRequestStats = function(id, callback) {
    global.ispravimeLogger.debug("executing getUserRequestStats");
    const query = {
        name: 'getUserRequestStats',
        text: "SELECT AVG(wordcount) as avgWordCount, avg(mistakecount) as avgMistakes, SUM(wordcount) as totalWordCount, SUM(mistakecount) as totalMistakes FROM Request JOIN _user ON request.userid = _user.id WHERE _user.id = $1",
        values: [id]
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            return callback(`User with id ${id} does not exist!`);
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getUserCount = function(callback) {
    global.ispravimeLogger.debug("executing getUserCount");
    const query = {
        name: 'getUserCount',
        text: "SELECT COUNT(*) FROM _user",
        values: [],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0][0]);
    });
};

module.exports.getAllUsers = function(callback) {
    global.ispravimeLogger.debug("executing getAllUsers");
    const query = {
        name: 'getAllUsers',
        text: "SELECT * FROM _user",
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.upsertUser = function(id, name, lastaccess, lastip, callback) {
    global.ispravimeLogger.debug("executing upsertUser");
    const updateQuery = {
        name: 'updateUser',
        text: "UPDATE _user SET lastaccess = $1, lastip = $2 WHERE id = $3 RETURNING *",
        values: [lastaccess, lastip, id]
    };
    const insertQuery = {
        name: 'insertUser',
        text: "INSERT INTO _user(id, name, firstaccess, lastaccess, lastip) VALUES ($1, $2, $3, $4, $5) RETURNING *",
        values: [id, name, lastaccess, lastaccess, lastip]
    };
    db.execute(updateQuery, (err, res) => {
        if (err){
            return callback(err); 
        }

        if (res.rowCount == 0) {
            db.execute(insertQuery, (err, res) => {
                if (err){
                    return callback(err); 
                }
        
                const inserted = res;
                updateUserIpHistory(id, lastip, lastaccess, (err, res) => {
                    if (err) {
                        global.ispravimeLogger.error(err);
                    }

                    return callback(null, inserted.rows[0]);
                });
            });
        }
        
        const updated = res;
        updateUserIpHistory(id, lastip, lastaccess, (err, res) => {
            if (err) {
                global.ispravimeLogger.error(err);
            }

            return callback(null, updated.rows[0]);
        });
    });
}

function updateUserIpHistory(userId, Ip, access, callback) {
    global.ispravimeLogger.debug("executing updateUserIpHistory");
    const query = {
        name: 'updateUserIpHistory',
        text: "INSERT INTO UserIPHistory(UserId, IP, Access) VALUES ($1, $2, $3) RETURNING *",
        values: [userId, Ip, access]
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err);
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.getUserIpHistory = function(userId, callback) {
    global.ispravimeLogger.debug("executing getUserIpHistory");
    const query = {
        name: 'getUserIpHistory',
        text: "SELECT DISTINCT ip, userid FROM useriphistory WHERE userId = $1",
        values: [userId]
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err);
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteUsers = function (callback) {
    global.ispravimeLogger.debug("executing deleteUsers");
    const query = {
        name: 'deleteUsers',
        text: "DELETE FROM _user RETURNING *",
        values: []
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err);
        }

        return callback(null, res.rows);
    });
}
