'use strict';

var appRoot = require('app-root-path');
//var config = require(appRoot + '/config.js');
var db = require(appRoot + '/models/_postgres.js');

module.exports.getAdmins = function(skip, limit, filterObject, sortObject, callback) {
    global.ispravimeLogger.debug(`executing getAdmins ${skip} ${limit} ${JSON.stringify(filterObject)} ${JSON.stringify(sortObject)}`);
    const SKIP = skip > 1 ? skip : 0;
    const LIMIT = limit > 1 ? limit : config.defaultPageSize;
    const whereFilters = [];
    const havingFilters = [];
    const email = filterObject.email;
    if (email) {
        whereFilters.push(`email LIKE '%${email}%'`);
    }

    const dr = filterObject.dateregistered;
    if (dr) {
        if (dr.comparison && dr.value) {
            whereFilters.push(`dateregistered ${dr.comparison} '${dr.value}'`);
        }
        
        if (dr.from) {
            whereFilters.push(`dateregistered >= '${dr.from}'`);
        }

        if (dr.until) {
            whereFilters.push(`dateregistered <= '${dr.until}'`);
        }
    }

    const id = filterObject.id;
    if (id) {
        if (id.comparison && id.value)
        {
            whereFilters.push(`a.id ${id.comparison} '${id.value}'`);
        }

        if (id.from) {
            whereFilters.push(`a.id >= '${id.from}'`);
        }
        
        if (id.until) {
            whereFilters.push(`a.id <= '${id.until}'`);
        }
    }

    const nt = filterObject.numtokens;
    if (nt) {
        if (nt.comparison && nt.value) {
            havingFilters.push(`COUNT(t.*) ${nt.comparison} '${nt.value}'`);
        }

        if (nt.from) {
            havingFilters.push(`COUNT(t.*) >= '${nt.from}'`);
        }

        if (nt.until) {
            havingFilters.push(`COUNT(t.*) <= '${nt.until}'`);
        }
    }

    var ORDER = "";
    if (sortObject) {
        if (sortObject.direction && sortObject.columnName) {
            ORDER = ` ORDER BY ${sortObject.columnName} ${sortObject.direction}`;
        }
    }

    const WHERE = whereFilters.length > 0 ? `WHERE ${whereFilters.join(' AND ')}` : "";
    const HAVING = havingFilters.length > 0 ? `HAVING ${havingFilters.join(' AND ')}`: "";    
    const query = {
        name: 'getAdmins',
        text: `SELECT a.id, email, dateregistered, COUNT(t.*) as numtokens FROM _admin as a LEFT JOIN token as t ON a.id = t.adminid ${WHERE} GROUP BY a.id, email, dateregistered ${HAVING} ${ORDER}`,
        values: []
    };
    const query2 = {
        name: 'getAdminsCount',
        text: `SELECT COUNT(*) FROM _admin as a LEFT JOIN token as t ON a.id = t.adminid ${WHERE} GROUP BY a.id, email, dateregistered ${HAVING}`,
        values: []
    };
    const admins = {
        count: 0,
        rows: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        admins.rows = res.rows;
        db.execute(query2, (err, res) => {
            if (err){
                return callback(err); 
            }
    
            admins.count = res.rows.length > 0 ? res.rows[0].count : 0;
            return callback(null, admins);
        });
    });
};

module.exports.getAllAdmins = function(callback) {
    global.ispravimeLogger.debug("executing getAllAdmins");
    const query = {
        name: 'getAllAdmins',
        text: "SELECT * FROM _admin",
        values: []
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.createAdmin = function(email, salt, password, callback) {
    global.ispravimeLogger.debug("executing createAdmin");
    const query = {
        name: 'createAdmin',
        text: "INSERT INTO _admin(email, dateregistered, salt, password) VALUES ($1, now(), $2, $3) RETURNING id, email dateregistered, salt, password",
        values: [email, salt, password],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.createRole = function(name, adminId, callback) {
    global.ispravimeLogger.debug("executing createRole");
    const query = {
        name: 'createRole',
        text: "INSERT INTO _role(name, adminId) VALUES ($1, $2) RETURNING *",
        values: [name, adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.createServiceAccess = function(uri, method, tokenId, adminId, callback) {
    global.ispravimeLogger.debug("executing createServiceAccess");
    const query = {
        name: 'createServiceAccess',
        text: "INSERT INTO ServiceAccess(uri, method, tokenid, adminid) VALUES ($1, $2, $3, $4) RETURNING *",
        values: [uri, method, tokenId, adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getAdminEmails = function(callback) {
    global.ispravimeLogger.debug("executing getAdminEmails");
    const query = {
        name: 'getAdminEmails',
        text: "SELECT id, email FROM _admin",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
};

module.exports.getAdminDetails = function(id, callback) {
    global.ispravimeLogger.debug("executing getAdminDetails");
    const query = {
        name: 'getAdminDetails',
        text: "SELECT id, email, dateregistered, (SELECT ARRAY(SELECT to_json(r.name) FROM _role AS r WHERE r.adminid = a.id)) as roles FROM _admin as a WHERE id = $1",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getAdminRoles = function(adminid, callback) {
    global.ispravimeLogger.debug("executing getAdminRoles");
    const query = {
        name: 'getAdminRoles',
        text: "SELECT name FROM _Role WHERE adminid = $1",
        values: [adminid],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows.map(x => x.name));
    });
};

module.exports.setAdminRole = function(name, adminId, callback) {
    global.ispravimeLogger.debug("executing setAdminRole");
    const query = {
        name: 'setAdminRole',
        text: "INSERT INTO _Role(name,adminId) VALUES ($1, $2) RETURNING *",
        values: [name, adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
}

module.exports.deleteAdminRoles = function(names, adminId, callback) {
    global.ispravimeLogger.debug("executing deleteAdminRoles");
    var params = [];
    for(var i = 1+1; i <= names.length+1; i++) {
        params.push('$' + i);
    }
    names.unshift(adminId);
    const query = {
        name: 'deleteAdminRoles',
        text: `DELETE FROM _Role WHERE adminid = $1 AND name in (${params.join(',')}) RETURNING *`,
        values: names,
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}


module.exports.getAdminByEmail = function(email, callback) {
    global.ispravimeLogger.debug("executing getAdminByEmail");
    const query = {
        name: 'getAdminByEmail',
        text: "SELECT * FROM _Admin WHERE email = $1",
        values: [email],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getAdminTokens = function(callback) {
    global.ispravimeLogger.debug("executing getAdminTokens");
    const query = {
        name: 'getAdminTokens',
        text: "SELECT a.id, a.email, (SELECT ARRAY(SELECT to_json(t.*) FROM token as t WHERE t.adminid = a.id)) AS grantedtokens FROM _admin AS a",
        values: []
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err);
        }

        return callback(null, res.rows);
        // var newRows = [];
        // var dict = {};
        // for (var i=0; i<res.rows.length; i++) {
        //     var row = res.rows[i];
        //     if (row.adminid in dict) {
        //         dict[row.adminid]["grantedTokens"].push({
        //             "token": row.token,
        //              "isvalid": row.isvalid
        //         });
        //     }
        //     else {
        //         dict[row.adminid]={
        //             "id": row.adminid,
        //             "email": row.email,
        //             "grantedTokens": [{
        //                 "token": row.token,
        //                 "isvalid": row.isvalid
        //             }]
        //         };
        //     }
        // }
        
        // for (var key in dict) {
        //     newRows.push(dict[key]);
        // }

        // return callback(null, newRows); 
    });
};

module.exports.getTokensForAdmin = function(adminId, callback) {
    global.ispravimeLogger.debug("executing getTokensForAdmin");
    const query = {
        name: 'getTokensForAdmin',
        text: "SELECT t.*, (SELECT ARRAY(SELECT to_json(sa.*) FROM serviceaccess AS sa WHERE sa.tokenid = t.id)) AS endpoints FROM token AS t WHERE adminid = $1",
        values: [adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getAdminById = function(adminId, callback) {
    global.ispravimeLogger.debug("executing getAdminById");
    const query = {
        name: 'getAdminById',
        text: "SELECT * FROM _admin WHERE id = $1",
        values: [adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

// module.exports.setAdminDetails = function(callback) {
//     global.ispravimeLogger.debug("executing getNetworks");
//     const query = {
//         name: 'test',
//         text: "SELECT * FROM Request",
//         values: [],
//         rowMode: 'array'
//     };
//     db.execute(query, (err, res) => {
//         if (err){
//             callback(err); 
//         }

//         callback(null, res)
//     });
// };

//permissionsEditor
// module.exports.addEndpointPermission = function(callback) {
//     global.ispravimeLogger.debug("executing addEndpointPermission");
//     const query = {
//         name: 'test',
//         text: "SELECT * FROM Request",
//         values: [],
//         rowMode: 'array'
//     };
//     db.execute(query, (err, res) => {
//         if (err){
//             callback(err); 
//         }

//         callback(null, res)
//     });
// };

// module.exports.removeEndpointPermission = function(callback) {
//     global.ispravimeLogger.debug("executing removeEndpointPermission");
//     const query = {
//         name: 'test',
//         text: "SELECT * FROM Request",
//         values: [],
//         rowMode: 'array'
//     };
//     db.execute(query, (err, res) => {
//         if (err){
//             callback(err); 
//         }

//         callback(null, res)
//     });
// };

// module.exports.removeInvalidServiceAccess = function(callback) {
//     global.ispravimeLogger.debug("executing removeInvalidServiceAccess");
//     const query = {
//         name: 'test',
//         text: "SELECT * FROM Request",
//         values: [],
//         rowMode: 'array'
//     };
//     db.execute(query, (err, res) => {
//         if (err){
//             callback(err); 
//         }

//         callback(null, res)
//     });
// };

module.exports.getServiceAccessForAdmin = function(adminId, callback) {
    global.ispravimeLogger.debug("executing getServiceAccessForAdmin");
    const query = {
        name: 'getServiceAccessForAdmin',
        text: "SELECT * FROM ServiceAccess WHERE adminid = $1",
        values: [adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getFullAdmins = function(callback) {
    global.ispravimeLogger.debug("executing getFullAdmins");
    const query = {
        name: 'getFullAdmins',
        text: "SELECT a.*, (SELECT ARRAY(SELECT to_json(sa.*) FROM serviceAccess as sa WHERE sa.adminid = a.id)) AS serviceAccess, (SELECT ARRAY(SELECT to_json(subq.*) FROM (SELECT t.*, (SELECT ARRAY(SELECT to_json(e.*) FROM serviceAccess AS e WHERE e.tokenid = t.id)) AS endpoints FROM token AS t WHERE t.adminid = a.id) as subq)) AS grantedtokens, (SELECT ARRAY(SELECT to_json(r.*) FROM _role as r WHERE r.adminid = a.id)) AS roles FROM _admin AS a",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getAdminByIdOrEmail = function(id, email, callback) {
    global.ispravimeLogger.debug("executing getAdminByIdOrEmail");
    const query = {
        name: 'getAdminByIdOrEmail',
        text: "SELECT * FROM _Admin WHERE id = $1 OR email = $2",
        values: [id, email],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err){
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.createAdminPasswordReset = function(id, passwordresettoken, passwordresetissuedat, callback) {
    global.ispravimeLogger.debug("executing createAdminPasswordReset");
    const query = {
        name: 'createAdminPasswordReset',
        text: "INSERT INTO AdminPasswordReset(Token, IssuedAt, AdminId) VALUES($1, $2, $3) RETURNING *",
        values: [passwordresettoken, passwordresetissuedat, id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getAdminPasswordReset = function(id, callback) {
    global.ispravimeLogger.debug("executing getAdminPasswordReset");
    const query = {
        name: 'getAdminPasswordReset',
        text: "SELECT * FROM AdminPasswordReset WHERE adminId = $1 ORDER BY IssuedAt DESC",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteAdminPasswordReset = function(id, callback) {
    global.ispravimeLogger.debug("executing deleteAdminPasswordReset");
    const query = {
        name: 'deleteAdminPasswordReset',
        text: "DELETE FROM AdminPasswordReset WHERE adminId = $1",
        values: [id],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.updateAdmin = function(id, email, dateregistered, salt, password, callback) {
    global.ispravimeLogger.debug("executing updateAdmin");
    const query = {
        name: 'updateAdmin',
        text: "UPDATE _admin SET email = $1, dateregistered = $2, salt = $3, password = $4 WHERE id = $5 RETURNING *",
        values: [email, dateregistered, salt, password, id]
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.createToken = function(token, adminId, isValid, callback) {
    global.ispravimeLogger.debug("executing createToken");
    const query = {
        name: 'createToken',
        text: "INSERT INTO token(token, adminid, isvalid) VALUES ($1, $2, $3) RETURNING *",
        values: [token, adminId, isValid],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows[0]);
    });
};

module.exports.getTokensByAdmin = function(adminId, callback) {
    global.ispravimeLogger.debug("executing getTokensByAdmin");
    const query = {
        name: 'getTokensByAdmin',
        text: "SELECT * FROM token WHERE adminid = $1",
        values: [adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteTokenForAdmin = function(token, adminId,  callback) {
    global.ispravimeLogger.debug("executing deleteToken");
    const query = {
        name: 'deleteToken',
        text: "DELETE FROM token WHERE token = $1 AND adminid = $2 RETURNING *",
        values: [token, adminId],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.deleteAdmins = function(callback) {
    global.ispravimeLogger.debug("executing deleteAdmins");
    const query = {
        name: 'deleteAdmins',
        text: "DELETE FROM _admin RETURNING *",
        values: [],
        //rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows);
    });
}

module.exports.getAdminCount = function(callback) {
    global.ispravimeLogger.debug("executing getAdminCount");
    const query = {
        name: 'getAdminCount',
        text: "SELECT COUNT(*) FROM _admin",
        values: [],
        rowMode: 'array'
    };
    db.execute(query, (err, res) => {
        if (err) {
            return callback(err); 
        }

        return callback(null, res.rows[0][0]);
    });
}
