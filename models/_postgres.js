'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');
const connectionString = global.testing ? config.postgresqlDBTest : config.postgresqlDB;
const pg = require('pg');
// const poolConfig = {
//     host: 'localhost',
//     user: 'database-user',
//     max: 20,
//     idleTimeoutMillis: 30000,
//     connectionTimeoutMillis: 2000,
// };
/*******************************/
//https://node-postgres.com/features/connecting
//PG TEST
// const pg = require('pg');
// const poll = pg.Pool();
// const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/todo';

// const client = new pool.Client(connectionString);
// client.connect();
// const query = client.query(
//   'CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
// query.on('end', () => { client.end(); });
/*******************************/

module.exports.execute = function(queryObject, callback) {
    const client = new pg.Client(connectionString);
    client.connect((err) => {
        if (err) {
            return callback(err);
        }

        client.query(queryObject, (err, res) => {
            if (err) {
                return callback(err);
            } 

            client.end((err) => {
                if (err) {
                    return callback(err);
                }

                return callback(null, res);
            });
          });
    });
};

module.exports.executeTransaction = function(queryObject, callback) {
    const client = new pg.Client(connectionString);
    client.connect((err) => {
        if (err) {
            return callback(err);
        }

        const shouldAbort = (err) => {
            if (err) {
                console.error('Error in transaction', err.stack);
                client.query('ROLLBACK', (err) => {
                    if (err) {
                        console.error('Error rolling back client', err.stack);
                    }
                    // release the client back to the pool
                    client.end(err => {
                        if (err) {
                            console.error('Error rolling back client', err.stack);
                        }
                    });
                });
            }

            return !!err;
        }

        client.query('BEGIN', (err) => {
            if (shouldAbort(err)) {
                console.error('Query error', err);
                return callback(err);
            }
            
            client.query(queryObject, (err, res) => {
                if (shouldAbort(err)) {
                    console.error('Query error', err);
                    return callback(err);
                }
                
                client.query('COMMIT', (err) => {
                    if (err) {
                      console.error('Error committing transaction', err.stack);
                      return callback(err);
                    }
    
                    client.end(err => {
                        if (err) {
                            console.error('Error rolling back client', err.stack);
                        }
    
                        return callback(null, result);
                    });
                });
            }); 
        });
    });
};

function executePooled(queryObject, callback) {
    // pool.connect((err, client, release) => {
    //     if (err) {
    //         return console.error('Error acquiring client', err.stack)
    //     }

    //     client.query(queryObject, (err, result) => {
    //         release();
    //         if (err) {
    //             return console.error('Error executing query', err.stack)
    //         }
            
    //         callback(null, result);
    //     }); 
    // });
    pool.query(queryObject, (err, res) => {
        if (err) {
            return callback(err);
        } 
        
        return callback(null, res);
    });
}

