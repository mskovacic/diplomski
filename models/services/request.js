var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'requests';

var mistake = new mongoose.Schema({
  'suspicious': { type: String, index: false },
  'suggestions': { type: [String], index: false },
  'severity': { type: String, index: false },
  'occurrences': { type: Number, index: false },
}, { _id : false });

var report = new mongoose.Schema({
  'numberOfMistakes': { type: Number, index: true },
  'mistakes': { type: [mistake], index: false },
}, { _id : false });

var requestSchema = new mongoose.Schema({
  'requestTime': { type: Date, index: true },
  'processingTime': { type: Number, index: true },
  'ip': { type: String, index: true },
  '_ip_buf': { type: Buffer, index: true },
  'wordCount': { type: Number, index: true },
  'userID': { type: mongoose.Schema.Types.ObjectId, index: true },
  'hUserID': { type: String, index: true },
  'userName': { type: String, index: true, default: '' },
  'hRegUserID': { type: String, index: true },
  'context': { type: String, index: true },
  'report': { type: report, index: false },
  'textFile': { type: String, index: false },
  'ignore': { type: Boolean, index: true, default: false }
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, requestSchema);
