var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'networks';

var networkSchema = new mongoose.Schema({
  'name': { type: String, index: true },
  'mask': { type: String, index: true },
  'allowed': { type: Boolean, index: true, default: true }
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, networkSchema);
