var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'users';

var userSchema = new mongoose.Schema({
  'name': { type: String, index: true, default: '' },
  'userID': { type: String, index: true },
  'regUserID': { type: String, index: true },
  'firstAccess': { type: Date, index: true, default: Date.now },
  'lastAccess': { type: Date, index: true, default: Date.now },
  'lastIP': { type: String, index: true },
  '_lastIP_buf': { type: Buffer, index: true },
  'historyIP': { type: [String], index: true },
  '_historyIP_buf': { type: [Buffer], index: true },
  'requestTimes': { type: [Number], index: true },
  'numRequests': { type: Number, index: true, default: 0 },
  'uniqueMistakes': { type: [String], index: true },
  'numMistakesTotal': { type: Number, index: true, default: 0 },
  'ignore': { type: Boolean, index: true, default: false }
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, userSchema);
