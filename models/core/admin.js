var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'admins';
var serviceAccess = new mongoose.Schema({
  'uri': String,
  'method': String
}, { _id : false });
var token = new mongoose.Schema({
  'endpoints': [serviceAccess],
  'token': String
}, { _id : false });
var adminSchema = new mongoose.Schema({
  'email': String,
  'dateRegistered': { type: Date, default: Date.now },
  'salt': String,
  'password': String,
  'passwordReset': {
    'token': String,
    'issuedAt': Date
  },
  'roles': [String],
  'serviceAccess': [serviceAccess],
  'grantedTokens': [token]
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, adminSchema);
