var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'revokedTokens';
var revocationSchema = new mongoose.Schema({
  'token': { type: String, index: true },
  'tokenOwner': { type: String, index: true },
  'revokedAt': Date,
  'revokedUntil': { type: Date, index: true },
  'revokedBy': { type: String, index: true },
  'revocationReason': String
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, revocationSchema);
