var appRoot = require('app-root-path');
var mongoose = require(appRoot + '/models/_mongo.js');

var collectionName = 'services';
var serviceEndpoint = new mongoose.Schema({
  'uri': String,
  'method': String,
  'description': String,
  'noAuthRequired': Boolean,
  'allowedByDefault': Boolean,
  'allowedRoles': [String]
}, { _id : false });
var serviceSchema = new mongoose.Schema({
  'name': String,
  'description': String,
  'endpoints': [serviceEndpoint]
}, {
  'collection': collectionName
});

module.exports = mongoose.model(collectionName, serviceSchema);
