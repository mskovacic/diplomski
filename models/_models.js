var appRoot = require('app-root-path');
var fs = require('fs');

var models = {};
var coreFiles = fs.readdirSync(appRoot + '/models/core');
for(var i in coreFiles) {
  var model = coreFiles[i];
  if (model[0] != '_') {
    var reqPath = appRoot + '/models/core/' + model;
    models[model.slice(0, -3)] = require(reqPath);
  }
}
var serviceFiles = fs.readdirSync(appRoot + '/models/services');
for(var i in serviceFiles) {
  var model = serviceFiles[i];
  if (model[0] != '_') {
    var reqPath = appRoot + '/models/services/' + model;
    models[model.slice(0, -3)] = require(reqPath);
  }
}
module.exports = models;
