var config = require('../config.js');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var whichDB = config.database;
if (global.testing) {
  whichDB = config.databaseTest;
}

function connect() {
  mongoose.connection.close();
  mongoose.connect(whichDB, {
    server: {
      socketOptions: {
        keepAlive: 1000,
        socketTimeoutMS: 30000,
        connectTimeoutMS: 30000,
        auto_reconnect: false
      }
    },
    replset: {
      ha: true,
      haInterval: 10000,
      socketOptions: {
        keepAlive: 1000,
        socketTimeoutMS: 30000,
        connectTimeoutMS: 30000,
        auto_reconnect: false
      }
    }
  }).catch(function() {});
}

var db = mongoose.connection;

if (!global.testing) {
  // Weird bug - testing times out on "before all" if this event has a callback.
  db.on('connected', function() {
    global.ispravimeLogger.log('info', 'Worker ' + global.worker.id + ': MongoDB connected at ' + whichDB);
  });
}

db.on('error', function(err) {
  global.ispravimeLogger.log('error', 'Worker ' + global.worker.id + ': MongoDB connection error: ' + err);
});

db.on('disconnected', function() {
  global.ispravimeLogger.log('error', 'Worker ' + global.worker.id + ': MongoDB disconnected!');
  setTimeout(function() {
      global.ispravimeLogger.log('info', 'Worker ' + global.worker.id + ': Trying to reconnect...');
      connect();
    },
    config.reconnectTimeout);
});

db.once('open', function() {
  global.ispravimeLogger.log('info', 'Worker ' + global.worker.id + ': MongoDB connection opened.');
});

connect();

module.exports = mongoose;
