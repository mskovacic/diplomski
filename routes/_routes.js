'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');
var apicache  = require('apicache');
var redis = false;
var redisClient = false;
var cacheOptions = {
  debug: config.cacheDebugging
};
if (config.useRedis) {
  redis = require('redis');
  redisClient = redis.createClient();
  cacheOptions.redisClient = redisClient;
}
var cache = apicache.options(cacheOptions).middleware;
var fs = require('fs');
module.exports = function(router) {
  var coreFiles = fs.readdirSync(appRoot + '/routes/core');
  for(let i in coreFiles) {
    let route = coreFiles[i];
    if (route[0] != '_') {
      let reqPath = './core/' + route;
      require(reqPath)(router, cache, apicache, redisClient);
    }
  }

  var servicesFiles = fs.readdirSync(appRoot + '/routes/services');
  for(let i in servicesFiles) {
    let route = servicesFiles[i];
    if (route[0] != '_') {
      let reqPath = './services/' + route;
      require(reqPath)(router, cache, apicache, redisClient);
    }
  }
};
