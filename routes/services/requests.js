'use strict';

var appRoot = require('app-root-path');
var config = require(appRoot + '/config.js');
var timestampClear = new require(appRoot + '/utils/timestampClear.js').timestampClear;
var fs = require('fs');
const requestsBL = require(appRoot + '/bl/requests.js');
const servicesBL = require(appRoot + '/bl/services.js');
const filterHelper = require(appRoot + '/utils/filterHelper.js');

if (global.worker.id == 1) {
  // Insert service metadata.
  var serviceData = {
    'name': 'requests',
    'description': 'Requests',
    'endpoints': [{
      'uri': '/api/v1/requests',
      'method': 'GET',
      'description': 'List requests',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/mistakes',
      'method': 'GET',
      'description': 'List mistakes',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/requests/:id',
      'method': 'GET',
      'description': 'Request details',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/mistakes/:id',
      'method': 'GET',
      'description': 'Mistake details',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/count/requests',
      'method': 'GET',
      'description': 'Total number of requests',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/count/mistakes',
      'method': 'GET',
      'description': 'Total number of mistakes',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/count/unqiuemistakes',
      'method': 'GET',
      'description': 'Total number of unique mistakes',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/top/mistakes',
      'method': 'GET',
      'description': 'Most frequent mistakes',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/mistakesRatio',
      'method': 'GET',
      'description': 'Ratio of all mistake types',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/avg',
      'method': 'GET',
      'description': 'Average processing time, word count and mistake count',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/v1/lastRequest',
      'method': 'GET',
      'description': 'Latest request time (newest request)',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }]
  };
  servicesBL.deleteServiceByName(serviceData.name, (err, success)=>{
    if (err) {
      throw err;
    }

    servicesBL.createService(serviceData.name, serviceData.description, (err, success) => {
      if (err) {
        throw err;
      }

      for (var i=0; i<serviceData.endpoints.length; i++) {
        var endpoint = serviceData.endpoints[i];
        (function(endpoint, successid) {
          servicesBL.createServiceEndpoints(
            endpoint.method,
            endpoint.description,
            endpoint.noAuthRequired,
            endpoint.allowedByDefault,
            successid,
            endpoint.uri,
            (err, success) => {
              if (err) {
                console.error(err);
                return;
              }

              for (var i=0; i<endpoint.allowedRoles.length; i++) {
                var role = endpoint.allowedRoles[i];
                (function(role, endpointid){
                  servicesBL.createAllowedRoles(role, endpointid, (err, success) => {
                    if (err) {
                      console.error(err);
                      return;
                    }

                  });
                }) (role, success.id);
              }
          });
        }) (endpoint, success.id);
      }

      global.ispravimeLogger.log('info', 'Updated "' + serviceData.name + '" service\'s information.');
    });
  });
  // model.service.findOne({'name': serviceData.name }, function(err, srv) {
  //   if (err) throw err;
  //   if (srv === null) {
  //     var newService = new model.service(serviceData);
  //     newService.save(function(errSave, insertedService){
  //       global.ispravimeLogger.log('info', 'Added service "' + newService.name + '" to services repository.');
  //     });
  //   }
  //   else {
  //     srv.name = serviceData.name;
  //     srv.description = serviceData.description;
  //     srv.endpoints = serviceData.endpoints;

  //     srv.save(function(errSave, insertedService){
  //       global.ispravimeLogger.log('info', 'Updated "' + srv.name + '" service\'s information.');
  //     });
  //   }
  // });
}

// Routes.
module.exports = function(router, cache) {
  //OK
  // List requests.
  router.route('/requests')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'reqs';
    const userId = req.query.admin;
    const from = req.query.dateMin;
    const until = req.query.dateMax;
    // const columns = req.query.columns;
    const filterObject = filterHelper.requestsFiltering(req.query);
    const sortObject = filterHelper.requestsSorting(req.query);
    const skip = +req.query.start || 0;
    const limit = +req.query.length || config.defaultPageSize;
    if (userId) {
      requestsBL.getRequestCountByUser(from, until, userId, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_request_count'
          });
        }
  
        const requestCount = success;
        requestsBL.getRequestsByUser(userId, skip, limit, filterObject, sortObject, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_requests_pagination',
              'details': err
            });          
          }
       
          return res.json({
            'success': true,
            'recordsTotal': requestCount,
            'recordsFiltered': success.count,
            'response': success.rows
          });
        });
      });
    } else {
      requestsBL.getRequestsCount(from, until, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_request_count'
          });
        }
  
        const requestCount = success;
        requestsBL.getRequests(skip, limit, filterObject, sortObject, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_requests_pagination',
              'details': err
            });          
          }
  
          return res.json({
            'success': true,
            'recordsTotal': requestCount,
            'recordsFiltered': success.count,
            'response': success.rows
          });
        });
      });
    }
  });

  //OK
  // List mistakes.
  router.route('/mistakes')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'reqs';
    const from = req.query.dateMin;
    const until = req.query.dateMax;
    const perRequest = req.query.unique;
    const toLower = req.query.toLower;
    const filterObject = filterHelper.mistakesFiltering(req.query);
    const sortObject = filterHelper.mistakesSorting(req.query);
    const skip = +req.query.start || 0;
    const limit = +req.query.length || config.defaultPageSize;
    if (toLower == 'true') {
      requestsBL.getUniqueMistakesCount(from, until, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_mistakes_count'
          });
        }

        const mistakesCount = success;
        global.ispravimeLogger.debug(JSON.stringify(mistakesCount));
        requestsBL.getUniqueMistakes(from, until, skip, limit, perRequest, filterObject, sortObject, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_mistakes_find'
            });
          }
  
          return res.json({
            'success': true,
            'recordsTotal': mistakesCount.count,
            'recordsFiltered': success.count,
            'response': success.rows
          });
        }); 
      });
    }
    else {
      requestsBL.getMistakesCount(from, until, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_mistakes_count'
          });
        }
  
        const mistakesCount = success;
        requestsBL.getMistakes(from, until, skip, limit, perRequest, filterObject, sortObject, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_mistakes_find'
            });
          }
  
          return res.json({
            'success': true,
            'recordsTotal': mistakesCount.count,
            'recordsFiltered': success.count,
            'response': success.rows
          });
        }); 
      });
    }
  });

  //OK
  // Request details.
  router.route('/requests/:id')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'reqs';
    requestsBL.getRequestDetails(req.params.id, (err, request) => {
      if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_request_find'
          });
      }

      requestsBL.getMistakesByRequest(request.id, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_request_find'
          });
        }
        
        request.mistakes = success;
        var file = config.textFilesDir + '/' + request.textfile;
        fs.exists(file, function(exists) {
          if (exists) {
            fs.readFile(file, 'utf8', function (err, data) {
              if (!err) {
                request.text = data
                  .replace(/\{/g,"š")
                  .replace(/\}/g,"ć")
                  .replace(/\~/g,"č")
                  .replace(/\|/g,"đ")
                  .replace(/\`/g,"ž")
                  .replace(/\[/g,"Š")
                  .replace(/\]/g,"Ć")
                  .replace(/\^/g,"Č")
                  .replace(/\\/g,"Đ")
                  .replace(/\@/g,"Ž");
                return res.json({
                  'success': true,
                  'response': request
                });
              }
            });
          } else {
            return res.json({
              'success': true,
              'response': request
            });
          }
        });
      });
    });
  });

  //OK
  // Mistake details.
  router.route('/mistakes/:id')
  .get(timestampClear, cache(config.cacheDuration),
  function(req, res) {
    req.apicacheGroup = 'reqs';
    const mistakeId = req.params.id;
    requestsBL.getMistakeDetails(mistakeId, (err, success) => {
      if (err || !success) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_mistake_details'
        });
      }

      const mistake = success;
      requestsBL.getRequestsByMistakeId(mistakeId, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_mistake_details'
          });
        }

        mistake.requests = success;
        return res.json({
          'success': true,
          'response': mistake
        }); 
      }); 
    });
  });

  // Requests count.
  router.route('/count/requests')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'count_requests';
    requestsBL.getRequestsCount(req.query.dateMin, req.query.dateMax, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_request_count'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Mistakes count.
  router.route('/count/mistakes')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'count_mistakes';
    requestsBL.getMistakesSumEver((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_mistakes_count'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Mistakes count.
  router.route('/count/uniquemistakes')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'count_mistakes';
    requestsBL.getUniqueMistakesCountEver((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_mistakes_count'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Mistakes toplist.
  router.route('/top/mistakes')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'top_mistakes';
    const limit = +req.query.limit || config.defaultPageSize;
    const from = req.query.dateMin;
    const until = req.query.dateMax;
    const perRequest = req.query.unique;
    const toLower = req.query.toLower;
    const method = toLower == 'true' ? requestsBL.getTopUniqueMistakes : requestsBL.getTopMistakes;
    method(from, until, limit, perRequest, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_mistakes_top'
        });
      }
      
      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Mistakes ratio.
  router.route('/mistakesRatio')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'mistakes';
    const from = req.query.dateMin;
    const until = req.query.dateMax;
    const perRequest = req.query.unique;
    requestsBL.getMistakesRatio(from, until, perRequest, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_mistakes_top'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Average processing time, word count and mistake count.
  router.route('/avg')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'avg_processing';
    requestsBL.getAvg(req.query.dateMin, req.query.dateMax, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_avg'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  router.route('/lastRequest')
  .get(function(req, res) {
    requestsBL.getLastRequest((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_lastrequest'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });
}
