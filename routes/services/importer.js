'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const apiTokenPermissionsCheck = require(appRoot + '/utils/authHelper.js').apiTokenPermissionsCheck;
const http = require('http');
const async = require('async');
const adminBL = require(appRoot + '/bl/admin.js');
const serviceBL = require(appRoot + '/bl/services.js');
const usersBL = require(appRoot + '/bl/users.js');
const requestsBL = require(appRoot + '/bl/requests.js');

if (global.worker.id == 1) {
  // Insert service metadata.
  var serviceData = {
    'name': 'importer',
    'description': 'Import ',
    'endpoints': [{
      'uri': '/api/importer/v1',
      'method': 'PUT',
      'description': 'Import v1',
      'noAuthRequired': false,
      'allowedByDefault': false,
      'allowedRoles': ['admin', 'importer']
    }]
  };

  serviceBL.deleteServiceByName(serviceData.name, (err, success)=>{
    if (err) {
      throw err;
    }

    serviceBL.createService(serviceData.name, serviceData.description, (err, success) => {
      if (err) {
        throw err;
      }

      for (var i=0; i<serviceData.endpoints.length; i++) {
        var endpoint = serviceData.endpoints[i];
        (function(endpoint, successid) {
          serviceBL.createServiceEndpoints(
            endpoint.method,
            endpoint.description,
            endpoint.noAuthRequired,
            endpoint.allowedByDefault,
            successid,
            endpoint.uri,
            (err, success) => {
              if (err) {
                global.ispravimeLogger.error(err);
                return;
              }

              for (var i=0; i<endpoint.allowedRoles.length; i++) {
                var role = endpoint.allowedRoles[i];
                (function(role, endpointid) {
                  serviceBL.createAllowedRoles(role, endpointid, (err, success) => {
                    if (err) {
                      global.ispravimeLogger.error(err);
                    }
                  });
                }) (role, success.id);
              }
          });
        }) (endpoint, success.id);
      }

      global.ispravimeLogger.info(`Updated ${serviceData.name} service\'s information.`);
    });
  });
}

function insertRequest(req, res, userID) {
  const requestTime = req.body.requestTime;
  const processingTime = req.body.timeProcessed;
  const wordCount = req.body.wordCounter;
  const ip = req.body.ip;
  const context = req.body.context;
  const mistakes = req.body.report.errors;
  const numberOfMistakes = req.body.report.numberOfErrors;
  const textFile = req.body.textFile;
  requestsBL.createRequest(requestTime, processingTime, wordCount, ip, userID, context, mistakes, textFile, (err, success) => {
    if (err) {
      global.ispravimeLogger.error(err);
      res.status(503);
      return res.json({
        'success': false,
        'response': 'err_request_add',
        'details': err
      });
    }

    const request = success;
    for (var i=0; i<mistakes.length; i++) {
      (function(mistake, requestId) {
        requestsBL.createMistake(mistake.suspicious, mistake.suggestions, mistake.severity, mistake.occurrences, requestId, (err, success) => {
          if (err) {
            global.ispravimeLogger.error(err);
          }
        }); 
      }) (mistakes[i], success.id);
    }

    return res.json({
      'success': true,
      'response': 'request_added',
      'newRequestID': request.id
    });
  });
}

// Routes.
module.exports = function(router, cache) {
  // Importer v1
  router.route('/importer/v1')
  .put(apiTokenPermissionsCheck, function(req, res) {
    const userID = req.body.userID;
    const lastAccess = req.body.requestTime;
    const lastIP = req.body.ip;
    usersBL.upsertUser(userID, null, lastAccess, lastIP, (err, success) => {
      if (err) {
        global.ispravimeLogger.error(err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_add',
          'details': err
        });
      }
      
      return insertRequest(req, res, userID);
    });
  });

  function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function randomDate() {
    const year = randomRange(2014, 2016);
    const month = randomRange(1, 12);
    const day = randomRange(1, 25);
    const hour = randomRange(0, 23);
    const minute = randomRange(0, 59);
    const second = randomRange(0, 59);
    return year + '-' + month + '-' + day;
  }

  // Randomize dates (for testing)
  router.route('/importer/randomizeDates/:pageSize')
  .post(function (req, res) {
    global.ispravimeLogger.debug("/importer/randomizeDates/:pageSize");
    let pageSize = req.params.pageSize;
    requestsBL.getRequestsCount(null, null, (err, success) => {
      var functions = [];
      for (let currentPage = 1; currentPage <= Math.ceil(success/pageSize); currentPage++) {
        functions.push(function(acallback) {
          let options = {
            host: config.listenAddress,
            port: config.httpPort,
            method: 'POST',
            path: '/api/importer/randomizeDates/' + currentPage + '/' + pageSize
          };
          http.request(options, function(ress) {
            var str = '';
            ress.on('data', function (chunk) {
              str += chunk;
            });

            ress.on('end', function () {
              //global.ispravimeLogger.debug('updated page ' + currentPage + '/' + Math.ceil(success/pageSize));
              acallback(null, currentPage);
            });
          }).end();
        });
      }
      async.series(functions, function(err, results) {
        return res.json({
          'success': true,
          'response': 'randomized_dates',
          'details': results
        });
      });
    });
  });

  // Randomize dates (for testing)
  router.route('/importer/randomizeDates/:pageNumber/:pageSize')
  .post(function (req, res) {
    global.ispravimeLogger.debug("/importer/randomizeDates/:pageNumber/:pageSize");
    const pageSize = +req.params.pageSize;
    const pageNumber = +req.params.pageNumber;
    const numUpdated = 0;
    requestsBL.getRequests(-1, (pageNumber - 1) * pageSize, pageSize, (err, success) => {
      if (success.length == 0) {
        return res.json({
          'success': false,
          'response': 'randomize_dates_out_of_range'
        });
      }

      for (let i = 0; i < success.length; i++) {
        const requestTime = new Date(randomDate());
        const r = success[i];
        (function (id, requestTime, processingTime, ipAddress, wordCount, mistakeCount, uniqueMistakeCount, userId, textFile, context, ignore) {
          requestsBL.updateRequest(id, requestTime, processingTime, ipAddress, wordCount, mistakeCount, uniqueMistakeCount, userId, textFile, context, ignore, (err, success) => {
            if (err) {
              global.ispravimeLogger.error(err);
            }
          });
        }) (r.id, requestTime, r.processingtime, r.ipaddress, r.wordcount, r.mistakecount, r.uniquemistakecount, r.userid, r.textfile, r.context, r.ignore);
      }
      
      return res.json({
        'success': true,
        'response': 'randomized_dates',
        'details': {
          'page': pageNumber,
          'pageSize': pageSize
        }
      });
    });
  });
};
