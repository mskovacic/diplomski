'use strict';

var appRoot = require('app-root-path');
var timestampClear = new require(appRoot + '/utils/timestampClear.js').timestampClear;
var fs = require('fs');
const networksBL = require(appRoot + '/bl/networks.js');
const servicesBL = require(appRoot + '/bl/services.js');

if (global.worker.id == 1) {
  // Insert service metadata.
  var serviceData = {
    'name': 'networks',
    'description': 'Networks',
    'endpoints': [{
      'uri': '/api/networks',
      'method': 'GET',
      'description': 'List networks',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/networks',
      'method': 'PUT',
      'description': 'Adds new network',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/networks/:id',
      'method': 'POST',
      'description': 'Updates network',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }]
  };
  servicesBL.deleteServiceByName(serviceData.name, (err, success)=>{
    if (err) {
      global.ispravimeLogger.error(err);
      throw err;
    }

    servicesBL.createService(serviceData.name, serviceData.description, (err, success) => {
      if (err) {
        global.ispravimeLogger.error(err);
        throw err;
      }

      for (var i=0; i<serviceData.endpoints.length; i++) {
        var endpoint = serviceData.endpoints[i];
        (function(endpoint, successid) {
          servicesBL.createServiceEndpoints(
            endpoint.method,
            endpoint.description,
            endpoint.noAuthRequired,
            endpoint.allowedByDefault,
            successid,
            endpoint.uri,
            (err, success) => {
              if (err) {
                global.ispravimeLogger.error(err);
                return;
              }

              for (var i=0; i<endpoint.allowedRoles.length; i++) {
                var role = endpoint.allowedRoles[i];
                (function(role, endpointid){
                  servicesBL.createAllowedRoles(role, endpointid, (err, success) => {
                    if (err) {
                      global.ispravimeLogger.error(err);
                      return;
                    }
                  });
                }) (role, success.id);
              }
          });
        }) (endpoint, success.id);
      }

      global.ispravimeLogger.info(`Updated ${serviceData.name} service\'s information.`);
    });
  });
  
  // Update networks from file.
  var networksFile = appRoot + '/networks.txt';
  fs.readFile(networksFile, 'utf8', function (err, data) {
    if (!err) {
      let lines = data.split('\n');
      for (let i = 0; i < lines.length; i++) {
        let network = lines[i].trim();
        if (network) {
          let nsplit = network.split(',');
          if (nsplit.length === 3) {
            let mask = nsplit[0].trim();
            let name = nsplit[1].trim();
            let allowed = false;
            if (nsplit[2] === '1') allowed = true;
            networksBL.deleteNetworkByNameAndMask(name, mask, (err, success)=>{
              if (err) {
                global.ispravimeLogger.log('error', err);
              }

              networksBL.createNetwork(name, mask, allowed, (err, success) => {
                if (err) {
                  global.ispravimeLogger.log('error', err);
                }

                global.ispravimeLogger.log('info', 'Updated networks.');
              });
            });
          }
        }
      }
    }
  });
}

// Routes.
module.exports = function(router, cache) {
  //OK
  // List networks.
  router.route('/networks')
  .get(function(req, res) {
    networksBL.getNetworks((err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_networks_list'
          });
        }

        return res.json({
          'success': true,
          'response': success
        });
      });
  });

  //OK
  // Create new network.
  router.route('/networks')
  .put(function(req, res) {
    var requiredFields = [];
    var reqIncomplete = false;
    if (!req.body.name) {
      reqIncomplete = true;
      requiredFields.push('required_name');
    }
    if (!req.body.mask) {
      reqIncomplete = true;
      requiredFields.push('required_mask');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    } 

    networksBL.getNetworksCountByNameAndMask(req.body.name, req.body.mask, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_network_find'
        });
      }

      if (success.count > 0) {
        res.status(409);
        return res.json({
          'success': false,
          'response': 'network_exists'
        });
      }

      networksBL.createNetwork(req.body.name, req.body.mask, req.body.allowed, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_network_add'
          });
        }

        return res.json({
          'success': true,
          'response': 'network_added',
          'newNetworkID': success.id
        });
      });
    });
  });

  //OK
  // Update existing network.
  router.route('/networks/:id')
  .post(function(req, res) {
    var requiredFields = [];
    var reqIncomplete = false;
    if (!req.body.name) {
      reqIncomplete = true;
      requiredFields.push('required_name');
    }
    if (!req.body.mask) {
      reqIncomplete = true;
      requiredFields.push('required_mask');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }

    networksBL.updateNetwork(req.params.id, req.body.name, req.body.mask, req.body.allowed, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_network_update_find'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Delete existing network.
  router.route('/networks/:id')
  .delete(function(req, res) {
    networksBL.deleteNetwork(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_network_delete'
        });
      }
      return res.json({
        'success': true,
        'response': 'network_deleted',
        'deletedId': success.id
      });
    });
  });
};
