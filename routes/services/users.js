'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const timestampClear = new require(appRoot + '/utils/timestampClear.js').timestampClear;
const usersBL = require(appRoot + '/bl/users.js');
const servicesBL = require(appRoot + '/bl/services.js');
const filterHelper = require(appRoot + '/utils/filterHelper.js');

if (global.worker.id == 1) {
  // Insert service metadata.
  var serviceData = {
    'name': 'users',
    'description': 'Users',
    'endpoints': [{
      'uri': '/api/users',
      'method': 'GET',
      'description': 'List users',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id',
      'method': 'GET',
      'description': 'User details',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id',
      'method': 'POST',
      'description': 'Set/change user\'s name',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id/mistakeTypes',
      'method': 'GET',
      'description': 'User\'s mistake types',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id/requestsTimes',
      'method': 'GET',
      'description': 'User\'s request times by month',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id/requestsTimesBy/:range',
      'method': 'GET',
      'description': 'User\'s request times by range (year, month or year)',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/users/:id/requestsStats',
      'method': 'GET',
      'description': 'User\'s requests stats',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }, {
      'uri': '/api/count/users',
      'method': 'GET',
      'description': 'User count',
      'noAuthRequired': true,
      'allowedByDefault': true,
      'allowedRoles': []
    }]
  };
  servicesBL.deleteServiceByName(serviceData.name, (err, success)=>{
    if (err) {
      throw err;
    }

    servicesBL.createService(serviceData.name, serviceData.description, (err, success) => {
      if (err) {
        throw err;
      }

      for (var i=0; i<serviceData.endpoints.length; i++) {
        var endpoint = serviceData.endpoints[i];
        (function(endpoint, successid) {
          servicesBL.createServiceEndpoints(
            endpoint.method,
            endpoint.description,
            endpoint.noAuthRequired,
            endpoint.allowedByDefault,
            successid,
            endpoint.uri,
            (err, success) => {
              if (err) {
                console.error(err);
                return;
              }

              for (var i=0; i<endpoint.allowedRoles.length; i++) {
                var role = endpoint.allowedRoles[i];
                (function(role, endpointid){
                  servicesBL.createAllowedRoles(role, endpointid, (err, success) => {
                    if (err) {
                      console.error(err);
                      return;
                    }
                  });
                }) (role, success.id);
              }
          });
        }) (endpoint, success.id);
      }

      global.ispravimeLogger.log('info', 'Updated "' + serviceData.name + '" service\'s information.');
    });
  });
  // model.service.findOne({'name': serviceData.name }, function(err, srv) {
  //   if (err) throw err;
  //   if (srv === null) {
  //     var newService = new model.service(serviceData);
  //     newService.save(function(errSave, insertedService){
  //       global.ispravimeLogger.log('info', 'Added service "' + newService.name + '" to services repository.');
  //     });
  //   }
  //   else {
  //     srv.name = serviceData.name;
  //     srv.description = serviceData.description;
  //     srv.endpoints = serviceData.endpoints;

  //     srv.save(function(errSave, insertedService){
  //       global.ispravimeLogger.log('info', 'Updated "' + srv.name + '" service\'s information.');
  //     });
  //   }
  // });
}

// Routes.
module.exports = function(router, cache, apicache) {
  //OK
  // Users list.
  router.route('/users')
  .get(timestampClear, cache(config.cacheDuration),
  function(req, res) {
    req.apicacheGroup = 'usrs';
    const skip = +req.query.start || 0;
    const limit = +req.query.length || config.defaultPageSize;
    const filterObject = filterHelper.usersFiltering(req.query);
    const sortObject = filterHelper.usersSorting(req.query);
    usersBL.getUserCount((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_users_count'
        });
      }

      var userCount = success;
      usersBL.getUsers(skip, limit, filterObject, sortObject, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_users_pagination'
          });
        }
        
        const filteredUsers = success;
        usersBL.getUserRequestStatistics(filteredUsers.rows.map(x => x.id), (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_users_pagination'
            });
          }

          return res.json({
            'success': true,
            'recordsTotal': userCount,
            'recordsFiltered': filteredUsers.count,
            'count': {
              'numRequests': success.numberofrequests,
              'numMistakesTotal': success.totalmistakes,
              'numUniqueMistakes': success.totaluniquemistakes,
              'avgRequestTime': success.avgrequesttime,
              'totalRequestTime': success.totalrequesttime
            },
            'response': filteredUsers.rows
          });
        });  
      });
    });
  });

  // router.route('/usersstats')
  // .get((err, res) => { 
  //   const userIds = [
  //     '79466034-B6FD-11E6-925F-B7EFD4852795',
  //     '89CE12C8-B615-11E6-BCAD-EF85463E1228',
  //     '460E81CE-B572-11E6-BD63-B1712F2A24C0',
  //     '8910B40C-C705-11E6-88ED-92DC373BB070',
  //     '43E2050C-C84A-11E6-93A4-E9A2E389C7E8',
  //     'E32D2628-D3EE-11E6-8BA7-EBE38F072025',
  //     'C7E7BBCA-B55E-11E6-B36F-BDC4B95FBB1E',
  //     'C9BA45A6-D52C-11E6-B11D-E918DFE1E74C',
  //     '800E7DB8-D8E3-11E6-8158-DF8596985EE3',
  //     'FE9B3C86-BA14-11E6-87CD-D40BC7BE66EA',
  //     'D8120C54-BBF2-11E6-8E16-DED262737AAE',
  //     '74CA4F02-DB42-11E6-A655-91FFB8097BA2',
  //     '4189151A-DB42-11E6-B0AE-E328B3DFCEFF',
  //     '43BB100A-D29F-11E6-B0C7-841DB4FB2115',
  //     '55877068-DAAA-11E6-9390-8995A318C76C',
  //     'BC8FB876-B582-11E6-AB61-D5C6093A93D2'];
  //   usersBL.getUserRequestStatistics(userIds, (err, success) => {
  //     if (err) {
  //       global.ispravimeLogger.log('error', err);
  //       res.status(503);
  //       return res.json({
  //         'success': false,
  //         'response': err
  //       });
  //     }

  //     return res.json({
  //       'success': true,
  //       'response': success
  //     });
  //   });
  // });

  //OK
  // User details.
  router.route('/users/:id')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'usrs';
    var userId = req.params.id;
    usersBL.getUserDetails(userId, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_find'
        });
      }

      var response = success;
      usersBL.getUserIpHistory(userId, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_user_find'
          });
        }

        response.historyip = success;
        return res.json({
          'success': true,
          'response': response
        });
      });
    });
  });

  //OK
  // Set user's nickname.
  router.route('/users/:id')
  .post(function(req, res) {
    usersBL.setUserDetails(req.params.id, req.body.name, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_name_update'
        });
      }

      // Clear cache.
      apicache.clear('usrs');
      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // User's mistake types.
  router.route('/users/:id/mistakeTypes')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'usrs';
    usersBL.getUserMistakeTypes(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_mistakes_find'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  var requestTimes = function(req, res) {
    req.apicacheGroup = 'usrs';
    const defaultRange = 'month';
    const userId = req.params.id;
    var range = req.params.range || defaultRange;
    if (['year', 'month', 'day'].indexOf(range) == -1) {
      range = defaultRange;
    }
    
    usersBL.getUserRequestTimes(userId, range, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_requests_times_find'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  };

  //OK
  // User's requests times.
  router.route('/users/:id/requestsTimes')
  .get(timestampClear, cache(config.cacheDuration), requestTimes);
  router.route('/users/:id/requestsTimesBy/:range')
  .get(timestampClear, cache(config.cacheDuration), requestTimes);

  //OK
  // User's requests stats.
  router.route('/users/:id/requestsStats')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'usrs';
    usersBL.getUserRequestStats(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_requests_stats_find'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  // Users count.
  router.route('/count/users')
  .get(timestampClear, cache(config.cacheDuration), function(req, res) {
    req.apicacheGroup = 'usrs';
    usersBL.getUserCount((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_user_count'
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });
};
