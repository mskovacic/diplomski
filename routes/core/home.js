'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const jwt = require('jsonwebtoken');
const servicesBL = require(appRoot + '/bl/services.js');
const adminBL = require(appRoot + '/bl/admin.js');
const requestsBL = require(appRoot + '/bl/requests.js');

/*
API homepage route.
*/

module.exports = function(router, cache) {
  //OK
  router.get('/', function(req, res) {
    var adminToken = req.headers['Authorization'] || req.headers['authorization'];
    if (!adminToken) {
      // Token not provided - get available services.
      servicesBL.getFullServices((err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': err
          });
        }

        return res.json({
          'success': true,
          'version': config.version,
          'services': success
        });
      });
    }
    else {
      jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(errToken, decoded) {
        if (errToken || !decoded) {
          // Token not valid error.
          res.status(401);
          return res.json({
            'success': false,
            'response': errToken
          });
        }

        // Get admin ID.
        var adminId = decoded.adminId;
        adminBL.getAdminDetails(adminId, (err, success) => {
          if (err || success == undefined) {
            // Admin with ID does not exist error.
            res.status(401);
            res.set('WWW-Authenticate', 'admin_not_exist');
            return res.json({
              'success': false,
              'response': err
            });
          }

          const admin = success;
          adminBL.getServiceAccessForAdmin(adminId, (err, success) => {
            if (err) {
              global.ispravimeLogger.log('error', err);
              res.status(503);
              return res.json({
                'success': false,
                'response': err
              });
            }

            const adminServices = success;
            servicesBL.getFullServices((err, success) => {
              if (err) {
                global.ispravimeLogger.log('error', err);
                res.status(503);
                return res.json({
                  'success': false,
                  'response': err
                });
              }

              // Cross check available services with services admin has access to.
              const services = success;
              for (let i = 0; i < services.length; i++) {
                const endpoints = services[i].endpoints;
                for (var j = 0; j < endpoints.length; j++) {
                  var endpoint = endpoints[j];
                  var hasAccess = false;
                  for (var k in adminServices) {
                    var adminEndpoint = adminServices[k];
                    if (admin.roles.indexOf('admin') > -1 || endpoint.noAuthRequired || (adminEndpoint.method == endpoint.method && adminEndpoint.uri == endpoint.uri)) {
                      // Admin always has access to all services.
                      hasAccess = true;
                      break;
                    }
                  }
                  endpoint.adminHasAccess = hasAccess;
                }
              }
              
              return res.json({
                'success': true,
                'version': config.version,
                'services': services
              });
            });
          });
        });
      });
    }
  });
};
