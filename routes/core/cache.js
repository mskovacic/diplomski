'use strict';

// Routes.
module.exports = function(router, cache, apicache) {
  router.route('/cache')
  .get(function(req, res) {
    return res.json(apicache.getIndex());
  });
  router.route('/cache/clear/:key')
  .get(function(req, res) {
    if (req.params.key == 'all') {
      return res.json(apicache.clear());
    }
    return res.json(apicache.clear(req.params.key));
  });
};
