'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const jwt = require('jsonwebtoken');
const ms = require('ms');
const roleCheck = new require(appRoot + '/utils/authHelper.js').roleCheck;
const permissionsEditor = require(appRoot + '/utils/permissionsEditor.js');
const stringGenerator = require(appRoot + '/utils/stringGenerator.js');
const adminBL = require(appRoot + '/bl/admin.js');
const serviceBL = require(appRoot + '/bl/services.js');
const filterHelper = require(appRoot + '/utils/filterHelper.js');

/*
Admin management route.
*/

module.exports = function(router, cache) {
  //TODO
  router.route('/admins')
  .get(roleCheck('admin'), function(req, res) {
    const skip = +req.query.start || 0;
    const length = +req.query.length || config.defaultPageSize;
    const filterObject = filterHelper.adminsFiltering(req.query);
    const sortObject = filterHelper.adminsSorting(req.query);
    adminBL.getAdminCount((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_count'
        });
      }      

      const adminCount = success;
      adminBL.getAdmins(skip, length, filterObject, sortObject, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_admin_find',
            'details': err
          });
        }
  
        return res.json({
          'success': true,
          'recordsTotal': adminCount,
          'recordsFiltered': success.count,
          'response': success.rows
        });
      });
    });
  })
  //OK
  .put(function(req, res) {
    // Create new admin.
 
    var requiredFields = [];
    var reqIncomplete = false;
    if (!req.body.email) {
      reqIncomplete = true;
      requiredFields.push('required_email');
    }
    if (!req.body.password) {
      reqIncomplete = true;
      requiredFields.push('required_password');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        },
        'details': null
      });
    }

    const email = req.body.email;
    adminBL.getAllAdmins((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_find',
          'details': err
        });
      }

      const admins = success;
      if (admins.map(x => x.email).indexOf(email) > -1) {
        // Admin with same email already exist.
        res.status(409);
        return res.json({
          'success': false,
          'response': 'email_exists',
          'details': null
        });
      }

      const salt = require('crypto').randomBytes(128).toString('base64');
      const password = require('crypto').createHash('sha256').update(salt + req.body.password).digest('base64');
      const roles = [];
      if (admins.length == 0) {
        roles.push('admin');
      }

      serviceBL.getServiceEndpoints((err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_fetch_service_data',
            'details': err
          });
        }
  
        const endpoints = success;
        const serviceaccess = [];
        for (var e = 0; e < endpoints.length; e++) {
          var endpoint = endpoints[e];
          if (endpoint.allowedByDefault === true || endpoint.noAuthRequired === true || roles.indexOf('admin') > -1) {
            var srv = {};
            srv.uri = endpoint.uri;
            srv.method = endpoint.method;
            serviceaccess.push(srv);
          }
        }
       
        adminBL.createAdmin(email, salt, password, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_admin_add',
              'details': err
            });
          }
  
          for (var i=0; i<roles.length; i++) {
            (function(role, successid) {
              adminBL.createRole(role, successid, (err, success) => {
                if (err) {
                  global.ispravimeLogger.log('error', err);
                  // res.status(503);
                  // return res.json({
                  //   'success': false,
                  //   'response': 'err_role_add'
                  //});
                }
              });
            }) (roles[i], success.id);  
          }
  
          for (var i=0; i<serviceaccess.length; i++) {
            var sa = serviceaccess[i];
            (function(uri, method, tokenid, adminid) {
              adminBL.createServiceAccess(uri, method, tokenid, adminid, (err, success) => {
                if (err) {
                  global.ispravimeLogger.log('error', err);
                }
              });
            }) (sa.uri, sa.method, null, success.id);
          }

          return res.json({
            'success': true,
            'response': 'admin_added',
            'newAdminID': success.id
          });          
        });
      });
    });
  });

  //OK
  router.route('/admins/emails')
  .get(roleCheck('admin'), function(req, res) {
    adminBL.getAdminEmails((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_list',
          'details': err
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  router.route('/admins/details/:id')
  .get(roleCheck('admin'), function(req, res) {
    global.ispravimeLogger.debug('/admins/details/:id');
    adminBL.getAdminDetails(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_count',
          'details': err
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  });

  //OK
  router.route('/admins/roles/:id')
  .get(function(req, res) {
    adminBL.getAdminRoles(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
      }

      if (err || !success) {
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_find',
          'details': err
        });
      }

      return res.json({
        'success': true,
        'response': success
      });
    });
  })
  //OK
  .put(roleCheck('admin'), function(req, res) {
    // Check if required fields are set.
    var requiredFields = [];
    var reqIncomplete = false;
    if (!req.params.id) {
      reqIncomplete = true;
      requiredFields.push('required_admin_id');
    }
    if (!req.body.roles) {
      reqIncomplete = true;
      requiredFields.push('required_roles');
    }
    else if (!Array.isArray(req.body.roles)) {
      reqIncomplete = true;
      requiredFields.push('required_roles_array');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        },
        'details': null
      });
    } 

    if(typeof req.body.roles === 'string') {
      req.body.roles = [req.body.roles];
    }

    for(var i=0; i<req.body.roles.length; i++) {
      var result = (function(role, adminId) {
        adminBL.setAdminRole(role, adminId, (err, success) => {
          if (err){
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': err,
              'details': err
            });
          }
          
        });
      }) (req.body.roles[i], req.params.id);
      // if (result != undefined) {
      //   return 
      // }
    }

    adminBL.getAdminRoles(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_admin_find',
          'details': err
        });
      }

      return res.json({
        'success': true,
        'response': {
          'addedRoles': req.body.roles,
          'updatedRoles': success
        }
      });
    });
  })
  //OK
  .delete(roleCheck('admin'), function(req, res) {
    // Check if required fields are set.
    var requiredFields = [];
    var reqIncomplete = false;
    if (!req.params.id) {
      reqIncomplete = true;
      requiredFields.push('required_admin_id');
    }
    if (!req.body.roles) {
      reqIncomplete = true;
      requiredFields.push('required_roles');
    }
    else if (!Array.isArray(req.body.roles)) {
      reqIncomplete = true;
      requiredFields.push('required_roles_array');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }

    if(typeof req.body.roles === 'string') {
      req.body.roles = [req.body.roles];
    }

    var admId = req.params.id;
    var roles = req.body.roles;
    adminBL.deleteAdminRoles(roles, admId, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': err
        });
      }

      adminBL.getAdminRoles(req.params.id, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_admin_find'
          });
        }
  
        return res.json({
          'success': true,
          'response': {
            'removedRoles': roles,
            'updatedRoles': success
          }
        });
      });
    });
  });

  router.route('/admins/auth')
  //OK
  .post(function(req, res) {
    // Admin token generation (== login).

    // Find the admin.
    adminBL.getAdminByEmail(req.body.email, (err, admin) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(500);
        return res.json({
          'success': false,
          'response': 'err_admin_find'
        });
      }

      if (!admin) {
        // Admin doesn't exist.
        res.status(401);
        res.set('WWW-Authenticate', 'admin_not_exist');
        return res.json({
          'success': false,
          'response': 'admin_not_exist'
        });
      }

      var hashedPass = require('crypto').createHash('sha256').update(admin.salt + req.body.password).digest('base64');
      if (hashedPass != admin.password) {
        // Wrong password.
        res.status(401);
        res.set('WWW-Authenticate', 'password_wrong');
        return res.json({
          'success': false,
          'response': 'password_wrong'
        });
      }

      adminBL.getAdminRoles(admin.id, (err, success) => {
        // Generate token if everything is OK.
        var d = new Date();
        var tokenData = {
          'adminId': admin.id,
          'tokenType': 'adminToken',
          'roles': success,
          'endpoints': admin.serviceAccess,
          'iat': Math.floor(d / 1000),
          'iatUTC': d.toUTCString()
        };
        var token = jwt.sign(tokenData, config.secretPrivate, {
          algorithm: 'RS256',
          expiresIn: config.adminTokenExpire
        });
        // Return token.
        return res.json({
          'success': true,
          'response': 'token_received',
          'token': token,
          'adminId': admin.id
        });
      });
    });
  });

  router.route('/admins/auth/:token')
  //OK
  .get(function(req, res) {
    // Get token information.
    global.ispravimeLogger.debug('/admins/auth/:token');
    var token = req.params.token;
    jwt.verify(token, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      if (err) {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'token_invalid_or_expired');
        return res.json({
          'success': false,
          'response': 'token_invalid_or_expired'
        });
      }
      else if (decoded.tokenType == 'adminToken') {
        return res.json({
          'success': true,
          'response': decoded
        });
      }
      else {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'token_expect_admin');
        return res.json({
          'success': false,
          'response': 'token_expect_admin'
        });
      }
    });
  });

  //OK
  router.route('/admins/tokens')
  .get(function(req, res) {
    // Get tokens for all admins.

    var adminToken = req.headers['Authorization'] || req.headers['authorization'];
    jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      if (err) {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'Token not valid.');
        return res.json({
          'success': false,
          'response': 'token_invalid'
        });
      }
      else if (decoded.tokenType == 'adminToken') {
        if (decoded.roles.indexOf('admin') > -1) {
          adminBL.getAdminTokens((err, success) => {
            if (err) {
              global.ispravimeLogger.log('error', err);
              res.status(500);
              return res.json({
                'success': false,
                'response': 'err_admin_find'
              });
            }

            if (success) {
              return res.json(success);
            }

            res.status(400);
            return res.json({
              'success': false,
              'response': 'admin_not_exist'              
            });
          });
        } else {
          res.status(403);
          return res.json({
            'success': false,
            'response': 'access_denied'
          });
        }
      } else {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'token_expect_admin');
        return res.json({
          'success': false,
          'response': 'token_expect_admin'
        });
      }
    });
  });

  //OK
  router.route('/admins/:id/tokens')
  .get(function(req, res) {
    // Get admin's tokens.

    var adminId = req.params.id;
    var adminToken = req.headers['Authorization'] || req.headers['authorization'];
    jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      if (err) {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'Token not valid.');
        return res.json({
          'success': false,
          'response': 'token_invalid',
          'details': err
        });
      }

      if (decoded.tokenType == 'adminToken') {
        if (decoded.adminId == adminId || decoded.roles.indexOf('admin') > -1) {
          adminBL.getTokensForAdmin(adminId, (err, success) => {
            if (err) {
              global.ispravimeLogger.log('error', err);
              res.status(500);
              return res.json({
                'success': false,
                'response': 'err_admin_find'
              });
            }

            // if (!success) {
            //   res.status(400);
            //   return res.json({
            //     'success': false,
            //     'response': 'admin_not_exist'
            //   });              
            // }

            return res.json({
              'success': true,
              'response': success
            });
          });
        } else {
          res.status(403);
          return res.json({
            'success': false,
            'response': 'access_denied'
          });
        }
      } else {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'token_expect_admin');
        return res.json({
          'success': false,
          'response': 'token_expect_admin'
        });
      }
    });
  });

  //OK
  router.route('/admins/reset-password')
  .put(function(req, res) {
    // Request password reset.

    var reqIncomplete = false;
    var requiredFields = [];
    if (!req.body.adminId && !req.body.email) {
      reqIncomplete = true;
      requiredFields.push('required_adminid_or_email');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }

    var adminId = req.body.adminId;
    var email = req.body.email;
    adminBL.getAdminByIdOrEmail(adminId, email, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(500);
        return res.json({
          'success': false,
          'response': 'err_admin_find'
        });
      }

      if (!success) {
        res.status(400);
        return res.json({
          'success': false,
          'response': 'admin_not_exist'
        });
      }

      adminId = success.id;
      email = success.email;
      const passwordResetToken = stringGenerator(16);
      const passwordResetIssuedAt = new Date().toISOString();
      var prdata = {};
      prdata.token = passwordResetToken;
      prdata.issuedAt = passwordResetIssuedAt;
      prdata.adminId = adminId;
      prdata.adminEmail = email;
      adminBL.createAdminPasswordReset(adminId, passwordResetToken, passwordResetIssuedAt, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_admin_pwd_reset'
          });
        }

        const nodemailer = require('nodemailer');
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport(config.email.connection);
        const link = 'https://' + config.hostname + ':' + config.httpsPort + '/reset-password/' + email + '/' + adminId + '/' + passwordResetToken;

        // setup e-mail data with unicode symbols
        var mailOptions = {
          from: config.email.from,
          to: email,
          subject: 'Password Reset',
          text: 'Follow this link: ' + link,
          html: '<a href="' + link + '">Follow this link</a>'
          // TODO: PRIJEVODI MAILOVA
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(errMail, info){
          if(errMail){
            global.ispravimeLogger.log('error', errMail);
          }
        });

        var rdata = {
          'success': true,
          'response': 'check_mail_reset_pwd'
        };
        const adminToken = req.headers['Authorization'] || req.headers['authorization'];
        jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
          if (!err && decoded && decoded.tokenType == 'adminToken') {
            if (adminId == decoded.adminId || decoded.roles.indexOf('admin') > -1) {
              // If the same admin is requesting password reset, show reset info.
              // If there is an admin admin token alongside the password reset token, then return reset info in request.
              rdata.passwordReset = prdata;
            }
          }
          return res.json(rdata);
        });
      });
    });
  });

  //OK
  router.route('/admins/reset-password/:token')
  .post(function(req, res) {
    // Reset the password.

    var reqIncomplete = false;
    var requiredFields = [];
    if (!req.body.password) {
      reqIncomplete = true;
      requiredFields.push('required_password');
    }
    if (!req.body.adminId && !req.body.email) {
      reqIncomplete = true;
      requiredFields.push('required_adminid_or_email');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }

    const adminId = req.body.adminId;
    const email = req.body.email;
    const resetToken = req.params.token;
    adminBL.getAdminByIdOrEmail(adminId, email, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(500);
        return res.json({
          'success': false,
          'response': 'err_admin_find'
        });
      }

      if (success == undefined) {
        res.status(400);
        return res.json({
          'success': false,
          'response': 'admin_not_exist'
        });       
      }

      const admin = success;
      adminBL.getAdminPasswordReset(adminId, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(500);
          return res.json({
            'success': false,
            'response': 'err_admin_find'
          });
        }

        if (success.length == 0) {
          res.status(403);
          return res.json({
            'success': false,
            'response': 'pwd_reset_not_requested'
          });
        }

        const passwordReset = success[0];
        const currentTime = new Date().toISOString();
        const issuedTime = passwordReset.issuedat;
        const passwordResetToken = passwordReset.token;
        if (!passwordResetToken || !issuedTime) {
          // Reset not requested.
          res.status(403);
          return res.json({
            'success': false,
            'response': 'pwd_reset_not_requested'
          });
        }

        if (issuedTime + ms(config.resetTokenExpire) >= currentTime) {
          if (resetToken == passwordResetToken) {
            const salt = require('crypto').randomBytes(128).toString('base64');
            const password = require('crypto').createHash('sha256').update(salt + req.body.password).digest('base64');
            adminBL.deleteAdminPasswordReset(adminId, (err, success) => {
              if (err) {
                global.ispravimeLogger.log('error', err);
                res.status(503);
                return res.json({
                  'success': false,
                  'response': 'err_admin_update'
                });
              }

              adminBL.updateAdmin(adminId, admin.email, admin.dateregistered, salt, password, (err, success) => {
                if (err) {
                  global.ispravimeLogger.log('error', err);
                  res.status(503);
                  return res.json({
                    'success': false,
                    'response': 'err_admin_update'
                  });
                }

                return res.json({
                  'success': true,
                  'response': 'pwd_reset'
                });   
              });     
            });
          } else {
            // Tokens do not match.
            res.status(403);
            return res.json({
              'success': false,
              'response': 'pwd_reset_invalid_token'
            });
          }
        } else {
          // Time to reset expired.
          res.status(403);
          return res.json({
            'success': false,
            'response': 'pwd_reset_expired'
          }); 
        } 
      });
    });
  });

  //OK
  router.route('/admins/:id/endpointAccess')
  .put(roleCheck('admin'), function(req, res) {
    var reqIncomplete = false;
    var requiredFields = [];
    if (!req.body.serviceName) {
      reqIncomplete = true;
      requiredFields.push('required_serviceName');
    }
    if (!req.body.uri) {
      reqIncomplete = true;
      requiredFields.push('required_uri');
    }
    if (!req.body.method) {
      reqIncomplete = true;
      requiredFields.push('required_method');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }
    else {
      permissionsEditor.addEndpointPermission(req.params.id, req.body.serviceName, req.body.uri, req.body.method, function(per) {
        if (per === 'noService') {
          // Not all required fields are set.
          res.status(400);
          return res.json({
            'success': false,
            'response': 'no_service'
          });
        }
        else if (per === 'errUpdate') {
          global.ispravimeLogger.log('error', per);
          res.status(500);
          return res.json({
            'success': false,
            'response': 'err_service_access_granting'
          });
        }
        else {
          return res.json({
            'success': true,
            'response': per
          });
        }
      });
    }
  })

  //OK
  .delete(roleCheck('admin'), function(req, res) {
    var reqIncomplete = false;
    var requiredFields = [];
    if (!req.body.uri) {
      reqIncomplete = true;
      requiredFields.push('required_uri');
    }
    if (!req.body.method) {
      reqIncomplete = true;
      requiredFields.push('required_method');
    }

    if (reqIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredFields
        }
      });
    }
    else {
      permissionsEditor.removeEndpointPermission(req.params.id, req.body.uri, req.body.method, function(per) {
        if (per === 'errUpdate') {
          global.ispravimeLogger.log('error', per);
          res.status(500);
          return res.json({
            'success': false,
            'response': 'err_service_access_removal'
          });
        }
        else {
          return res.json({
            'success': true,
            'response': per
          });
        }
      });
    }
  });
};
