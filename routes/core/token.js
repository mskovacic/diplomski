'use strict';

const appRoot = require('app-root-path');
const config = require(appRoot + '/config.js');
const jwt = require('jsonwebtoken');
const sameAdminCheckRevocation = require(appRoot + '/utils/authHelper.js').sameAdminCheckRevocation;
const adminBL = require(appRoot + '/bl/admin.js');
const tokensBL = require(appRoot + '/bl/token.js');
/*
API access token info and generation route.
*/

module.exports = function(router, cache) {
  //OK
  router.route('/token/:apiToken')
  .get(function(req, res) {
    // Get token information

    var token = req.params.apiToken;
    jwt.verify(token, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      if (err) {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'Token not valid.');
        return res.json({
          'success': false,
          'response': 'token_invalid'
        });
      }
      else if (decoded.tokenType == 'apiAccess') {
        return res.json({
          'success': true,
          'response': decoded
        });
      }
      else {
        // Token not valid error.
        res.status(401);
        res.set('WWW-Authenticate', 'token_expect_api');
        return res.json({
          'success': false,
          'response': 'token_expect_api'
        });
      }
    });
  });

  router.route('/token')
  .put(function(req, res) {
    // Token creation.

    if (!req.body.endpoints || req.body.endpoints.length === 0) {
      res.status(400);
      return res.json({
        'success': false,
        'response': 'no_endpoints'
      });
    }
    // Get admin token from header.
    var adminToken = req.headers['Authorization'] || req.headers['authorization'];
    if (!adminToken) {
      // Token not provided error.
      res.status(401);
      res.set('WWW-Authenticate', 'Admin access token required.');
      return res.json({
        'success': false,
        'response': 'no_token'
      });
    }

    jwt.verify(adminToken, config.secretPublic, { algorithm: 'RS256' }, function(errToken, decoded) {
      if (errToken) {
        // Token not valid error.
        res.status(400);
        return res.json({
          'success': false,
          'response': 'token_invalid'
        });
      }
      
      // Get admin ID.
      var adminId = decoded.adminId;
      adminBL.getAdminById(adminId, (err, success) => {
        if (err || !success) {
          // Admin with ID does not exist error.
          res.status(401);
          res.set('WWW-Authenticate', 'admin_not_exist');
          return res.json({
            'success': false,
            'response': 'admin_not_exist'
          });
        }

        adminBL.getAdminRoles(adminId, (err, success) => {
          if (err) {
            // This should not happen XD
            res.status(400);
            return res.json({
              'success': false,
              'response': 'err_granting'
            });
          }
          
          if (success.indexOf('tokenCreation') == -1 && success.indexOf('admin') == -1) {
            res.status(403);
            return res.json({
              'success': false,
              'response': 'admin_not_token_creation_role'
            });
          }

          const roles = success;
          adminBL.getServiceAccessForAdmin(adminId, (err, success) => {
            if (err) {
              // This should not happen XD
              res.status(400);
              return res.json({
                'success': false,
                'response': 'err_granting'
              });
            }

            // Get admin's service access.
            const serviceAccess = success;
            // Requested API endpoints.
            const requestedEndpoints = req.body.endpoints;
            var grantedEndpoints = [];
            for (var i in requestedEndpoints) {
              var requestedEndpoint = requestedEndpoints[i];
              if (roles.indexOf('admin') > -1) {
                // Admin has access to all endpoints.
                grantedEndpoints.push(requestedEndpoint);
              } 
              else {
                var hasAccess = false;
                for (var j in serviceAccess) {
                  var sa = serviceAccess[j];
                  if (sa.method == requestedEndpoint.method &&
                      sa.uri == requestedEndpoint.uri) {
                    hasAccess = true;
                    break;
                  }
                }

                if (hasAccess) {
                  // Push to granted endpoints if admin has access to requested endpoint.
                  grantedEndpoints.push(requestedEndpoint);
                }
              }
            }
            
            if (grantedEndpoints.length === 0) {
              // No endpoints were granted.
              res.status(403);
              return res.json({
                'success': false,
                'response': 'no_permissions'
              });
            }

            if (grantedEndpoints.length > 0 && grantedEndpoints.length < requestedEndpoints.length) {
              // Some endpoints were granted - DO NOT generate token.
              res.status(403);
              return res.json({
                'success': false,
                'response': 'partial_permissions',
                'granted': grantedEndpoints
              });
            }

            if (grantedEndpoints.length == requestedEndpoints.length) {
              // All requested endpoints were granted - generate token.
              var d = new Date();
              var h = req.body.clientHosts || ['localhost'];
              if (h.length === 0) {
                h = ['localhost'];
              }

              var tokenData = {
                'adminId': adminId,
                'tokenType': 'apiAccess',
                'allowedHosts': h,
                'endpoints': grantedEndpoints,
                'iat': Math.floor(d / 1000),
                'iatUTC': d.toUTCString()
              };
              var apiToken = jwt.sign(tokenData, config.secretPrivate, {
                algorithm: 'RS256',
                expiresIn: config.apiTokenExpire // Expires in 365 days.
              });
              adminBL.createToken(apiToken, adminId, true, (err, success) => {
                if (err) {
                  global.ispravimeLogger.log('error', 'err_saving_token');
                  res.status(500);
                  return res.json({
                    'success': false,
                    'response': 'err_saving_token',
                    'details': err
                  });
                }

                const token = success;
                for (var i=0; i<grantedEndpoints.length; i++) {
                  (function(endpoint, tokenId) {
                    adminBL.createServiceAccess(endpoint.uri, endpoint.method, tokenId, null, (err, success) => {
                      if(err) {
                        global.ispravimeLogger.debug(err);
                      }
                    });
                  }) (grantedEndpoints[i], token.id);
                }

                return res.json({
                  'success': true,
                  'response': 'enjoy_token',
                  'token': apiToken
                });
              });
            }
          });          
        });
      });
    });
  });

  function removeOldRevocations(callback) {
    tokensBL.deleteOldRevokedTokens((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', errRemove);
        return;
      }

      if (callback) {
        return callback(success);
      }
    });
  }

  router.route('/token/revoke/clean')
  .post(function(req, res) {
    var token = req.headers['authorization'] || req.headers['Authorization'];
    jwt.verify(token, config.secretPublic, { algorithm: 'RS256' }, function(err, decoded) {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': err
        });
      }
      else if (decoded) {
        if (decoded.roles.indexOf('admin') > -1) {
          removeOldRevocations(function(n) {
            return res.json({
              'success': true,
              'response': n
            });
          });
        }
        else {
          res.status(403);
          return res.json({
            'success': false,
            'response': 'no_permissions'
          });
        }
      }
    });
  });

  router.route('/token/revoke/:rtoken')
  .put(sameAdminCheckRevocation, function(req, res) {
    // Token revocation.

    const token = req.params.rtoken;
    const tokenOwner = req.tokenOwner;
    tokensBL.getRevokedToken(token, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_token_revocation_db'
        });
      }

      var alreadyRevoked = success[0];
      if (alreadyRevoked) {
        res.status(400);
        return res.json({
          'success': false,
          'response': 'err_token_already_revoked'
        });
      }

      adminBL.deleteTokenForAdmin(token, tokenOwner, (err, success) => {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_token_revocation'
          });
        }

        if (success.length == 0) {
          res.status(400);
          return res.json({
            'success': false,
            'response': 'token_does_not_exist'
          });
        }

        var d = new Date();
        var u = new Date((parseInt(req.revokedUntil) + 100000) * 1000);
        const revokedUntil = u.toISOString();
        const revokedAt = d.toISOString();
        const revokedBy = req.revocationRequester;
        const revocationReason = req.body.reason || 'n/a';
        tokensBL.revokeToken(token, tokenOwner, revokedAt, revokedUntil, revokedBy, revocationReason, (err, success) => {
          if (err) {
            global.ispravimeLogger.log('error', err);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_token_revocation'
            });
          }
          
          return res.json({
            'success': true,
            'response': 'token_revoked',
            'revokedToken': token
          });
        });
      });
    });
  });
};
