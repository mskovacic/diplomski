'use strict';

const appRoot = require('app-root-path');
const authHelper = new require(appRoot + '/utils/authHelper.js');
const permissionsEditor = new require(appRoot + '/utils/permissionsEditor.js');
const roleCheck = authHelper.roleCheck;
const async = require('async');
const servicesBL = require(appRoot + '/bl/services.js');

// Routes.
module.exports = function(router, cache) {
  router.route('/services')
  .put(roleCheck('admin'), function(req, res) {
    // Create new service.
    const name = req.body.name;
    servicesBL.getServiceByName(name, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_service_find'
        });
      }

      if (success[0]) {
        // Service with same name already exist.
        res.status(409);
        return res.json({
          'success': false,
          'response': 'service_exists'
        });
      }

      // Check if required fields are set.
      var requiredFields = [];
      var reqIncomplete = false;
      if (!req.body.name) {
        reqIncomplete = true;
        requiredFields.push('required_srv_name');
      }
      if (!req.body.description) {
        reqIncomplete = true;
        requiredFields.push('required_srv_description');
      }
      if (!req.body.endpoints) {
        reqIncomplete = true;
        requiredFields.push('required_srv_endpoints');
      }

      if (reqIncomplete) {
        // Not all required fields are set.
        res.status(400);
        return res.json({
          'success': false,
          'response': {
            'requiredFields': requiredFields
          }
        });
      }

      // Add new service

      // Check if service with desired endpoints already exists.
      var endpointExists = false;
      var serviceFindCalls = [];
      req.body.endpoints.forEach(function(endpoint) {
        serviceFindCalls.push(function(callback) {
          servicesBL.getServiceEndpointByMethodAndUri(endpoint.method, endpoint.uri, (err, success) => {
            if (err)
              return callback(err);
            if (success) {
              endpointExists = true;
            }
            callback(err, success);
          });
        });
      });

      async.parallel(serviceFindCalls, function(err, srv) {
        if (endpointExists) {
          res.status(409);
          return res.json({
            'success': false,
            'reponse': 'service_with_endpoints_exists',
            'requestedEndpoints': req.body.endpoints
          });
        }

        const endpoints = [];
        //var endpointsIncomplete = false;
        var requiredEndpointFields = [];
        for (var i = 0; i < req.body.endpoints.length; i++) {
          var endpoint = req.body.endpoints[i];
          var newEndpoint = {};
          var endpointIncomplete = false;
          if (!endpoint.uri && typeof endpoint.uri !== 'string') {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_uri');
          }
          if (!endpoint.method && typeof endpoint.method !== 'string') {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_method');
          }
          if (!endpoint.description && typeof endpoint.description !== 'string') {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_description');
          }
          if (!endpoint.noAuthRequired && typeof endpoint.noAuthRequired !== 'boolean') {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_noAuthRequired');
          }
          if (!endpoint.allowedByDefault && typeof endpoint.allowedByDefault !== 'boolean') {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_allowedByDefault');
          }
          if (!endpoint.allowedRoles && !Array.isArray(endpoint.allowedRoles)) {
            endpointIncomplete = true;
            requiredEndpointFields.push('required_endpoint_allowedRoles');
          }

          if (endpointIncomplete) {
            //endpointsIncomplete = true;
            // Not all required fields are set.
            res.status(400);
            return res.json({
              'success': false,
              'response': {
                'requiredFields': requiredEndpointFields
              }
            });
          } 
          
          newEndpoint.uri = endpoint.uri;
          newEndpoint.method = endpoint.method;
          newEndpoint.description = endpoint.description;
          newEndpoint.noAuthRequired = endpoint.noAuthRequired;
          newEndpoint.allowedByDefault = endpoint.allowedByDefault;
          newEndpoint.allowedRoles = endpoint.allowedRoles;
          endpoints.push(newEndpoint);
        }

        const name = req.body.name;
        const description = req.body.description;
        // Add service.
        servicesBL.createService(name, description, (err, success) => {
          if (errSave) {
            global.ispravimeLogger.log('error', errSave);
            res.status(503);
            return res.json({
              'success': false,
              'response': 'err_service_add'
            });
          }

          for (let i=0; i<endpoints; i++) {
            (function(endpoint, serviceId) {
              servicesBL.createServiceEndpoints(endpoint.method, endpoint.description, endpoint.noAuthRequired, endpoint.allowedByDefault, serviceId, endpoint.uri, (err, success) => {
                if (err) {
                  global.ispravimeLogger.error(err);
                }
              });
            }) (endpoints[i], success.id);
          }

          permissionsEditor.grantDefaultEndpointsForService(insertedService.name, insertedService.endpoints, function(newPermissions) {});
          return res.json({
            'success': true,
            'response': 'service_added',
            'newService': success
          });
        })
      });
    });
  });

  router.route('/services')
  .get(function(req, res) {
    servicesBL.getServices((err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_services_find'
        });
      }

      return res.json({
        'success': 'true',
        'response': success
      });
    });
  });

  router.route('/services/:id')
  .get(function(req, res) {
    // Get service details

    servicesBL.getServiceDetails(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_service_details_find'
        });
      }

      return res.json({
        'success': 'true',
        'response': success
      });
    });
  })
  .delete(roleCheck('admin'), function(req, res) {
    // Get service details

    servicesBL.deleteService(req.params.id, (err, success) => {
      if (err) {
        global.ispravimeLogger.log('error', err);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_service_remove'
        });
      }

      return res.json({
        'success': 'true',
        'response': 'service_removed'
      });
    });
  });

  router.route('/services/:id/endpoints')
  .put(roleCheck('admin'), function(req, res) {
    // Add new endpoints.

    servicesBL.getServiceDetails(req.params.id, (err, success) => {
      if (errFind) {
        global.ispravimeLogger.log('error', errFind);
        res.status(503);
        return res.json({
          'success': false,
          'response': 'err_service_find'
        });
      }

      if (!success) {
        // Service not found
        res.status(404);
        return res.json({
          'success': false,
          'response': 'service_not_exists'
        });
      }

      if (!req.body.endpoints || !Array.isArray(req.body.endpoints) || req.body.endpoints.length === 0) {
        res.status(400);
        return res.json({
          'success': false,
          'response': 'no_endpoints_provided'
        });
      }
    
      var newEndpoints = [];
      for (var i = 0; i < req.body.endpoints.length; i++) {
        var endpoint = req.body.endpoints[i];
        var newEndpoint = {};
        var requiredEndpointFields = [];
        var endpointIncomplete = false;
        if (!endpoint.uri && typeof endpoint.uri !== 'string') {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_uri');
        }
        if (!endpoint.method && typeof endpoint.method !== 'string') {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_method');
        }
        if (!endpoint.description && typeof endpoint.description !== 'string') {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_description');
        }
        if (!endpoint.noAuthRequired && typeof endpoint.noAuthRequired !== 'boolean') {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_noAuthRequired');
        }
        if (!endpoint.allowedByDefault && typeof endpoint.allowedByDefault !== 'boolean') {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_allowedByDefault');
        }
        if (!endpoint.allowedRoles && !Array.isArray(endpoint.allowedRoles)) {
          endpointIncomplete = true;
          requiredEndpointFields.push('required_endpoint_allowedRoles');
        }

        if (endpointIncomplete) {
          // Not all required fields are set.
          res.status(400);
          return res.json({
            'success': false,
            'response': {
              'requiredFields': requiredEndpointFields
            }
          });
        }

        newEndpoint.uri = endpoint.uri;
        newEndpoint.method = endpoint.method;
        newEndpoint.description = endpoint.description;
        newEndpoint.noAuthRequired = endpoint.noAuthRequired;
        newEndpoint.allowedByDefault = endpoint.allowedByDefault;
        newEndpoint.allowedRoles = endpoint.allowedRoles;
        newEndpoint.serviceId = req.params.id;
        newEndpoints.push(newEndpoint);
      }

      var endpointsAdded = [];
      for (var i = 0; i < newEndpoints.length; i++) {
        var endpoint = newEndpoints[i];
        (function(endpoint) {
          servicesBL.createServiceEndpoints(
            endpoint.method, 
            endpoint.description,
            endpoint.noAuthRequired,
            endpoint.allowedByDefault,
            endpoint.serviceId,
            endpoint.uri, (err, success) => {
              if (err) {
                return;
              }

              for (var j = 0; j < endpoint.allowedRoles.length; j++) {
                var role = endpoint.allowedRoles[j];
                (function(role){
                  servicesBL.createAllowedRoles(role, success.id, (err, success) => {
                    if (err) {
                      return;
                    }

                    endpointsAdded.push(endpoint);
                  });
                }) (role);
              } 
            });
        }) (endpoint);
      }

      if (endpointsAdded.length == 0)
      {
        if (err) {
          global.ispravimeLogger.log('error', err);
          res.status(503);
          return res.json({
            'success': false,
            'response': 'err_service_endpoint_adding'
          });
        }
      }
        
      return res.json({
        'success': true,
        'response': 'endpoints_added',
        'endpointsAdded': endpointsAdded
      });
    });
  })
  .delete(roleCheck('admin'), function(req, res) {
    // Remove endpoints.

    // Check if required fields are set.
    if (!req.body.endpoints || !Array.isArray(req.body.endpoints) || req.body.endpoints.length === 0) {
      res.status(400);
      return res.json({
        'success': false,
        'response': 'no_endpoints_provided'
      });
    } 

    var endpointsIncomplete = false;
    var requiredEndpointFields = [];
    for (var i = 0; i < req.body.endpoints.length; i++) {
      var endpoint = req.body.endpoints[i];
      if (!endpoint.uri && typeof endpoint.uri !== 'string') {
        endpointsIncomplete = true;
        requiredEndpointFields.push('required_endpoint_uri');
      }
      if (!endpoint.method && typeof endpoint.method !== 'string') {
        endpointsIncomplete = true;
        requiredEndpointFields.push('required_endpoint_method');
      }
    }

    if (endpointsIncomplete) {
      // Not all required fields are set.
      res.status(400);
      return res.json({
        'success': false,
        'response': {
          'requiredFields': requiredEndpointFields
        }
      });
    }

    var deleteServiceEndpoints = [];
    for (var i=0; i < req.body.endpoints.length; i++) {
      var endpoint = req.body.endpoints[i];
      (function(endpoint) {
        servicesBL.deleteServiceEndpoint(req.params.id, endpoint.uri, endpoint.method, (err, success) => {
          if (err) {
            return;
          }

          deleteServiceEndpoints.push(endpoint);
        });
      })(endpoint);
    }

    if (deleteServiceEndpoints.length == 0) {
      global.ispravimeLogger.log('error', "No endpoints deleted!");
      res.status(500);
      return res.json({
        'success': false,
        'response': "No endpoints deleted!"
      });
    }

    return res.json({
      'success': true,
      'response': 'endpoints_removed'
    });
  });
};
